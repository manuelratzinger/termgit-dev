Instance: noe-lga-noerad 
InstanceOf: CodeSystem 
Usage: #definition 
* id = "noe-lga-noerad" 
* url = "https://termgit.noe-lga.at/CodeSystem/noerad" 
* name = "noe-lga-noerad" 
* title = "NÖ LGA Radiologiekatalog" 
* status = #draft 
* content = #complete 
* version = "1.0.0+202302" 
* description = "**Description:** CodeSystem for all NÖ Radiology services" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:1.2.40.0.10.1.6.1.10.4.1 " 
* date = "2023-10-17" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "http://www.noe-lga.at/noerad/" 
* count = 1142 
* property[0].code = #child 
* property[0].type = #code 
* property[1].code = #parent 
* property[1].type = #code 
* property[2].code = #status 
* property[2].type = #code 
* property[3].code = #hints 
* property[3].type = #string 
* #AN "AN Angiographie"
* #AN ^property[0].code = #child 
* #AN ^property[0].valueCode = #AN01 
* #AN ^property[1].code = #child 
* #AN ^property[1].valueCode = #AN02 
* #AN ^property[2].code = #child 
* #AN ^property[2].valueCode = #AN03 
* #AN ^property[3].code = #child 
* #AN ^property[3].valueCode = #AN04 
* #AN ^property[4].code = #child 
* #AN ^property[4].valueCode = #AN05 
* #AN ^property[5].code = #child 
* #AN ^property[5].valueCode = #AN06 
* #AN ^property[6].code = #child 
* #AN ^property[6].valueCode = #AN07 
* #AN ^property[7].code = #child 
* #AN ^property[7].valueCode = #AN08 
* #AN ^property[8].code = #child 
* #AN ^property[8].valueCode = #AN09 
* #AN ^property[9].code = #child 
* #AN ^property[9].valueCode = #AN10 
* #AN ^property[10].code = #child 
* #AN ^property[10].valueCode = #AN11 
* #AN01 "AN Becken, untere Extremität"
* #AN01 ^property[0].code = #parent 
* #AN01 ^property[0].valueCode = #AN 
* #AN01 ^property[1].code = #child 
* #AN01 ^property[1].valueCode = #AN0101 
* #AN01 ^property[2].code = #child 
* #AN01 ^property[2].valueCode = #AN0102 
* #AN0101 "AN Becken-Bein-Bereich diagnostisch"
* #AN0101 ^property[0].code = #parent 
* #AN0101 ^property[0].valueCode = #AN01 
* #AN0101 ^property[1].code = #child 
* #AN0101 ^property[1].valueCode = #AN010101 
* #AN0101 ^property[2].code = #child 
* #AN0101 ^property[2].valueCode = #AN010102 
* #AN0101 ^property[3].code = #child 
* #AN0101 ^property[3].valueCode = #AN010103 
* #AN0101 ^property[4].code = #child 
* #AN0101 ^property[4].valueCode = #AN010104 
* #AN010101 "AN Becken li."
* #AN010101 ^property[0].code = #parent 
* #AN010101 ^property[0].valueCode = #AN0101 
* #AN010102 "AN Becken re."
* #AN010102 ^property[0].code = #parent 
* #AN010102 ^property[0].valueCode = #AN0101 
* #AN010103 "AN untere Extremität li."
* #AN010103 ^property[0].code = #parent 
* #AN010103 ^property[0].valueCode = #AN0101 
* #AN010104 "AN untere Extremität re."
* #AN010104 ^property[0].code = #parent 
* #AN010104 ^property[0].valueCode = #AN0101 
* #AN0102 "AN Intervention Becken-Bein Bereich"
* #AN0102 ^property[0].code = #parent 
* #AN0102 ^property[0].valueCode = #AN01 
* #AN0102 ^property[1].code = #child 
* #AN0102 ^property[1].valueCode = #AN010201 
* #AN0102 ^property[2].code = #child 
* #AN0102 ^property[2].valueCode = #AN010202 
* #AN0102 ^property[3].code = #child 
* #AN0102 ^property[3].valueCode = #AN010203 
* #AN0102 ^property[4].code = #child 
* #AN0102 ^property[4].valueCode = #AN010204 
* #AN0102 ^property[5].code = #child 
* #AN0102 ^property[5].valueCode = #AN010205 
* #AN0102 ^property[6].code = #child 
* #AN0102 ^property[6].valueCode = #AN010206 
* #AN0102 ^property[7].code = #child 
* #AN0102 ^property[7].valueCode = #AN010210 
* #AN0102 ^property[8].code = #child 
* #AN0102 ^property[8].valueCode = #AN010215 
* #AN0102 ^property[9].code = #child 
* #AN0102 ^property[9].valueCode = #AN010216 
* #AN0102 ^property[10].code = #child 
* #AN0102 ^property[10].valueCode = #AN010217 
* #AN0102 ^property[11].code = #child 
* #AN0102 ^property[11].valueCode = #AN010218 
* #AN0102 ^property[12].code = #child 
* #AN0102 ^property[12].valueCode = #AN010219 
* #AN010201 "AN PTA Becken li."
* #AN010201 ^property[0].code = #parent 
* #AN010201 ^property[0].valueCode = #AN0102 
* #AN010202 "AN PTA Becken re."
* #AN010202 ^property[0].code = #parent 
* #AN010202 ^property[0].valueCode = #AN0102 
* #AN010203 "AN Stent Becken li."
* #AN010203 ^property[0].code = #parent 
* #AN010203 ^property[0].valueCode = #AN0102 
* #AN010204 "AN Stent Becken re."
* #AN010204 ^property[0].code = #parent 
* #AN010204 ^property[0].valueCode = #AN0102 
* #AN010205 "AN Embolisation Becken li."
* #AN010205 ^property[0].code = #parent 
* #AN010205 ^property[0].valueCode = #AN0102 
* #AN010206 "AN Embolisation Becken re."
* #AN010206 ^property[0].code = #parent 
* #AN010206 ^property[0].valueCode = #AN0102 
* #AN010210 "AN PTA m. medikamentenbeschichtetem Ballon (DEB) unt. Extremität re."
* #AN010210 ^property[0].code = #parent 
* #AN010210 ^property[0].valueCode = #AN0102 
* #AN010215 "AN Embolisation untere Extremität li."
* #AN010215 ^property[0].code = #parent 
* #AN010215 ^property[0].valueCode = #AN0102 
* #AN010216 "AN Embolisation untere Extremität re."
* #AN010216 ^property[0].code = #parent 
* #AN010216 ^property[0].valueCode = #AN0102 
* #AN010217 "AN PTA Oberschenkel"
* #AN010217 ^property[0].code = #parent 
* #AN010217 ^property[0].valueCode = #AN0102 
* #AN010217 ^property[1].code = #child 
* #AN010217 ^property[1].valueCode = #AN010217a 
* #AN010217 ^property[2].code = #child 
* #AN010217 ^property[2].valueCode = #AN010217b 
* #AN010217 ^property[3].code = #child 
* #AN010217 ^property[3].valueCode = #AN010217c 
* #AN010217 ^property[4].code = #child 
* #AN010217 ^property[4].valueCode = #AN010217d 
* #AN010217a "AN PTA Oberschenkelarterien li."
* #AN010217a ^property[0].code = #parent 
* #AN010217a ^property[0].valueCode = #AN010217 
* #AN010217b "AN PTA Oberschenkelarterien re."
* #AN010217b ^property[0].code = #parent 
* #AN010217b ^property[0].valueCode = #AN010217 
* #AN010217c "AN PT Rekanal. mit Stentimplantation Oberschenkelarterien li."
* #AN010217c ^property[0].code = #parent 
* #AN010217c ^property[0].valueCode = #AN010217 
* #AN010217d "AN PT Rekanal. mit Stentimplantation Oberschenkelarterien re."
* #AN010217d ^property[0].code = #parent 
* #AN010217d ^property[0].valueCode = #AN010217 
* #AN010218 "AN PTA Unterschenkel"
* #AN010218 ^property[0].code = #parent 
* #AN010218 ^property[0].valueCode = #AN0102 
* #AN010218 ^property[1].code = #child 
* #AN010218 ^property[1].valueCode = #AN010218a 
* #AN010218 ^property[2].code = #child 
* #AN010218 ^property[2].valueCode = #AN010218b 
* #AN010218 ^property[3].code = #child 
* #AN010218 ^property[3].valueCode = #AN010218c 
* #AN010218 ^property[4].code = #child 
* #AN010218 ^property[4].valueCode = #AN010218d 
* #AN010218a "AN PTA Unterschenkelarterien li."
* #AN010218a ^property[0].code = #parent 
* #AN010218a ^property[0].valueCode = #AN010218 
* #AN010218b "AN PTA Unterschenkelarterien re."
* #AN010218b ^property[0].code = #parent 
* #AN010218b ^property[0].valueCode = #AN010218 
* #AN010218c "AN PT Rekanal. mit Stentimplantation Unterschenkelarterien li."
* #AN010218c ^property[0].code = #parent 
* #AN010218c ^property[0].valueCode = #AN010218 
* #AN010218d "AN PT Rekanal. mit Stentimplantation Unterschenkelarterien re."
* #AN010218d ^property[0].code = #parent 
* #AN010218d ^property[0].valueCode = #AN010218 
* #AN010219 "AN PTA m. medikamentenbeschichtetem Ballon (DEB) unt. Extremität li."
* #AN010219 ^property[0].code = #parent 
* #AN010219 ^property[0].valueCode = #AN0102 
* #AN02 "AN Schädel-/Halsbereich"
* #AN02 ^property[0].code = #parent 
* #AN02 ^property[0].valueCode = #AN 
* #AN02 ^property[1].code = #child 
* #AN02 ^property[1].valueCode = #AN0201 
* #AN02 ^property[2].code = #child 
* #AN02 ^property[2].valueCode = #AN0202 
* #AN0201 "AN Schädel- /Halsbereich diagnostisch"
* #AN0201 ^property[0].code = #parent 
* #AN0201 ^property[0].valueCode = #AN02 
* #AN0201 ^property[1].code = #child 
* #AN0201 ^property[1].valueCode = #AN020101 
* #AN0201 ^property[2].code = #child 
* #AN0201 ^property[2].valueCode = #AN020102 
* #AN0201 ^property[3].code = #child 
* #AN0201 ^property[3].valueCode = #AN020103 
* #AN0201 ^property[4].code = #child 
* #AN0201 ^property[4].valueCode = #AN020104 
* #AN0201 ^property[5].code = #child 
* #AN0201 ^property[5].valueCode = #AN020105 
* #AN0201 ^property[6].code = #child 
* #AN0201 ^property[6].valueCode = #AN020106 
* #AN0201 ^property[7].code = #child 
* #AN0201 ^property[7].valueCode = #AN020107 
* #AN0201 ^property[8].code = #child 
* #AN0201 ^property[8].valueCode = #AN020108 
* #AN0201 ^property[9].code = #child 
* #AN0201 ^property[9].valueCode = #AN020109 
* #AN0201 ^property[10].code = #child 
* #AN0201 ^property[10].valueCode = #AN020110 
* #AN0201 ^property[11].code = #child 
* #AN0201 ^property[11].valueCode = #AN020111 
* #AN0201 ^property[12].code = #child 
* #AN0201 ^property[12].valueCode = #AN020112 
* #AN0201 ^property[13].code = #child 
* #AN0201 ^property[13].valueCode = #AN020113 
* #AN0201 ^property[14].code = #child 
* #AN0201 ^property[14].valueCode = #AN020114 
* #AN0201 ^property[15].code = #child 
* #AN0201 ^property[15].valueCode = #AN020115 
* #AN020101 "AN A. Carotis com. li."
* #AN020101 ^property[0].code = #parent 
* #AN020101 ^property[0].valueCode = #AN0201 
* #AN020102 "AN A. Carotis com. re."
* #AN020102 ^property[0].code = #parent 
* #AN020102 ^property[0].valueCode = #AN0201 
* #AN020103 "AN ACI (Supersel. Carotis int) li."
* #AN020103 ^property[0].code = #parent 
* #AN020103 ^property[0].valueCode = #AN0201 
* #AN020104 "AN ACI (Supersel. Carotis int) re."
* #AN020104 ^property[0].code = #parent 
* #AN020104 ^property[0].valueCode = #AN0201 
* #AN020105 "AN ACE (Supersel. Carotis ext) li."
* #AN020105 ^property[0].code = #parent 
* #AN020105 ^property[0].valueCode = #AN0201 
* #AN020106 "AN ACE (Supersel. Carotis ext) re."
* #AN020106 ^property[0].code = #parent 
* #AN020106 ^property[0].valueCode = #AN0201 
* #AN020107 "AN A. Vertebralis li."
* #AN020107 ^property[0].code = #parent 
* #AN020107 ^property[0].valueCode = #AN0201 
* #AN020108 "AN A. Vertebralis re"
* #AN020108 ^property[0].code = #parent 
* #AN020108 ^property[0].valueCode = #AN0201 
* #AN020109 "AN A. Basilaris"
* #AN020109 ^property[0].code = #parent 
* #AN020109 ^property[0].valueCode = #AN0201 
* #AN020110 "AN A. Subclavia li."
* #AN020110 ^property[0].code = #parent 
* #AN020110 ^property[0].valueCode = #AN0201 
* #AN020111 "AN A. Subclavia re."
* #AN020111 ^property[0].code = #parent 
* #AN020111 ^property[0].valueCode = #AN0201 
* #AN020112 "AN Tr. Brachiocephalicus li."
* #AN020112 ^property[0].code = #parent 
* #AN020112 ^property[0].valueCode = #AN0201 
* #AN020113 "AN Tr. Brachiocephalicus re."
* #AN020113 ^property[0].code = #parent 
* #AN020113 ^property[0].valueCode = #AN0201 
* #AN020114 "AN Arcus Aortae"
* #AN020114 ^property[0].code = #parent 
* #AN020114 ^property[0].valueCode = #AN0201 
* #AN020115 "AN Dacryocystographie"
* #AN020115 ^property[0].code = #parent 
* #AN020115 ^property[0].valueCode = #AN0201 
* #AN0202 "AN Intervention Schädel-/Halsbereich"
* #AN0202 ^property[0].code = #parent 
* #AN0202 ^property[0].valueCode = #AN02 
* #AN0202 ^property[1].code = #child 
* #AN0202 ^property[1].valueCode = #AN020201 
* #AN0202 ^property[2].code = #child 
* #AN0202 ^property[2].valueCode = #AN020202 
* #AN0202 ^property[3].code = #child 
* #AN0202 ^property[3].valueCode = #AN020203 
* #AN0202 ^property[4].code = #child 
* #AN0202 ^property[4].valueCode = #AN020204 
* #AN0202 ^property[5].code = #child 
* #AN0202 ^property[5].valueCode = #AN020205 
* #AN0202 ^property[6].code = #child 
* #AN0202 ^property[6].valueCode = #AN020206 
* #AN0202 ^property[7].code = #child 
* #AN0202 ^property[7].valueCode = #AN020207 
* #AN0202 ^property[8].code = #child 
* #AN0202 ^property[8].valueCode = #AN020208 
* #AN020201 "AN Coiling intrakranieller Gefäße "
* #AN020201 ^property[0].code = #parent 
* #AN020201 ^property[0].valueCode = #AN0202 
* #AN020202 "AN Lokale Lyse kranieller Gefäße"
* #AN020202 ^property[0].code = #parent 
* #AN020202 ^property[0].valueCode = #AN0202 
* #AN020203 "AN Katheterembolisation – kranielle Gefäße"
* #AN020203 ^property[0].code = #parent 
* #AN020203 ^property[0].valueCode = #AN0202 
* #AN020204 "AN Thrombektomie intrakranieller Gefäße"
* #AN020204 ^property[0].code = #parent 
* #AN020204 ^property[0].valueCode = #AN0202 
* #AN020205 "AN PTA intrakranielle Gefäße"
* #AN020205 ^property[0].code = #parent 
* #AN020205 ^property[0].valueCode = #AN0202 
* #AN020206 "AN Rekanalisation mit Stentimplantation intrakranelle Gefäße"
* #AN020206 ^property[0].code = #parent 
* #AN020206 ^property[0].valueCode = #AN0202 
* #AN020207 "AN PTA Halsgefäße"
* #AN020207 ^property[0].code = #parent 
* #AN020207 ^property[0].valueCode = #AN0202 
* #AN020208 "AN Stent Halsgefäße"
* #AN020208 ^property[0].code = #parent 
* #AN020208 ^property[0].valueCode = #AN0202 
* #AN03 "AN obere Extremität"
* #AN03 ^property[0].code = #parent 
* #AN03 ^property[0].valueCode = #AN 
* #AN03 ^property[1].code = #child 
* #AN03 ^property[1].valueCode = #AN0301 
* #AN03 ^property[2].code = #child 
* #AN03 ^property[2].valueCode = #AN0302 
* #AN0301 "AN obere Extremität diagnostisch"
* #AN0301 ^property[0].code = #parent 
* #AN0301 ^property[0].valueCode = #AN03 
* #AN0301 ^property[1].code = #child 
* #AN0301 ^property[1].valueCode = #AN030101 
* #AN0301 ^property[2].code = #child 
* #AN0301 ^property[2].valueCode = #AN030102 
* #AN030101 "AN obere Extremität li"
* #AN030101 ^property[0].code = #parent 
* #AN030101 ^property[0].valueCode = #AN0301 
* #AN030102 "AN obere Extremität re"
* #AN030102 ^property[0].code = #parent 
* #AN030102 ^property[0].valueCode = #AN0301 
* #AN0302 "AN Intervention obere Extremität"
* #AN0302 ^property[0].code = #parent 
* #AN0302 ^property[0].valueCode = #AN03 
* #AN0302 ^property[1].code = #child 
* #AN0302 ^property[1].valueCode = #AN030201 
* #AN0302 ^property[2].code = #child 
* #AN0302 ^property[2].valueCode = #AN030202 
* #AN0302 ^property[3].code = #child 
* #AN0302 ^property[3].valueCode = #AN030203 
* #AN0302 ^property[4].code = #child 
* #AN0302 ^property[4].valueCode = #AN030204 
* #AN0302 ^property[5].code = #child 
* #AN0302 ^property[5].valueCode = #AN030205 
* #AN0302 ^property[6].code = #child 
* #AN0302 ^property[6].valueCode = #AN030206 
* #AN030201 "AN PTA obere Extremität, Thorax li."
* #AN030201 ^property[0].code = #parent 
* #AN030201 ^property[0].valueCode = #AN0302 
* #AN030202 "AN PTA obere Extremität, Thorax re."
* #AN030202 ^property[0].code = #parent 
* #AN030202 ^property[0].valueCode = #AN0302 
* #AN030203 "AN Stent obere Extremität, Thorax li."
* #AN030203 ^property[0].code = #parent 
* #AN030203 ^property[0].valueCode = #AN0302 
* #AN030204 "AN Stent obere Extremität, Thorax re."
* #AN030204 ^property[0].code = #parent 
* #AN030204 ^property[0].valueCode = #AN0302 
* #AN030205 "AN Katheterembolisation obere Extremität, Thorax li."
* #AN030205 ^property[0].code = #parent 
* #AN030205 ^property[0].valueCode = #AN0302 
* #AN030206 "AN Katheterembolisation obere Extremität, Thorax re."
* #AN030206 ^property[0].code = #parent 
* #AN030206 ^property[0].valueCode = #AN0302 
* #AN04 "AN Körperstamm"
* #AN04 ^property[0].code = #parent 
* #AN04 ^property[0].valueCode = #AN 
* #AN04 ^property[1].code = #child 
* #AN04 ^property[1].valueCode = #AN0401 
* #AN04 ^property[2].code = #child 
* #AN04 ^property[2].valueCode = #AN0402 
* #AN04 ^property[3].code = #child 
* #AN04 ^property[3].valueCode = #AN0403 
* #AN0401 "AN Gefäße Thorax/Abdomen diagnostisch"
* #AN0401 ^property[0].code = #parent 
* #AN0401 ^property[0].valueCode = #AN04 
* #AN0401 ^property[1].code = #child 
* #AN0401 ^property[1].valueCode = #AN040101 
* #AN0401 ^property[2].code = #child 
* #AN0401 ^property[2].valueCode = #AN040102 
* #AN0401 ^property[3].code = #child 
* #AN0401 ^property[3].valueCode = #AN040104 
* #AN0401 ^property[4].code = #child 
* #AN0401 ^property[4].valueCode = #AN040105 
* #AN0401 ^property[5].code = #child 
* #AN0401 ^property[5].valueCode = #AN040106 
* #AN0401 ^property[6].code = #child 
* #AN0401 ^property[6].valueCode = #AN040107 
* #AN0401 ^property[7].code = #child 
* #AN0401 ^property[7].valueCode = #AN040108 
* #AN0401 ^property[8].code = #child 
* #AN0401 ^property[8].valueCode = #AN040109 
* #AN0401 ^property[9].code = #child 
* #AN0401 ^property[9].valueCode = #AN040110 
* #AN0401 ^property[10].code = #child 
* #AN0401 ^property[10].valueCode = #AN040111 
* #AN0401 ^property[11].code = #child 
* #AN0401 ^property[11].valueCode = #AN040112 
* #AN0401 ^property[12].code = #child 
* #AN0401 ^property[12].valueCode = #AN040113 
* #AN0401 ^property[13].code = #child 
* #AN0401 ^property[13].valueCode = #AN040114 
* #AN0401 ^property[14].code = #child 
* #AN0401 ^property[14].valueCode = #AN040115 
* #AN0401 ^property[15].code = #child 
* #AN0401 ^property[15].valueCode = #AN040116 
* #AN0401 ^property[16].code = #child 
* #AN0401 ^property[16].valueCode = #AN040117 
* #AN0401 ^property[17].code = #child 
* #AN0401 ^property[17].valueCode = #AN040118 
* #AN0401 ^property[18].code = #child 
* #AN0401 ^property[18].valueCode = #AN040119 
* #AN040101 "AN Aorta"
* #AN040101 ^property[0].code = #parent 
* #AN040101 ^property[0].valueCode = #AN0401 
* #AN040102 "AN Katheterangiographie der Pulmonalarterien"
* #AN040102 ^property[0].code = #parent 
* #AN040102 ^property[0].valueCode = #AN0401 
* #AN040102 ^property[1].code = #child 
* #AN040102 ^property[1].valueCode = #AN040102a 
* #AN040102 ^property[2].code = #child 
* #AN040102 ^property[2].valueCode = #AN040102b 
* #AN040102 ^property[3].code = #child 
* #AN040102 ^property[3].valueCode = #AN040102c 
* #AN040102a "AN Perkutaner Verschluss eines offenen Ductus arteriosus Botalli"
* #AN040102a ^property[0].code = #parent 
* #AN040102a ^property[0].valueCode = #AN040102 
* #AN040102b "AN Perk. Verschl. aorto-, atriopulm-, arterio-, venoven- Kollateralgef"
* #AN040102b ^property[0].code = #parent 
* #AN040102b ^property[0].valueCode = #AN040102 
* #AN040102c "AN Stentimplant. in Pulmonalgef. od. e. off. Ductus arteriosus Botalli"
* #AN040102c ^property[0].code = #parent 
* #AN040102c ^property[0].valueCode = #AN040102 
* #AN040104 "AN A.Spinalis"
* #AN040104 ^property[0].code = #parent 
* #AN040104 ^property[0].valueCode = #AN0401 
* #AN040105 "AN A.Intercostalis li."
* #AN040105 ^property[0].code = #parent 
* #AN040105 ^property[0].valueCode = #AN0401 
* #AN040106 "AN A.Intercostalis re."
* #AN040106 ^property[0].code = #parent 
* #AN040106 ^property[0].valueCode = #AN0401 
* #AN040107 "AN A.Bronchialis li."
* #AN040107 ^property[0].code = #parent 
* #AN040107 ^property[0].valueCode = #AN0401 
* #AN040108 "AN A.Bronchialis re."
* #AN040108 ^property[0].code = #parent 
* #AN040108 ^property[0].valueCode = #AN0401 
* #AN040109 "AN A.Phrenica li."
* #AN040109 ^property[0].code = #parent 
* #AN040109 ^property[0].valueCode = #AN0401 
* #AN040110 "AN A.Phrenica re."
* #AN040110 ^property[0].code = #parent 
* #AN040110 ^property[0].valueCode = #AN0401 
* #AN040111 "AN A.Renalis li."
* #AN040111 ^property[0].code = #parent 
* #AN040111 ^property[0].valueCode = #AN0401 
* #AN040112 "AN A.Renalis re."
* #AN040112 ^property[0].code = #parent 
* #AN040112 ^property[0].valueCode = #AN0401 
* #AN040113 "AN A.Mesenterica inferior"
* #AN040113 ^property[0].code = #parent 
* #AN040113 ^property[0].valueCode = #AN0401 
* #AN040114 "AN A.Mesenterica superior"
* #AN040114 ^property[0].code = #parent 
* #AN040114 ^property[0].valueCode = #AN0401 
* #AN040115 "AN A.Hepatica"
* #AN040115 ^property[0].code = #parent 
* #AN040115 ^property[0].valueCode = #AN0401 
* #AN040116 "AN Truncus Coeliacus"
* #AN040116 ^property[0].code = #parent 
* #AN040116 ^property[0].valueCode = #AN0401 
* #AN040117 "AN Mesenterico-Portographie"
* #AN040117 ^property[0].code = #parent 
* #AN040117 ^property[0].valueCode = #AN0401 
* #AN040118 "AN Splenoportographie"
* #AN040118 ^property[0].code = #parent 
* #AN040118 ^property[0].valueCode = #AN0401 
* #AN040119 "AN Penisangio"
* #AN040119 ^property[0].code = #parent 
* #AN040119 ^property[0].valueCode = #AN0401 
* #AN0402 "AN Interventionen Thorax/Abdomen"
* #AN0402 ^property[0].code = #parent 
* #AN0402 ^property[0].valueCode = #AN04 
* #AN0402 ^property[1].code = #child 
* #AN0402 ^property[1].valueCode = #AN040201 
* #AN0402 ^property[2].code = #child 
* #AN0402 ^property[2].valueCode = #AN040202 
* #AN0402 ^property[3].code = #child 
* #AN0402 ^property[3].valueCode = #AN040203 
* #AN0402 ^property[4].code = #child 
* #AN0402 ^property[4].valueCode = #AN040204 
* #AN0402 ^property[5].code = #child 
* #AN0402 ^property[5].valueCode = #AN040205 
* #AN0402 ^property[6].code = #child 
* #AN0402 ^property[6].valueCode = #AN040206 
* #AN0402 ^property[7].code = #child 
* #AN0402 ^property[7].valueCode = #AN040207 
* #AN0402 ^property[8].code = #child 
* #AN0402 ^property[8].valueCode = #AN040208 
* #AN0402 ^property[9].code = #child 
* #AN0402 ^property[9].valueCode = #AN040209 
* #AN0402 ^property[10].code = #child 
* #AN0402 ^property[10].valueCode = #AN040210 
* #AN040201 "AN Katheterangiographie und lokale Lyse der Pulmonalarterie"
* #AN040201 ^property[0].code = #parent 
* #AN040201 ^property[0].valueCode = #AN0402 
* #AN040202 "AN PTA Nieren, Viszeralgefäße"
* #AN040202 ^property[0].code = #parent 
* #AN040202 ^property[0].valueCode = #AN0402 
* #AN040203 "AN Stent Niere, Viszeralgefäße"
* #AN040203 ^property[0].code = #parent 
* #AN040203 ^property[0].valueCode = #AN0402 
* #AN040204 "AN Embolisation Niere, Viszeralgefäße"
* #AN040204 ^property[0].code = #parent 
* #AN040204 ^property[0].valueCode = #AN0402 
* #AN040205 "AN Angio und Lyse Viszeralgefäße li."
* #AN040205 ^property[0].code = #parent 
* #AN040205 ^property[0].valueCode = #AN0402 
* #AN040206 "AN Angio und Lyse Viszeralgefäße re."
* #AN040206 ^property[0].code = #parent 
* #AN040206 ^property[0].valueCode = #AN0402 
* #AN040207 "AN PTA Aorta"
* #AN040207 ^property[0].code = #parent 
* #AN040207 ^property[0].valueCode = #AN0402 
* #AN040208 "AN Stent Aorta"
* #AN040208 ^property[0].code = #parent 
* #AN040208 ^property[0].valueCode = #AN0402 
* #AN040209 "AN Cavafilter"
* #AN040209 ^property[0].code = #parent 
* #AN040209 ^property[0].valueCode = #AN0402 
* #AN040210 "AN Cavaschirmentfernung"
* #AN040210 ^property[0].code = #parent 
* #AN040210 ^property[0].valueCode = #AN0402 
* #AN0403 "AN Stentgraft"
* #AN0403 ^property[0].code = #parent 
* #AN0403 ^property[0].valueCode = #AN04 
* #AN0403 ^property[1].code = #child 
* #AN0403 ^property[1].valueCode = #AN040301 
* #AN0403 ^property[2].code = #child 
* #AN0403 ^property[2].valueCode = #AN040302 
* #AN0403 ^property[3].code = #child 
* #AN0403 ^property[3].valueCode = #AN040303 
* #AN0403 ^property[4].code = #child 
* #AN0403 ^property[4].valueCode = #AN040304 
* #AN0403 ^property[5].code = #child 
* #AN0403 ^property[5].valueCode = #AN040305 
* #AN0403 ^property[6].code = #child 
* #AN0403 ^property[6].valueCode = #AN040306 
* #AN0403 ^property[7].code = #child 
* #AN0403 ^property[7].valueCode = #AN040307 
* #AN040301 "AN Implantation Stentgraft Aorta thorakal "
* #AN040301 ^property[0].code = #parent 
* #AN040301 ^property[0].valueCode = #AN0403 
* #AN040302 "AN Implantation Stentgraft Aorta abdominal"
* #AN040302 ^property[0].code = #parent 
* #AN040302 ^property[0].valueCode = #AN0403 
* #AN040303 "AN Implant. gefenst. Stentgraft Aorta abdominal para-/suprarenal"
* #AN040303 ^property[0].code = #parent 
* #AN040303 ^property[0].valueCode = #AN0403 
* #AN040304 "AN Implantation Stentgraft Aortenbifurkation"
* #AN040304 ^property[0].code = #parent 
* #AN040304 ^property[0].valueCode = #AN0403 
* #AN040305 "AN Implantation Stentgraft Aorta aszendens"
* #AN040305 ^property[0].code = #parent 
* #AN040305 ^property[0].valueCode = #AN0403 
* #AN040306 "AN Implantation Stentgraft Beckengefäß li."
* #AN040306 ^property[0].code = #parent 
* #AN040306 ^property[0].valueCode = #AN0403 
* #AN040307 "AN Implantation Stentgraft Beckengefäß re."
* #AN040307 ^property[0].code = #parent 
* #AN040307 ^property[0].valueCode = #AN0403 
* #AN05 "AN Sonstige Gefäße"
* #AN05 ^property[0].code = #parent 
* #AN05 ^property[0].valueCode = #AN 
* #AN05 ^property[1].code = #child 
* #AN05 ^property[1].valueCode = #AN0501 
* #AN05 ^property[2].code = #child 
* #AN05 ^property[2].valueCode = #AN0502 
* #AN05 ^property[3].code = #child 
* #AN05 ^property[3].valueCode = #AN0503 
* #AN05 ^property[4].code = #child 
* #AN05 ^property[4].valueCode = #AN0504 
* #AN0501 "AN diagnostisch Diverses"
* #AN0501 ^property[0].code = #parent 
* #AN0501 ^property[0].valueCode = #AN05 
* #AN0502 "AN arteriovenöser Shunts"
* #AN0502 ^property[0].code = #parent 
* #AN0502 ^property[0].valueCode = #AN05 
* #AN0503 "AN Lymphgefäße"
* #AN0503 ^property[0].code = #parent 
* #AN0503 ^property[0].valueCode = #AN05 
* #AN0504 "AN Implantation eines Langzeit ZVK m/o Port"
* #AN0504 ^property[0].code = #parent 
* #AN0504 ^property[0].valueCode = #AN05 
* #AN06 "AN Denervationstherapie"
* #AN06 ^property[0].code = #parent 
* #AN06 ^property[0].valueCode = #AN 
* #AN06 ^property[1].code = #child 
* #AN06 ^property[1].valueCode = #AN0601 
* #AN06 ^property[2].code = #child 
* #AN06 ^property[2].valueCode = #AN0602 
* #AN0601 "AN Denervation periphere Nerven"
* #AN0601 ^property[0].code = #parent 
* #AN0601 ^property[0].valueCode = #AN06 
* #AN0602 "AN Denervation vegetative Ganglien"
* #AN0602 ^property[0].code = #parent 
* #AN0602 ^property[0].valueCode = #AN06 
* #AN07 "AN Diverse Eingriffe/Techniken (sonst nicht erfassbar)"
* #AN07 ^property[0].code = #parent 
* #AN07 ^property[0].valueCode = #AN 
* #AN07 ^property[1].code = #child 
* #AN07 ^property[1].valueCode = #AN0700 
* #AN07 ^property[2].code = #child 
* #AN07 ^property[2].valueCode = #AN0701 
* #AN07 ^property[3].code = #child 
* #AN07 ^property[3].valueCode = #AN0702 
* #AN07 ^property[4].code = #child 
* #AN07 ^property[4].valueCode = #AN0703 
* #AN07 ^property[5].code = #child 
* #AN07 ^property[5].valueCode = #AN0704 
* #AN07 ^property[6].code = #child 
* #AN07 ^property[6].valueCode = #AN0705 
* #AN07 ^property[7].code = #child 
* #AN07 ^property[7].valueCode = #AN0706 
* #AN07 ^property[8].code = #child 
* #AN07 ^property[8].valueCode = #AN0707 
* #AN07 ^property[9].code = #child 
* #AN07 ^property[9].valueCode = #AN0708 
* #AN07 ^property[10].code = #child 
* #AN07 ^property[10].valueCode = #AN0709 
* #AN07 ^property[11].code = #child 
* #AN07 ^property[11].valueCode = #AN0710 
* #AN07 ^property[12].code = #child 
* #AN07 ^property[12].valueCode = #AN0711 
* #AN07 ^property[13].code = #child 
* #AN07 ^property[13].valueCode = #AN0712 
* #AN07 ^property[14].code = #child 
* #AN07 ^property[14].valueCode = #AN0713 
* #AN07 ^property[15].code = #child 
* #AN07 ^property[15].valueCode = #AN0714 
* #AN07 ^property[16].code = #child 
* #AN07 ^property[16].valueCode = #AN0715 
* #AN07 ^property[17].code = #child 
* #AN07 ^property[17].valueCode = #AN0716 
* #AN07 ^property[18].code = #child 
* #AN07 ^property[18].valueCode = #AN0717 
* #AN07 ^property[19].code = #child 
* #AN07 ^property[19].valueCode = #AN0718 
* #AN07 ^property[20].code = #child 
* #AN07 ^property[20].valueCode = #AN0719 
* #AN07 ^property[21].code = #child 
* #AN07 ^property[21].valueCode = #AN0720 
* #AN07 ^property[22].code = #child 
* #AN07 ^property[22].valueCode = #AN0721 
* #AN07 ^property[23].code = #child 
* #AN07 ^property[23].valueCode = #AN0722 
* #AN0700 "AN Intravaskuläre Lithotripsie (IVL) peripher"
* #AN0700 ^property[0].code = #parent 
* #AN0700 ^property[0].valueCode = #AN07 
* #AN0701 "AN Rotablation li."
* #AN0701 ^property[0].code = #parent 
* #AN0701 ^property[0].valueCode = #AN07 
* #AN0702 "AN Rotablation re."
* #AN0702 ^property[0].code = #parent 
* #AN0702 ^property[0].valueCode = #AN07 
* #AN0703 "AN Laser li."
* #AN0703 ^property[0].code = #parent 
* #AN0703 ^property[0].valueCode = #AN07 
* #AN0704 "AN Laser re."
* #AN0704 ^property[0].code = #parent 
* #AN0704 ^property[0].valueCode = #AN07 
* #AN0705 "AN Angio und Lyse periphere Gefäße (arteriell und venös?)"
* #AN0705 ^property[0].code = #parent 
* #AN0705 ^property[0].valueCode = #AN07 
* #AN0706 "AN Aspiration/Thrombektomie"
* #AN0706 ^property[0].code = #parent 
* #AN0706 ^property[0].valueCode = #AN07 
* #AN0707 "AN PTA Diverses  (z.B. venös)"
* #AN0707 ^property[0].code = #parent 
* #AN0707 ^property[0].valueCode = #AN07 
* #AN0708 "AN Rekanalisation Diverses (z.B. venös)"
* #AN0708 ^property[0].code = #parent 
* #AN0708 ^property[0].valueCode = #AN07 
* #AN0709 "AN Stent Diverses  (z.B. venös)"
* #AN0709 ^property[0].code = #parent 
* #AN0709 ^property[0].valueCode = #AN07 
* #AN0710 "AN Thermoablation/Kryoablation"
* #AN0710 ^property[0].code = #parent 
* #AN0710 ^property[0].valueCode = #AN07 
* #AN0711 "AN TIPPS"
* #AN0711 ^property[0].code = #parent 
* #AN0711 ^property[0].valueCode = #AN07 
* #AN0712 "AN Organbiopsie unter Durchleuchtung"
* #AN0712 ^property[0].code = #parent 
* #AN0712 ^property[0].valueCode = #AN07 
* #AN0713 "AN Präoperative Markierung unter Durchleuchtung"
* #AN0713 ^property[0].code = #parent 
* #AN0713 ^property[0].valueCode = #AN07 
* #AN0714 "AN Drainage unter Durchleuchtung"
* #AN0714 ^property[0].code = #parent 
* #AN0714 ^property[0].valueCode = #AN07 
* #AN0715 "AN Intervention Kontrolle"
* #AN0715 ^property[0].code = #parent 
* #AN0715 ^property[0].valueCode = #AN07 
* #AN0716 "AN Intervention Drainage"
* #AN0716 ^property[0].code = #parent 
* #AN0716 ^property[0].valueCode = #AN07 
* #AN0717 "AN Intervention Embolisation"
* #AN0717 ^property[0].code = #parent 
* #AN0717 ^property[0].valueCode = #AN07 
* #AN0718 "AN Port-a-Cath Implantation"
* #AN0718 ^property[0].code = #parent 
* #AN0718 ^property[0].valueCode = #AN07 
* #AN0719 "AN Port-a-Cath Lagekontrolle"
* #AN0719 ^property[0].code = #parent 
* #AN0719 ^property[0].valueCode = #AN07 
* #AN0720 "AN Intravasale FK Entfernung"
* #AN0720 ^property[0].code = #parent 
* #AN0720 ^property[0].valueCode = #AN07 
* #AN0721 "AN Diskographie"
* #AN0721 ^property[0].code = #parent 
* #AN0721 ^property[0].valueCode = #AN07 
* #AN0722 "AN Intervention Kontrolle (Wundverband, etc.)"
* #AN0722 ^property[0].code = #parent 
* #AN0722 ^property[0].valueCode = #AN07 
* #AN08 "AN Gallenwege"
* #AN08 ^property[0].code = #parent 
* #AN08 ^property[0].valueCode = #AN 
* #AN08 ^property[1].code = #child 
* #AN08 ^property[1].valueCode = #AN0801 
* #AN08 ^property[2].code = #child 
* #AN08 ^property[2].valueCode = #AN0802 
* #AN0801 "AN Gallenwege diagnostisch (PTC)"
* #AN0801 ^property[0].code = #parent 
* #AN0801 ^property[0].valueCode = #AN08 
* #AN0802 "AN Intervention Gallenwege"
* #AN0802 ^property[0].code = #parent 
* #AN0802 ^property[0].valueCode = #AN08 
* #AN0802 ^property[1].code = #child 
* #AN0802 ^property[1].valueCode = #AN080201 
* #AN0802 ^property[2].code = #child 
* #AN0802 ^property[2].valueCode = #AN080202 
* #AN0802 ^property[3].code = #child 
* #AN0802 ^property[3].valueCode = #AN080203 
* #AN0802 ^property[4].code = #child 
* #AN0802 ^property[4].valueCode = #AN080204 
* #AN0802 ^property[5].code = #child 
* #AN0802 ^property[5].valueCode = #AN080205 
* #AN0802 ^property[6].code = #child 
* #AN0802 ^property[6].valueCode = #AN080207 
* #AN0802 ^property[7].code = #child 
* #AN0802 ^property[7].valueCode = #AN080208 
* #AN0802 ^property[8].code = #child 
* #AN0802 ^property[8].valueCode = #AN080209 
* #AN080201 "AN PTC Drainage"
* #AN080201 ^property[0].code = #parent 
* #AN080201 ^property[0].valueCode = #AN0802 
* #AN080202 "AN Cholangioscopie"
* #AN080202 ^property[0].code = #parent 
* #AN080202 ^property[0].valueCode = #AN0802 
* #AN080203 "AN Steinextraktion"
* #AN080203 ^property[0].code = #parent 
* #AN080203 ^property[0].valueCode = #AN0802 
* #AN080204 "AN ERCP Stent (endoskopisch)"
* #AN080204 ^property[0].code = #parent 
* #AN080204 ^property[0].valueCode = #AN0802 
* #AN080205 "AN perkutaner Stent Gallenwege"
* #AN080205 ^property[0].code = #parent 
* #AN080205 ^property[0].valueCode = #AN0802 
* #AN080207 "AN Drainage Lagekontrolle"
* #AN080207 ^property[0].code = #parent 
* #AN080207 ^property[0].valueCode = #AN0802 
* #AN080208 "AN Drainage Wechsel"
* #AN080208 ^property[0].code = #parent 
* #AN080208 ^property[0].valueCode = #AN0802 
* #AN080209 "AN Gallenwege Diverses"
* #AN080209 ^property[0].code = #parent 
* #AN080209 ^property[0].valueCode = #AN0802 
* #AN09 "AN Phlebographie"
* #AN09 ^property[0].code = #parent 
* #AN09 ^property[0].valueCode = #AN 
* #AN09 ^property[1].code = #child 
* #AN09 ^property[1].valueCode = #AN0901 
* #AN09 ^property[2].code = #child 
* #AN09 ^property[2].valueCode = #AN0902 
* #AN09 ^property[3].code = #child 
* #AN09 ^property[3].valueCode = #AN0903 
* #AN09 ^property[4].code = #child 
* #AN09 ^property[4].valueCode = #AN0904 
* #AN09 ^property[5].code = #child 
* #AN09 ^property[5].valueCode = #AN0905 
* #AN09 ^property[6].code = #child 
* #AN09 ^property[6].valueCode = #AN0906 
* #AN09 ^property[7].code = #child 
* #AN09 ^property[7].valueCode = #AN0907 
* #AN09 ^property[8].code = #child 
* #AN09 ^property[8].valueCode = #AN0908 
* #AN0901 "AN Phlebo Bein"
* #AN0901 ^property[0].code = #parent 
* #AN0901 ^property[0].valueCode = #AN09 
* #AN0902 "AN Phlebo Arm"
* #AN0902 ^property[0].code = #parent 
* #AN0902 ^property[0].valueCode = #AN09 
* #AN0903 "AN Phlebo V. Spermatica"
* #AN0903 ^property[0].code = #parent 
* #AN0903 ^property[0].valueCode = #AN09 
* #AN0904 "AN Cavographie"
* #AN0904 ^property[0].code = #parent 
* #AN0904 ^property[0].valueCode = #AN09 
* #AN0905 "AN Varicographie"
* #AN0905 ^property[0].code = #parent 
* #AN0905 ^property[0].valueCode = #AN09 
* #AN0906 "AN Nierenvenensampling"
* #AN0906 ^property[0].code = #parent 
* #AN0906 ^property[0].valueCode = #AN09 
* #AN0907 "AN Cavernosographie"
* #AN0907 ^property[0].code = #parent 
* #AN0907 ^property[0].valueCode = #AN09 
* #AN0908 "AN Phlebo Diverses"
* #AN0908 ^property[0].code = #parent 
* #AN0908 ^property[0].valueCode = #AN09 
* #AN0908 ^property[1].code = #child 
* #AN0908 ^property[1].valueCode = #AN090801 
* #AN0908 ^property[2].code = #child 
* #AN0908 ^property[2].valueCode = #AN090802 
* #AN090801 "AN Lebervenenkatheter und ind. Pfortaderdruckmessung o. Leberbiopsie"
* #AN090801 ^property[0].code = #parent 
* #AN090801 ^property[0].valueCode = #AN0908 
* #AN090802 "AN Lebervenenkatheter und ind. Pfortaderdruckmessung m. Leberbiopsie"
* #AN090802 ^property[0].code = #parent 
* #AN090802 ^property[0].valueCode = #AN0908 
* #AN10 "AN GI Trakt"
* #AN10 ^property[0].code = #parent 
* #AN10 ^property[0].valueCode = #AN 
* #AN10 ^property[1].code = #child 
* #AN10 ^property[1].valueCode = #AN1001 
* #AN10 ^property[2].code = #child 
* #AN10 ^property[2].valueCode = #AN1002 
* #AN10 ^property[3].code = #child 
* #AN10 ^property[3].valueCode = #AN1003 
* #AN10 ^property[4].code = #child 
* #AN10 ^property[4].valueCode = #AN1004 
* #AN10 ^property[5].code = #child 
* #AN10 ^property[5].valueCode = #AN1005 
* #AN10 ^property[6].code = #child 
* #AN10 ^property[6].valueCode = #AN1006 
* #AN1001 "AN PEG-Sonde"
* #AN1001 ^property[0].code = #parent 
* #AN1001 ^property[0].valueCode = #AN10 
* #AN1002 "AN PEG-Sonde Lagekontrolle"
* #AN1002 ^property[0].code = #parent 
* #AN1002 ^property[0].valueCode = #AN10 
* #AN1003 "AN Stent Ösophagus"
* #AN1003 ^property[0].code = #parent 
* #AN1003 ^property[0].valueCode = #AN10 
* #AN1004 "AN Stent Magen, Duodenum"
* #AN1004 ^property[0].code = #parent 
* #AN1004 ^property[0].valueCode = #AN10 
* #AN1005 "AN Stent Dünndarm"
* #AN1005 ^property[0].code = #parent 
* #AN1005 ^property[0].valueCode = #AN10 
* #AN1006 "AN Stent Dickdarm"
* #AN1006 ^property[0].code = #parent 
* #AN1006 ^property[0].valueCode = #AN10 
* #AN11 "AN i.v. DSA"
* #AN11 ^property[0].code = #parent 
* #AN11 ^property[0].valueCode = #AN 
* #AN11 ^property[1].code = #child 
* #AN11 ^property[1].valueCode = #AN1101 
* #AN11 ^property[2].code = #child 
* #AN11 ^property[2].valueCode = #AN1102 
* #AN11 ^property[3].code = #child 
* #AN11 ^property[3].valueCode = #AN1103 
* #AN11 ^property[4].code = #child 
* #AN11 ^property[4].valueCode = #AN1104 
* #AN11 ^property[5].code = #child 
* #AN11 ^property[5].valueCode = #AN1105 
* #AN11 ^property[6].code = #child 
* #AN11 ^property[6].valueCode = #AN1106 
* #AN11 ^property[7].code = #child 
* #AN11 ^property[7].valueCode = #AN1107 
* #AN1101 "AN i.v. DSA Kopf, Hals"
* #AN1101 ^property[0].code = #parent 
* #AN1101 ^property[0].valueCode = #AN11 
* #AN1102 "AN i.v. DSA Aorta"
* #AN1102 ^property[0].code = #parent 
* #AN1102 ^property[0].valueCode = #AN11 
* #AN1103 "AN i.v. DSA Thorax"
* #AN1103 ^property[0].code = #parent 
* #AN1103 ^property[0].valueCode = #AN11 
* #AN1104 "AN i.v. DSA Abdomen"
* #AN1104 ^property[0].code = #parent 
* #AN1104 ^property[0].valueCode = #AN11 
* #AN1105 "AN i.v. DSA Becken"
* #AN1105 ^property[0].code = #parent 
* #AN1105 ^property[0].valueCode = #AN11 
* #AN1106 "AN i.v. DSA obere Extremität"
* #AN1106 ^property[0].code = #parent 
* #AN1106 ^property[0].valueCode = #AN11 
* #AN1107 "AN i.v. DSA Untere Extrem."
* #AN1107 ^property[0].code = #parent 
* #AN1107 ^property[0].valueCode = #AN11 
* #CA "CA Coronarangiographie"
* #CA ^property[0].code = #child 
* #CA ^property[0].valueCode = #CA01 
* #CA ^property[1].code = #child 
* #CA ^property[1].valueCode = #CA02 
* #CA01 "CA Diagnostische Katheterangiographie"
* #CA01 ^property[0].code = #parent 
* #CA01 ^property[0].valueCode = #CA 
* #CA01 ^property[1].code = #child 
* #CA01 ^property[1].valueCode = #CA0101 
* #CA01 ^property[2].code = #child 
* #CA01 ^property[2].valueCode = #CA0102 
* #CA0101 "CA Akute Koronarangiographie"
* #CA0101 ^property[0].code = #parent 
* #CA0101 ^property[0].valueCode = #CA01 
* #CA0102 "CA Elektive Koronarangiographie"
* #CA0102 ^property[0].code = #parent 
* #CA0102 ^property[0].valueCode = #CA01 
* #CA02 "CA Intervention"
* #CA02 ^property[0].code = #parent 
* #CA02 ^property[0].valueCode = #CA 
* #CA02 ^property[1].code = #child 
* #CA02 ^property[1].valueCode = #CA0201 
* #CA02 ^property[2].code = #child 
* #CA02 ^property[2].valueCode = #CA0202 
* #CA02 ^property[3].code = #child 
* #CA02 ^property[3].valueCode = #CA0203 
* #CA02 ^property[4].code = #child 
* #CA02 ^property[4].valueCode = #CA0204 
* #CA02 ^property[5].code = #child 
* #CA02 ^property[5].valueCode = #CA0205 
* #CA02 ^property[6].code = #child 
* #CA02 ^property[6].valueCode = #CA0206 
* #CA02 ^property[7].code = #child 
* #CA02 ^property[7].valueCode = #CA0207 
* #CA02 ^property[8].code = #child 
* #CA02 ^property[8].valueCode = #CA0208 
* #CA02 ^property[9].code = #child 
* #CA02 ^property[9].valueCode = #CA0209 
* #CA02 ^property[10].code = #child 
* #CA02 ^property[10].valueCode = #CA0210 
* #CA02 ^property[11].code = #child 
* #CA02 ^property[11].valueCode = #CA0211 
* #CA02 ^property[12].code = #child 
* #CA02 ^property[12].valueCode = #CA0212 
* #CA02 ^property[13].code = #child 
* #CA02 ^property[13].valueCode = #CA0213 
* #CA02 ^property[14].code = #child 
* #CA02 ^property[14].valueCode = #CA0214 
* #CA02 ^property[15].code = #child 
* #CA02 ^property[15].valueCode = #CA0215 
* #CA02 ^property[16].code = #child 
* #CA02 ^property[16].valueCode = #CA0216 
* #CA02 ^property[17].code = #child 
* #CA02 ^property[17].valueCode = #CA0217 
* #CA02 ^property[18].code = #child 
* #CA02 ^property[18].valueCode = #CA0218 
* #CA02 ^property[19].code = #child 
* #CA02 ^property[19].valueCode = #CA0219 
* #CA02 ^property[20].code = #child 
* #CA02 ^property[20].valueCode = #CA0222 
* #CA02 ^property[21].code = #child 
* #CA02 ^property[21].valueCode = #CA0223 
* #CA02 ^property[22].code = #child 
* #CA02 ^property[22].valueCode = #CA0224 
* #CA02 ^property[23].code = #child 
* #CA02 ^property[23].valueCode = #CA0226 
* #CA02 ^property[24].code = #child 
* #CA02 ^property[24].valueCode = #CA0227 
* #CA02 ^property[25].code = #child 
* #CA02 ^property[25].valueCode = #CA0228 
* #CA02 ^property[26].code = #child 
* #CA02 ^property[26].valueCode = #CA0229 
* #CA02 ^property[27].code = #child 
* #CA02 ^property[27].valueCode = #CA0230 
* #CA02 ^property[28].code = #child 
* #CA02 ^property[28].valueCode = #CA0231 
* #CA02 ^property[29].code = #child 
* #CA02 ^property[29].valueCode = #CA0232 
* #CA02 ^property[30].code = #child 
* #CA02 ^property[30].valueCode = #CA0233 
* #CA02 ^property[31].code = #child 
* #CA02 ^property[31].valueCode = #CA0234 
* #CA02 ^property[32].code = #child 
* #CA02 ^property[32].valueCode = #CA0235 
* #CA02 ^property[33].code = #child 
* #CA02 ^property[33].valueCode = #CA0236 
* #CA02 ^property[34].code = #child 
* #CA02 ^property[34].valueCode = #CA0237 
* #CA02 ^property[35].code = #child 
* #CA02 ^property[35].valueCode = #CA0238 
* #CA0201 "CA Elektive PCI"
* #CA0201 ^property[0].code = #parent 
* #CA0201 ^property[0].valueCode = #CA02 
* #CA0201 ^property[1].code = #child 
* #CA0201 ^property[1].valueCode = #CA020101 
* #CA0201 ^property[2].code = #child 
* #CA0201 ^property[2].valueCode = #CA020102 
* #CA020101 "CA PTCA-Akut"
* #CA020101 ^property[0].code = #parent 
* #CA020101 ^property[0].valueCode = #CA0201 
* #CA020102 "CA PTCA-Elektiv"
* #CA020102 ^property[0].code = #parent 
* #CA020102 ^property[0].valueCode = #CA0201 
* #CA0202 "CA PTCA mit Cutting Balloon "
* #CA0202 ^property[0].code = #parent 
* #CA0202 ^property[0].valueCode = #CA02 
* #CA0203 "CA Koronar- und Nierenarterienangiographie"
* #CA0203 ^property[0].code = #parent 
* #CA0203 ^property[0].valueCode = #CA02 
* #CA0204 "CA Intraaort.Ballonpumpe"
* #CA0204 ^property[0].code = #parent 
* #CA0204 ^property[0].valueCode = #CA02 
* #CA0205 "CA Intrakoronarer Ultraschall"
* #CA0205 ^property[0].code = #parent 
* #CA0205 ^property[0].valueCode = #CA02 
* #CA0205 ^property[1].code = #child 
* #CA0205 ^property[1].valueCode = #CA020501 
* #CA0205 ^property[2].code = #child 
* #CA0205 ^property[2].valueCode = #CA020502 
* #CA020501 "CA Intrakoronarer Ultraschall - IVUS"
* #CA020501 ^property[0].code = #parent 
* #CA020501 ^property[0].valueCode = #CA0205 
* #CA020502 "CA Intrakoronarer Ultraschall - OCT"
* #CA020502 ^property[0].code = #parent 
* #CA020502 ^property[0].valueCode = #CA0205 
* #CA0206 "CA Anlage eines temporären intrakardialen Schrittmachers"
* #CA0206 ^property[0].code = #parent 
* #CA0206 ^property[0].valueCode = #CA02 
* #CA0207 "CA Implantation eines Stents in die Koronargefäße"
* #CA0207 ^property[0].code = #parent 
* #CA0207 ^property[0].valueCode = #CA02 
* #CA0207 ^property[1].code = #child 
* #CA0207 ^property[1].valueCode = #CA020701 
* #CA0207 ^property[2].code = #child 
* #CA0207 ^property[2].valueCode = #CA020702 
* #CA0207 ^property[3].code = #child 
* #CA0207 ^property[3].valueCode = #CA020703 
* #CA020701 "CA Akute Implantation eines Stents in die Koronargefäße"
* #CA020701 ^property[0].code = #parent 
* #CA020701 ^property[0].valueCode = #CA0207 
* #CA020702 "CA Elektive Implantation eines Stents in die Koronargefäße"
* #CA020702 ^property[0].code = #parent 
* #CA020702 ^property[0].valueCode = #CA0207 
* #CA020703 "CA Implant. eines vollständig bioresorbierbaren Stents in Koronargef."
* #CA020703 ^property[0].code = #parent 
* #CA020703 ^property[0].valueCode = #CA0207 
* #CA020703 ^property[1].code = #child 
* #CA020703 ^property[1].valueCode = #CA020703a 
* #CA020703 ^property[2].code = #child 
* #CA020703 ^property[2].valueCode = #CA020703b 
* #CA020703a "CA Akute Implant. e. vollst. bioresorbierbaren Stents in Koronargef."
* #CA020703a ^property[0].code = #parent 
* #CA020703a ^property[0].valueCode = #CA020703 
* #CA020703b "CA Elek. Implant. e. vollst. bioresorbierbaren Stents in Koronargef."
* #CA020703b ^property[0].code = #parent 
* #CA020703b ^property[0].valueCode = #CA020703 
* #CA0208 "CA Implantation e. medikam.-beschichteten Stents i. d. Koronargefäße"
* #CA0208 ^property[0].code = #parent 
* #CA0208 ^property[0].valueCode = #CA02 
* #CA0208 ^property[1].code = #child 
* #CA0208 ^property[1].valueCode = #CA020801 
* #CA0208 ^property[2].code = #child 
* #CA0208 ^property[2].valueCode = #CA020802 
* #CA0208 ^property[3].code = #child 
* #CA0208 ^property[3].valueCode = #CA020803 
* #CA0208 ^property[4].code = #child 
* #CA0208 ^property[4].valueCode = #CA020804 
* #CA020801 "CA Akute Implantation DES in Koronargefäße"
* #CA020801 ^property[0].code = #parent 
* #CA020801 ^property[0].valueCode = #CA0208 
* #CA020802 "CA Elektive Implantation DES in Koronargefäße"
* #CA020802 ^property[0].code = #parent 
* #CA020802 ^property[0].valueCode = #CA0208 
* #CA020803 "CA DES bei chronischem Verschluss (CTO) antegrad"
* #CA020803 ^property[0].code = #parent 
* #CA020803 ^property[0].valueCode = #CA0208 
* #CA020804 "CA DES bei chronischem Verschluss (CTO) retrograd"
* #CA020804 ^property[0].code = #parent 
* #CA020804 ^property[0].valueCode = #CA0208 
* #CA0209 "CA Rechtsherzkatheter"
* #CA0209 ^property[0].code = #parent 
* #CA0209 ^property[0].valueCode = #CA02 
* #CA0209 ^property[1].code = #child 
* #CA0209 ^property[1].valueCode = #CA020901 
* #CA0209 ^property[2].code = #child 
* #CA0209 ^property[2].valueCode = #CA020902 
* #CA0209 ^property[3].code = #child 
* #CA0209 ^property[3].valueCode = #CA020903 
* #CA020901 "CA Rechtsherzkatheter m. HMV i. Ruhe u. mehrstufiger Belastung"
* #CA020901 ^property[0].code = #parent 
* #CA020901 ^property[0].valueCode = #CA0209 
* #CA020902 "CA Rechtsherzkatheter mit pharmakologischer Testung"
* #CA020902 ^property[0].code = #parent 
* #CA020902 ^property[0].valueCode = #CA0209 
* #CA020903 "CA Rechtsherzkatheter m. HMV in Ruhe/Belastung u. pharmak. Testung"
* #CA020903 ^property[0].code = #parent 
* #CA020903 ^property[0].valueCode = #CA0209 
* #CA0210 "CA Rot. Angioplastie - Koronargefäße"
* #CA0210 ^property[0].code = #parent 
* #CA0210 ^property[0].valueCode = #CA02 
* #CA0211 "CA Elektive Rotablation"
* #CA0211 ^property[0].code = #parent 
* #CA0211 ^property[0].valueCode = #CA02 
* #CA0211 ^property[1].code = #child 
* #CA0211 ^property[1].valueCode = #CA021101 
* #CA021101 "CA Intravaskuläre Lithotripsie (IVL) koronar"
* #CA021101 ^property[0].code = #parent 
* #CA021101 ^property[0].valueCode = #CA0211 
* #CA0212 "CA Linksherzkatheter, Invasive Abklärung angeborener kardialer Vitien"
* #CA0212 ^property[0].code = #parent 
* #CA0212 ^property[0].valueCode = #CA02 
* #CA0213 "CA PFO-/ASD-Verschluss"
* #CA0213 ^property[0].code = #parent 
* #CA0213 ^property[0].valueCode = #CA02 
* #CA0213 ^property[1].code = #child 
* #CA0213 ^property[1].valueCode = #CA021301 
* #CA0213 ^property[2].code = #child 
* #CA0213 ^property[2].valueCode = #CA021302 
* #CA0213 ^property[3].code = #child 
* #CA0213 ^property[3].valueCode = #CA021303 
* #CA0213 ^property[4].code = #child 
* #CA0213 ^property[4].valueCode = #CA021304 
* #CA0213 ^property[5].code = #child 
* #CA0213 ^property[5].valueCode = #CA021305 
* #CA0213 ^property[6].code = #child 
* #CA0213 ^property[6].valueCode = #CA021306 
* #CA0213 ^property[7].code = #child 
* #CA0213 ^property[7].valueCode = #CA021307 
* #CA021301 "CA PFO-Verschluss mit Amplatzer PFO Occluder"
* #CA021301 ^property[0].code = #parent 
* #CA021301 ^property[0].valueCode = #CA0213 
* #CA021302 "CA PFO-Verschluss mit Cardioform Septal Occluder"
* #CA021302 ^property[0].code = #parent 
* #CA021302 ^property[0].valueCode = #CA0213 
* #CA021303 "CA ASD-Verschluss mit Amplatzer ASD Occluder"
* #CA021303 ^property[0].code = #parent 
* #CA021303 ^property[0].valueCode = #CA0213 
* #CA021304 "CA ASD-Verschluss mit Cardioform Septal Occluder"
* #CA021304 ^property[0].code = #parent 
* #CA021304 ^property[0].valueCode = #CA0213 
* #CA021305 "CA ASD-Verschluss mit Cardioform ASD Occluder"
* #CA021305 ^property[0].code = #parent 
* #CA021305 ^property[0].valueCode = #CA0213 
* #CA021306 "CA Perk. Verschluss von Defekten des intrakardialen Septums (ASD)"
* #CA021306 ^property[0].code = #parent 
* #CA021306 ^property[0].valueCode = #CA0213 
* #CA021307 "CA Perk. Verschluss von Defekten des intrakardialen Septums (VSD)"
* #CA021307 ^property[0].code = #parent 
* #CA021307 ^property[0].valueCode = #CA0213 
* #CA0214 "CA Perkutane Valvuloplastie"
* #CA0214 ^property[0].code = #parent 
* #CA0214 ^property[0].valueCode = #CA02 
* #CA0214 ^property[1].code = #child 
* #CA0214 ^property[1].valueCode = #CA021401 
* #CA0214 ^property[2].code = #child 
* #CA0214 ^property[2].valueCode = #CA021402 
* #CA021401 "CA Aortenklappenvalvuplastie"
* #CA021401 ^property[0].code = #parent 
* #CA021401 ^property[0].valueCode = #CA0214 
* #CA021402 "CA Perkutane Valvuloplastie - Pulmonalklappe"
* #CA021402 ^property[0].code = #parent 
* #CA021402 ^property[0].valueCode = #CA0214 
* #CA0215 "CA Elektro-Physiologie (EP)"
* #CA0215 ^property[0].code = #parent 
* #CA0215 ^property[0].valueCode = #CA02 
* #CA0216 "CA Diagnostische Ventrikelstimulation"
* #CA0216 ^property[0].code = #parent 
* #CA0216 ^property[0].valueCode = #CA02 
* #CA0217 "CA Katheteruntersuchung kard. Reizleitungssystem"
* #CA0217 ^property[0].code = #parent 
* #CA0217 ^property[0].valueCode = #CA02 
* #CA0218 "CA Katheterablation kard. Reizleitungssystem"
* #CA0218 ^property[0].code = #parent 
* #CA0218 ^property[0].valueCode = #CA02 
* #CA0219 "CA Katheterabl. kard. Reizleitungssys. i. B. d. Pulmonalvenen"
* #CA0219 ^property[0].code = #parent 
* #CA0219 ^property[0].valueCode = #CA02 
* #CA0222 "CA intrakoronare Druckmessung (\"pressure wire\")"
* #CA0222 ^property[0].code = #parent 
* #CA0222 ^property[0].valueCode = #CA02 
* #CA0223 "CA intrakoronare Thrombusaspiration"
* #CA0223 ^property[0].code = #parent 
* #CA0223 ^property[0].valueCode = #CA02 
* #CA0224 "CA Perkutane transluminale Atherektomie einer Koronararterie"
* #CA0224 ^property[0].code = #parent 
* #CA0224 ^property[0].valueCode = #CA02 
* #CA0226 "CA Katheterabl. Kard. Reizleitungssystems i. B. d.  Ventrikel"
* #CA0226 ^property[0].code = #parent 
* #CA0226 ^property[0].valueCode = #CA02 
* #CA0227 "CA Implant. eines Mitralklappenclips - perkutan"
* #CA0227 ^property[0].code = #parent 
* #CA0227 ^property[0].valueCode = #CA02 
* #CA0228 "CA Implant. Perm. Embolieprotektionssystem in d. li. Herzohr"
* #CA0228 ^property[0].code = #parent 
* #CA0228 ^property[0].valueCode = #CA02 
* #CA0229 "CA Septale Myokardablation"
* #CA0229 ^property[0].code = #parent 
* #CA0229 ^property[0].valueCode = #CA02 
* #CA0230 "CA Linksherzkatheteruntersuchung mit pharmakologischer Testung"
* #CA0230 ^property[0].code = #parent 
* #CA0230 ^property[0].valueCode = #CA02 
* #CA0231 "CA Myokardbiopsie"
* #CA0231 ^property[0].code = #parent 
* #CA0231 ^property[0].valueCode = #CA02 
* #CA0232 "CA Ballonatrioseptostomie"
* #CA0232 ^property[0].code = #parent 
* #CA0232 ^property[0].valueCode = #CA02 
* #CA0233 "CA Distale Protektion bei koronaren Interventionen"
* #CA0233 ^property[0].code = #parent 
* #CA0233 ^property[0].valueCode = #CA02 
* #CA0234 "CA Kardioversion mit externem Kardioverter in Narkose"
* #CA0234 ^property[0].code = #parent 
* #CA0234 ^property[0].valueCode = #CA02 
* #CA0235 "CA Ersatz der Mitralklappe – kathetergestützt, transapikal"
* #CA0235 ^property[0].code = #parent 
* #CA0235 ^property[0].valueCode = #CA02 
* #CA0236 "CA Implantation einer Pulmonalklappe – perkutan"
* #CA0236 ^property[0].code = #parent 
* #CA0236 ^property[0].valueCode = #CA02 
* #CA0237 "CA Ersatz der Aortenklappe - kathetergestützt, transapikal"
* #CA0237 ^property[0].code = #parent 
* #CA0237 ^property[0].valueCode = #CA02 
* #CA0238 "CA Ersatz der Aortenklappe - kathetergestützt, transvaskulär"
* #CA0238 ^property[0].code = #parent 
* #CA0238 ^property[0].valueCode = #CA02 
* #CT "CT Computertomographie"
* #CT ^property[0].code = #child 
* #CT ^property[0].valueCode = #CT01 
* #CT ^property[1].code = #child 
* #CT ^property[1].valueCode = #CT02 
* #CT ^property[2].code = #child 
* #CT ^property[2].valueCode = #CT03 
* #CT ^property[3].code = #child 
* #CT ^property[3].valueCode = #CT04 
* #CT ^property[4].code = #child 
* #CT ^property[4].valueCode = #CT05 
* #CT ^property[5].code = #child 
* #CT ^property[5].valueCode = #CT06 
* #CT ^property[6].code = #child 
* #CT ^property[6].valueCode = #CT07 
* #CT ^property[7].code = #child 
* #CT ^property[7].valueCode = #CT08 
* #CT ^property[8].code = #child 
* #CT ^property[8].valueCode = #CT09 
* #CT ^property[9].code = #child 
* #CT ^property[9].valueCode = #CT10 
* #CT ^property[10].code = #child 
* #CT ^property[10].valueCode = #CT11 
* #CT ^property[11].code = #child 
* #CT ^property[11].valueCode = #CT12 
* #CT ^property[12].code = #child 
* #CT ^property[12].valueCode = #CT13 
* #CT ^property[13].code = #child 
* #CT ^property[13].valueCode = #CT14 
* #CT ^property[14].code = #child 
* #CT ^property[14].valueCode = #CT15 
* #CT01 "CT Schädel"
* #CT01 ^property[0].code = #parent 
* #CT01 ^property[0].valueCode = #CT 
* #CT01 ^property[1].code = #child 
* #CT01 ^property[1].valueCode = #CT0101 
* #CT01 ^property[2].code = #child 
* #CT01 ^property[2].valueCode = #CT0102 
* #CT01 ^property[3].code = #child 
* #CT01 ^property[3].valueCode = #CT0103 
* #CT0101 "CT Hirnschädel"
* #CT0101 ^property[0].code = #parent 
* #CT0101 ^property[0].valueCode = #CT01 
* #CT0102 "CT Gesichtsschädel"
* #CT0102 ^property[0].code = #parent 
* #CT0102 ^property[0].valueCode = #CT01 
* #CT0102 ^property[1].code = #child 
* #CT0102 ^property[1].valueCode = #CT010201 
* #CT0102 ^property[2].code = #child 
* #CT0102 ^property[2].valueCode = #CT010202 
* #CT0102 ^property[3].code = #child 
* #CT0102 ^property[3].valueCode = #CT010203 
* #CT0102 ^property[4].code = #child 
* #CT0102 ^property[4].valueCode = #CT010204 
* #CT0102 ^property[5].code = #child 
* #CT0102 ^property[5].valueCode = #CT010205 
* #CT0102 ^property[6].code = #child 
* #CT0102 ^property[6].valueCode = #CT010206 
* #CT0102 ^property[7].code = #child 
* #CT0102 ^property[7].valueCode = #CT010207 
* #CT0102 ^property[8].code = #child 
* #CT0102 ^property[8].valueCode = #CT010208 
* #CT0102 ^property[9].code = #child 
* #CT0102 ^property[9].valueCode = #CT010209 
* #CT010201 "CT NNH"
* #CT010201 ^property[0].code = #parent 
* #CT010201 ^property[0].valueCode = #CT0102 
* #CT010202 "CT Felsenbein"
* #CT010202 ^property[0].code = #parent 
* #CT010202 ^property[0].valueCode = #CT0102 
* #CT010203 "CT Orbita"
* #CT010203 ^property[0].code = #parent 
* #CT010203 ^property[0].valueCode = #CT0102 
* #CT010204 "CT Sella"
* #CT010204 ^property[0].code = #parent 
* #CT010204 ^property[0].valueCode = #CT0102 
* #CT010205 "CT Oberkiefer"
* #CT010205 ^property[0].code = #parent 
* #CT010205 ^property[0].valueCode = #CT0102 
* #CT010206 "CT Unterkiefer"
* #CT010206 ^property[0].code = #parent 
* #CT010206 ^property[0].valueCode = #CT0102 
* #CT010207 "CT Dental"
* #CT010207 ^property[0].code = #parent 
* #CT010207 ^property[0].valueCode = #CT0102 
* #CT010208 "CT Tränenwege"
* #CT010208 ^property[0].code = #parent 
* #CT010208 ^property[0].valueCode = #CT0102 
* #CT010209 "CT Kiefergelenk"
* #CT010209 ^property[0].code = #parent 
* #CT010209 ^property[0].valueCode = #CT0102 
* #CT0103 "CT Navigation Schädelbereich"
* #CT0103 ^property[0].code = #parent 
* #CT0103 ^property[0].valueCode = #CT01 
* #CT02 "CT Hals"
* #CT02 ^property[0].code = #parent 
* #CT02 ^property[0].valueCode = #CT 
* #CT02 ^property[1].code = #child 
* #CT02 ^property[1].valueCode = #CT0201 
* #CT0201 "CT Halsweichteile"
* #CT0201 ^property[0].code = #parent 
* #CT0201 ^property[0].valueCode = #CT02 
* #CT03 "CT Thorax"
* #CT03 ^property[0].code = #parent 
* #CT03 ^property[0].valueCode = #CT 
* #CT03 ^property[1].code = #child 
* #CT03 ^property[1].valueCode = #CT0301 
* #CT03 ^property[2].code = #child 
* #CT03 ^property[2].valueCode = #CT0302 
* #CT03 ^property[3].code = #child 
* #CT03 ^property[3].valueCode = #CT0303 
* #CT03 ^property[4].code = #child 
* #CT03 ^property[4].valueCode = #CT0304 
* #CT03 ^property[5].code = #child 
* #CT03 ^property[5].valueCode = #CT0305 
* #CT03 ^property[6].code = #child 
* #CT03 ^property[6].valueCode = #CT0306 
* #CT0301 "CT Thorax"
* #CT0301 ^property[0].code = #parent 
* #CT0301 ^property[0].valueCode = #CT03 
* #CT0302 "CT Mediastinum"
* #CT0302 ^property[0].code = #parent 
* #CT0302 ^property[0].valueCode = #CT03 
* #CT0303 "CT HR Lunge"
* #CT0303 ^property[0].code = #parent 
* #CT0303 ^property[0].valueCode = #CT03 
* #CT0304 "CT Sternum"
* #CT0304 ^property[0].code = #parent 
* #CT0304 ^property[0].valueCode = #CT03 
* #CT0305 "CT Lunge low dose"
* #CT0305 ^property[0].code = #parent 
* #CT0305 ^property[0].valueCode = #CT03 
* #CT0306 "CT Clavicula"
* #CT0306 ^property[0].code = #parent 
* #CT0306 ^property[0].valueCode = #CT03 
* #CT04 "CT Herz"
* #CT04 ^property[0].code = #parent 
* #CT04 ^property[0].valueCode = #CT 
* #CT04 ^property[1].code = #child 
* #CT04 ^property[1].valueCode = #CT0401 
* #CT04 ^property[2].code = #child 
* #CT04 ^property[2].valueCode = #CT0402 
* #CT04 ^property[3].code = #child 
* #CT04 ^property[3].valueCode = #CT0403 
* #CT0401 "CT Herz Calciumscoring"
* #CT0401 ^property[0].code = #parent 
* #CT0401 ^property[0].valueCode = #CT04 
* #CT0402 "CT Herz exklusive Coronararterien"
* #CT0402 ^property[0].code = #parent 
* #CT0402 ^property[0].valueCode = #CT04 
* #CT0403 "CT Coronarangio"
* #CT0403 ^property[0].code = #parent 
* #CT0403 ^property[0].valueCode = #CT04 
* #CT05 "CT Abdomen"
* #CT05 ^property[0].code = #parent 
* #CT05 ^property[0].valueCode = #CT 
* #CT05 ^property[1].code = #child 
* #CT05 ^property[1].valueCode = #CT0501 
* #CT05 ^property[2].code = #child 
* #CT05 ^property[2].valueCode = #CT0502 
* #CT05 ^property[3].code = #child 
* #CT05 ^property[3].valueCode = #CT0503 
* #CT05 ^property[4].code = #child 
* #CT05 ^property[4].valueCode = #CT0504 
* #CT05 ^property[5].code = #child 
* #CT05 ^property[5].valueCode = #CT0505 
* #CT05 ^property[6].code = #child 
* #CT05 ^property[6].valueCode = #CT0506 
* #CT05 ^property[7].code = #child 
* #CT05 ^property[7].valueCode = #CT0507 
* #CT05 ^property[8].code = #child 
* #CT05 ^property[8].valueCode = #CT0508 
* #CT05 ^property[9].code = #child 
* #CT05 ^property[9].valueCode = #CT0509 
* #CT05 ^property[10].code = #child 
* #CT05 ^property[10].valueCode = #CT0510 
* #CT05 ^property[11].code = #child 
* #CT05 ^property[11].valueCode = #CT0511 
* #CT05 ^property[12].code = #child 
* #CT05 ^property[12].valueCode = #CT0512 
* #CT05 ^property[13].code = #child 
* #CT05 ^property[13].valueCode = #CT0513 
* #CT0501 "CT gesamtes Abdomen / Retroperitoneum"
* #CT0501 ^property[0].code = #parent 
* #CT0501 ^property[0].valueCode = #CT05 
* #CT0502 "CT Oberbauch"
* #CT0502 ^property[0].code = #parent 
* #CT0502 ^property[0].valueCode = #CT05 
* #CT0503 "CT Leber"
* #CT0503 ^property[0].code = #parent 
* #CT0503 ^property[0].valueCode = #CT05 
* #CT0504 "CT Pankreas"
* #CT0504 ^property[0].code = #parent 
* #CT0504 ^property[0].valueCode = #CT05 
* #CT0505 "CT Nieren"
* #CT0505 ^property[0].code = #parent 
* #CT0505 ^property[0].valueCode = #CT05 
* #CT0506 "CT Nebennieren"
* #CT0506 ^property[0].code = #parent 
* #CT0506 ^property[0].valueCode = #CT05 
* #CT0507 "CT Unterbauch"
* #CT0507 ^property[0].code = #parent 
* #CT0507 ^property[0].valueCode = #CT05 
* #CT0508 "CT 3D Urogramm"
* #CT0508 ^property[0].code = #parent 
* #CT0508 ^property[0].valueCode = #CT05 
* #CT0509 "CT Harntrakt nativ / Steinserie"
* #CT0509 ^property[0].code = #parent 
* #CT0509 ^property[0].valueCode = #CT05 
* #CT0510 "CT Colonographie"
* #CT0510 ^property[0].code = #parent 
* #CT0510 ^property[0].valueCode = #CT05 
* #CT0511 "CT Enteroklysma"
* #CT0511 ^property[0].code = #parent 
* #CT0511 ^property[0].valueCode = #CT05 
* #CT0512 "CT Volumetrie"
* #CT0512 ^property[0].code = #parent 
* #CT0512 ^property[0].valueCode = #CT05 
* #CT0513 "CT Fistelfüllung"
* #CT0513 ^property[0].code = #parent 
* #CT0513 ^property[0].valueCode = #CT05 
* #CT06 "CT WS/Becken"
* #CT06 ^property[0].code = #parent 
* #CT06 ^property[0].valueCode = #CT 
* #CT06 ^property[1].code = #child 
* #CT06 ^property[1].valueCode = #CT0606 
* #CT06 ^property[2].code = #child 
* #CT06 ^property[2].valueCode = #CT0607 
* #CT06 ^property[3].code = #child 
* #CT06 ^property[3].valueCode = #CT0608 
* #CT06 ^property[4].code = #child 
* #CT06 ^property[4].valueCode = #CT0609 
* #CT06 ^property[5].code = #child 
* #CT06 ^property[5].valueCode = #CT0610 
* #CT06 ^property[6].code = #child 
* #CT06 ^property[6].valueCode = #CT0611 
* #CT0606 "CT HWS"
* #CT0606 ^property[0].code = #parent 
* #CT0606 ^property[0].valueCode = #CT06 
* #CT0607 "CT BWS"
* #CT0607 ^property[0].code = #parent 
* #CT0607 ^property[0].valueCode = #CT06 
* #CT0608 "CT LWS"
* #CT0608 ^property[0].code = #parent 
* #CT0608 ^property[0].valueCode = #CT06 
* #CT0609 "CT gesamte WS"
* #CT0609 ^property[0].code = #parent 
* #CT0609 ^property[0].valueCode = #CT06 
* #CT0610 "CT Becken"
* #CT0610 ^property[0].code = #parent 
* #CT0610 ^property[0].valueCode = #CT06 
* #CT0610 ^property[1].code = #child 
* #CT0610 ^property[1].valueCode = #CT061001 
* #CT061001 "CT SIG/Sakrum"
* #CT061001 ^property[0].code = #parent 
* #CT061001 ^property[0].valueCode = #CT0610 
* #CT0611 "CT Myelographie"
* #CT0611 ^property[0].code = #parent 
* #CT0611 ^property[0].valueCode = #CT06 
* #CT07 "CT Obere Extremität"
* #CT07 ^property[0].code = #parent 
* #CT07 ^property[0].valueCode = #CT 
* #CT07 ^property[1].code = #child 
* #CT07 ^property[1].valueCode = #CT0701 
* #CT07 ^property[2].code = #child 
* #CT07 ^property[2].valueCode = #CT0702 
* #CT07 ^property[3].code = #child 
* #CT07 ^property[3].valueCode = #CT0703 
* #CT07 ^property[4].code = #child 
* #CT07 ^property[4].valueCode = #CT0704 
* #CT07 ^property[5].code = #child 
* #CT07 ^property[5].valueCode = #CT0705 
* #CT07 ^property[6].code = #child 
* #CT07 ^property[6].valueCode = #CT0706 
* #CT07 ^property[7].code = #child 
* #CT07 ^property[7].valueCode = #CT0707 
* #CT07 ^property[8].code = #child 
* #CT07 ^property[8].valueCode = #CT0708 
* #CT07 ^property[9].code = #child 
* #CT07 ^property[9].valueCode = #CT0709 
* #CT07 ^property[10].code = #child 
* #CT07 ^property[10].valueCode = #CT0710 
* #CT07 ^property[11].code = #child 
* #CT07 ^property[11].valueCode = #CT0711 
* #CT07 ^property[12].code = #child 
* #CT07 ^property[12].valueCode = #CT0712 
* #CT07 ^property[13].code = #child 
* #CT07 ^property[13].valueCode = #CT0713 
* #CT07 ^property[14].code = #child 
* #CT07 ^property[14].valueCode = #CT0714 
* #CT07 ^property[15].code = #child 
* #CT07 ^property[15].valueCode = #CT0715 
* #CT07 ^property[16].code = #child 
* #CT07 ^property[16].valueCode = #CT0716 
* #CT07 ^property[17].code = #child 
* #CT07 ^property[17].valueCode = #CT0717 
* #CT07 ^property[18].code = #child 
* #CT07 ^property[18].valueCode = #CT0718 
* #CT0701 "CT Schulter li."
* #CT0701 ^property[0].code = #parent 
* #CT0701 ^property[0].valueCode = #CT07 
* #CT0702 "CT Schulter re."
* #CT0702 ^property[0].code = #parent 
* #CT0702 ^property[0].valueCode = #CT07 
* #CT0703 "CT Schulter bds."
* #CT0703 ^property[0].code = #parent 
* #CT0703 ^property[0].valueCode = #CT07 
* #CT0704 "CT Oberarm li."
* #CT0704 ^property[0].code = #parent 
* #CT0704 ^property[0].valueCode = #CT07 
* #CT0705 "CT Oberarm re."
* #CT0705 ^property[0].code = #parent 
* #CT0705 ^property[0].valueCode = #CT07 
* #CT0706 "CT Oberarm bds."
* #CT0706 ^property[0].code = #parent 
* #CT0706 ^property[0].valueCode = #CT07 
* #CT0707 "CT Ellbogen li."
* #CT0707 ^property[0].code = #parent 
* #CT0707 ^property[0].valueCode = #CT07 
* #CT0708 "CT Ellbogen re."
* #CT0708 ^property[0].code = #parent 
* #CT0708 ^property[0].valueCode = #CT07 
* #CT0709 "CT Ellbogen bds."
* #CT0709 ^property[0].code = #parent 
* #CT0709 ^property[0].valueCode = #CT07 
* #CT0710 "CT Unterarm li."
* #CT0710 ^property[0].code = #parent 
* #CT0710 ^property[0].valueCode = #CT07 
* #CT0711 "CT Unterarm re."
* #CT0711 ^property[0].code = #parent 
* #CT0711 ^property[0].valueCode = #CT07 
* #CT0712 "CT Unterarm bds."
* #CT0712 ^property[0].code = #parent 
* #CT0712 ^property[0].valueCode = #CT07 
* #CT0713 "CT Handgelenk li."
* #CT0713 ^property[0].code = #parent 
* #CT0713 ^property[0].valueCode = #CT07 
* #CT0714 "CT Handgelenk re."
* #CT0714 ^property[0].code = #parent 
* #CT0714 ^property[0].valueCode = #CT07 
* #CT0715 "CT Handgelenk bds."
* #CT0715 ^property[0].code = #parent 
* #CT0715 ^property[0].valueCode = #CT07 
* #CT0716 "CT Hand li."
* #CT0716 ^property[0].code = #parent 
* #CT0716 ^property[0].valueCode = #CT07 
* #CT0717 "CT Hand re."
* #CT0717 ^property[0].code = #parent 
* #CT0717 ^property[0].valueCode = #CT07 
* #CT0718 "CT Hand bds."
* #CT0718 ^property[0].code = #parent 
* #CT0718 ^property[0].valueCode = #CT07 
* #CT08 "CT Untere Extremität"
* #CT08 ^property[0].code = #parent 
* #CT08 ^property[0].valueCode = #CT 
* #CT08 ^property[1].code = #child 
* #CT08 ^property[1].valueCode = #CT0801 
* #CT08 ^property[2].code = #child 
* #CT08 ^property[2].valueCode = #CT0802 
* #CT08 ^property[3].code = #child 
* #CT08 ^property[3].valueCode = #CT0803 
* #CT08 ^property[4].code = #child 
* #CT08 ^property[4].valueCode = #CT0804 
* #CT08 ^property[5].code = #child 
* #CT08 ^property[5].valueCode = #CT0805 
* #CT08 ^property[6].code = #child 
* #CT08 ^property[6].valueCode = #CT0806 
* #CT08 ^property[7].code = #child 
* #CT08 ^property[7].valueCode = #CT0807 
* #CT08 ^property[8].code = #child 
* #CT08 ^property[8].valueCode = #CT0808 
* #CT08 ^property[9].code = #child 
* #CT08 ^property[9].valueCode = #CT0809 
* #CT08 ^property[10].code = #child 
* #CT08 ^property[10].valueCode = #CT0810 
* #CT08 ^property[11].code = #child 
* #CT08 ^property[11].valueCode = #CT0811 
* #CT08 ^property[12].code = #child 
* #CT08 ^property[12].valueCode = #CT0812 
* #CT08 ^property[13].code = #child 
* #CT08 ^property[13].valueCode = #CT0813 
* #CT08 ^property[14].code = #child 
* #CT08 ^property[14].valueCode = #CT0814 
* #CT08 ^property[15].code = #child 
* #CT08 ^property[15].valueCode = #CT0815 
* #CT08 ^property[16].code = #child 
* #CT08 ^property[16].valueCode = #CT0816 
* #CT08 ^property[17].code = #child 
* #CT08 ^property[17].valueCode = #CT0817 
* #CT08 ^property[18].code = #child 
* #CT08 ^property[18].valueCode = #CT0818 
* #CT08 ^property[19].code = #child 
* #CT08 ^property[19].valueCode = #CT0819 
* #CT08 ^property[20].code = #child 
* #CT08 ^property[20].valueCode = #CT0820 
* #CT08 ^property[21].code = #child 
* #CT08 ^property[21].valueCode = #CT0821 
* #CT08 ^property[22].code = #child 
* #CT08 ^property[22].valueCode = #CT0822 
* #CT0801 "CT Hüftgelenk li."
* #CT0801 ^property[0].code = #parent 
* #CT0801 ^property[0].valueCode = #CT08 
* #CT0802 "CT Hüftgelenk re."
* #CT0802 ^property[0].code = #parent 
* #CT0802 ^property[0].valueCode = #CT08 
* #CT0803 "CT Hüftgelenk bds."
* #CT0803 ^property[0].code = #parent 
* #CT0803 ^property[0].valueCode = #CT08 
* #CT0804 "CT Oberschenkel li."
* #CT0804 ^property[0].code = #parent 
* #CT0804 ^property[0].valueCode = #CT08 
* #CT0805 "CT Oberschenkel re."
* #CT0805 ^property[0].code = #parent 
* #CT0805 ^property[0].valueCode = #CT08 
* #CT0806 "CT Oberschenkel bds."
* #CT0806 ^property[0].code = #parent 
* #CT0806 ^property[0].valueCode = #CT08 
* #CT0807 "CT Knie li."
* #CT0807 ^property[0].code = #parent 
* #CT0807 ^property[0].valueCode = #CT08 
* #CT0808 "CT Knie re."
* #CT0808 ^property[0].code = #parent 
* #CT0808 ^property[0].valueCode = #CT08 
* #CT0809 "CT Knie bds."
* #CT0809 ^property[0].code = #parent 
* #CT0809 ^property[0].valueCode = #CT08 
* #CT0810 "CT Unterschenkel li."
* #CT0810 ^property[0].code = #parent 
* #CT0810 ^property[0].valueCode = #CT08 
* #CT0811 "CT Unterschenkel re."
* #CT0811 ^property[0].code = #parent 
* #CT0811 ^property[0].valueCode = #CT08 
* #CT0812 "CT Unterschenkel bds."
* #CT0812 ^property[0].code = #parent 
* #CT0812 ^property[0].valueCode = #CT08 
* #CT0813 "CT Sprunggelenk li."
* #CT0813 ^property[0].code = #parent 
* #CT0813 ^property[0].valueCode = #CT08 
* #CT0814 "CT Sprunggelenk re."
* #CT0814 ^property[0].code = #parent 
* #CT0814 ^property[0].valueCode = #CT08 
* #CT0815 "CT Sprunggelenk bds."
* #CT0815 ^property[0].code = #parent 
* #CT0815 ^property[0].valueCode = #CT08 
* #CT0816 "CT Fersenbein li."
* #CT0816 ^property[0].code = #parent 
* #CT0816 ^property[0].valueCode = #CT08 
* #CT0817 "CT Fersenbein re."
* #CT0817 ^property[0].code = #parent 
* #CT0817 ^property[0].valueCode = #CT08 
* #CT0818 "CT Fersenbein bds."
* #CT0818 ^property[0].code = #parent 
* #CT0818 ^property[0].valueCode = #CT08 
* #CT0819 "CT Fuß li."
* #CT0819 ^property[0].code = #parent 
* #CT0819 ^property[0].valueCode = #CT08 
* #CT0820 "CT Fuß re."
* #CT0820 ^property[0].code = #parent 
* #CT0820 ^property[0].valueCode = #CT08 
* #CT0821 "CT Fuß bds."
* #CT0821 ^property[0].code = #parent 
* #CT0821 ^property[0].valueCode = #CT08 
* #CT0822 "CT Beinlängen- und Winkelmessung"
* #CT0822 ^property[0].code = #parent 
* #CT0822 ^property[0].valueCode = #CT08 
* #CT09 "CT Angio"
* #CT09 ^property[0].code = #parent 
* #CT09 ^property[0].valueCode = #CT 
* #CT09 ^property[1].code = #child 
* #CT09 ^property[1].valueCode = #CT0901 
* #CT09 ^property[2].code = #child 
* #CT09 ^property[2].valueCode = #CT0902 
* #CT09 ^property[3].code = #child 
* #CT09 ^property[3].valueCode = #CT0903 
* #CT09 ^property[4].code = #child 
* #CT09 ^property[4].valueCode = #CT0904 
* #CT09 ^property[5].code = #child 
* #CT09 ^property[5].valueCode = #CT0905 
* #CT09 ^property[6].code = #child 
* #CT09 ^property[6].valueCode = #CT0906 
* #CT09 ^property[7].code = #child 
* #CT09 ^property[7].valueCode = #CT0907 
* #CT09 ^property[8].code = #child 
* #CT09 ^property[8].valueCode = #CT0908 
* #CT09 ^property[9].code = #child 
* #CT09 ^property[9].valueCode = #CT0909 
* #CT09 ^property[10].code = #child 
* #CT09 ^property[10].valueCode = #CT0910 
* #CT09 ^property[11].code = #child 
* #CT09 ^property[11].valueCode = #CT0911 
* #CT09 ^property[12].code = #child 
* #CT09 ^property[12].valueCode = #CT0912 
* #CT09 ^property[13].code = #child 
* #CT09 ^property[13].valueCode = #CT0913 
* #CT09 ^property[14].code = #child 
* #CT09 ^property[14].valueCode = #CT0914 
* #CT09 ^property[15].code = #child 
* #CT09 ^property[15].valueCode = #CT0915 
* #CT09 ^property[16].code = #child 
* #CT09 ^property[16].valueCode = #CT0916 
* #CT09 ^property[17].code = #child 
* #CT09 ^property[17].valueCode = #CT0917 
* #CT0901 "CTA intracranielle Gefäße"
* #CT0901 ^property[0].code = #parent 
* #CT0901 ^property[0].valueCode = #CT09 
* #CT0902 "CTA Halsgefäße"
* #CT0902 ^property[0].code = #parent 
* #CT0902 ^property[0].valueCode = #CT09 
* #CT0903 "CTA Carotis / Vertebralis"
* #CT0903 ^property[0].code = #parent 
* #CT0903 ^property[0].valueCode = #CT09 
* #CT0904 "CTA Aortenbogen"
* #CT0904 ^property[0].code = #parent 
* #CT0904 ^property[0].valueCode = #CT09 
* #CT0905 "CTA A.subclavia"
* #CT0905 ^property[0].code = #parent 
* #CT0905 ^property[0].valueCode = #CT09 
* #CT0906 "CTA Aorta thoracalis"
* #CT0906 ^property[0].code = #parent 
* #CT0906 ^property[0].valueCode = #CT09 
* #CT0907 "CTA Aorta abdominalis"
* #CT0907 ^property[0].code = #parent 
* #CT0907 ^property[0].valueCode = #CT09 
* #CT0908 "CTA gesamte Aorta und Abgänge"
* #CT0908 ^property[0].code = #parent 
* #CT0908 ^property[0].valueCode = #CT09 
* #CT0909 "CTA Pulmonalarterien"
* #CT0909 ^property[0].code = #parent 
* #CT0909 ^property[0].valueCode = #CT09 
* #CT0910 "CTA obere Extremität li."
* #CT0910 ^property[0].code = #parent 
* #CT0910 ^property[0].valueCode = #CT09 
* #CT0911 "CTA obere Extremität re."
* #CT0911 ^property[0].code = #parent 
* #CT0911 ^property[0].valueCode = #CT09 
* #CT0912 "CTA untere Extremität li."
* #CT0912 ^property[0].code = #parent 
* #CT0912 ^property[0].valueCode = #CT09 
* #CT0913 "CTA untere Extremität re."
* #CT0913 ^property[0].code = #parent 
* #CT0913 ^property[0].valueCode = #CT09 
* #CT0914 "CTA Viszeralgefäße"
* #CT0914 ^property[0].code = #parent 
* #CT0914 ^property[0].valueCode = #CT09 
* #CT0915 "CTA Becken- und Beingefäße"
* #CT0915 ^property[0].code = #parent 
* #CT0915 ^property[0].valueCode = #CT09 
* #CT0916 "CTA Perfusion"
* #CT0916 ^property[0].code = #parent 
* #CT0916 ^property[0].valueCode = #CT09 
* #CT0917 "CTA Thorax-Abdomen einschl. Aortenklappe zur TAVI-Planung"
* #CT0917 ^property[0].code = #parent 
* #CT0917 ^property[0].valueCode = #CT09 
* #CT10 "CT Phlebographie"
* #CT10 ^property[0].code = #parent 
* #CT10 ^property[0].valueCode = #CT 
* #CT10 ^property[1].code = #child 
* #CT10 ^property[1].valueCode = #CT1001 
* #CT10 ^property[2].code = #child 
* #CT10 ^property[2].valueCode = #CT1002 
* #CT10 ^property[3].code = #child 
* #CT10 ^property[3].valueCode = #CT1003 
* #CT10 ^property[4].code = #child 
* #CT10 ^property[4].valueCode = #CT1004 
* #CT10 ^property[5].code = #child 
* #CT10 ^property[5].valueCode = #CT1005 
* #CT1001 "CT Phlebographie obere Extremität li."
* #CT1001 ^property[0].code = #parent 
* #CT1001 ^property[0].valueCode = #CT10 
* #CT1002 "CT Phlebographie obere Extremität re."
* #CT1002 ^property[0].code = #parent 
* #CT1002 ^property[0].valueCode = #CT10 
* #CT1003 "CT Phlebographie untere Extremität li."
* #CT1003 ^property[0].code = #parent 
* #CT1003 ^property[0].valueCode = #CT10 
* #CT1004 "CT Phlebographie untere Extremität re."
* #CT1004 ^property[0].code = #parent 
* #CT1004 ^property[0].valueCode = #CT10 
* #CT1005 "CT Phlebographie Viszeralgefäße"
* #CT1005 ^property[0].code = #parent 
* #CT1005 ^property[0].valueCode = #CT10 
* #CT11 "CT Intervention"
* #CT11 ^property[0].code = #parent 
* #CT11 ^property[0].valueCode = #CT 
* #CT11 ^property[1].code = #child 
* #CT11 ^property[1].valueCode = #CT1101 
* #CT11 ^property[2].code = #child 
* #CT11 ^property[2].valueCode = #CT1102 
* #CT11 ^property[3].code = #child 
* #CT11 ^property[3].valueCode = #CT1103 
* #CT11 ^property[4].code = #child 
* #CT11 ^property[4].valueCode = #CT1104 
* #CT11 ^property[5].code = #child 
* #CT11 ^property[5].valueCode = #CT1105 
* #CT11 ^property[6].code = #child 
* #CT11 ^property[6].valueCode = #CT1106 
* #CT11 ^property[7].code = #child 
* #CT11 ^property[7].valueCode = #CT1107 
* #CT11 ^property[8].code = #child 
* #CT11 ^property[8].valueCode = #CT1108 
* #CT11 ^property[9].code = #child 
* #CT11 ^property[9].valueCode = #CT1109 
* #CT11 ^property[10].code = #child 
* #CT11 ^property[10].valueCode = #CT1110 
* #CT11 ^property[11].code = #child 
* #CT11 ^property[11].valueCode = #CT1111 
* #CT11 ^property[12].code = #child 
* #CT11 ^property[12].valueCode = #CT1112 
* #CT1101 "CT Infiltration gezielt"
* #CT1101 ^property[0].code = #parent 
* #CT1101 ^property[0].valueCode = #CT11 
* #CT1102 "CT Wurzelblockade gezielt"
* #CT1102 ^property[0].code = #parent 
* #CT1102 ^property[0].valueCode = #CT11 
* #CT1103 "CT Drainage gezielt "
* #CT1103 ^property[0].code = #parent 
* #CT1103 ^property[0].valueCode = #CT11 
* #CT1104 "CT Punktion gezielt"
* #CT1104 ^property[0].code = #parent 
* #CT1104 ^property[0].valueCode = #CT11 
* #CT1105 "CT Biopsie gezielt"
* #CT1105 ^property[0].code = #parent 
* #CT1105 ^property[0].valueCode = #CT11 
* #CT1106 "CT Drain Kontrolle"
* #CT1106 ^property[0].code = #parent 
* #CT1106 ^property[0].valueCode = #CT11 
* #CT1107 "CT Alkoholinstillation gezielt"
* #CT1107 ^property[0].code = #parent 
* #CT1107 ^property[0].valueCode = #CT11 
* #CT1108 "CT Radiofrequenzablation gezielt"
* #CT1108 ^property[0].code = #parent 
* #CT1108 ^property[0].valueCode = #CT11 
* #CT1108 ^property[1].code = #child 
* #CT1108 ^property[1].valueCode = #CT110801 
* #CT1108 ^property[2].code = #child 
* #CT1108 ^property[2].valueCode = #CT110802 
* #CT1108 ^property[3].code = #child 
* #CT1108 ^property[3].valueCode = #CT110803 
* #CT110801 "CT Radiofrequenzablation Leber gezielt"
* #CT110801 ^property[0].code = #parent 
* #CT110801 ^property[0].valueCode = #CT1108 
* #CT110802 "CT Radiofrequenzablation Knochen gezielt"
* #CT110802 ^property[0].code = #parent 
* #CT110802 ^property[0].valueCode = #CT1108 
* #CT110803 "CT Radiofrequenzablation Ösophagus gezielt"
* #CT110803 ^property[0].code = #parent 
* #CT110803 ^property[0].valueCode = #CT1108 
* #CT1109 "CT Vertebroplastie gezielt"
* #CT1109 ^property[0].code = #parent 
* #CT1109 ^property[0].valueCode = #CT11 
* #CT1110 "CT_Kyphoplastie gezielt"
* #CT1110 ^property[0].code = #parent 
* #CT1110 ^property[0].valueCode = #CT11 
* #CT1111 "CT Arthrographie"
* #CT1111 ^property[0].code = #parent 
* #CT1111 ^property[0].valueCode = #CT11 
* #CT1112 "CT Präoperative Markierung gezielt"
* #CT1112 ^property[0].code = #parent 
* #CT1112 ^property[0].valueCode = #CT11 
* #CT12 "CT Navigation"
* #CT12 ^property[0].code = #parent 
* #CT12 ^property[0].valueCode = #CT 
* #CT13 "CT Osteodensitometrie"
* #CT13 ^property[0].code = #parent 
* #CT13 ^property[0].valueCode = #CT 
* #CT14 "CT low-dose Ganzkörper"
* #CT14 ^property[0].code = #parent 
* #CT14 ^property[0].valueCode = #CT 
* #CT14 ^property[1].code = #child 
* #CT14 ^property[1].valueCode = #CT1401 
* #CT1401 "CT Ganzkörper Multiples Myelom"
* #CT1401 ^property[0].code = #parent 
* #CT1401 ^property[0].valueCode = #CT14 
* #CT15 "CT Körperstamm"
* #CT15 ^property[0].code = #parent 
* #CT15 ^property[0].valueCode = #CT 
* #CT15 ^property[1].code = #child 
* #CT15 ^property[1].valueCode = #CT1501 
* #CT1501 "CT Thorax / Abdomen"
* #CT1501 ^property[0].code = #parent 
* #CT1501 ^property[0].valueCode = #CT15 
* #EK "EK EKG"
* #EK ^property[0].code = #child 
* #EK ^property[0].valueCode = #EK01 
* #EK ^property[1].code = #child 
* #EK ^property[1].valueCode = #EK02 
* #EK ^property[2].code = #child 
* #EK ^property[2].valueCode = #EK03 
* #EK ^property[3].code = #child 
* #EK ^property[3].valueCode = #EK04 
* #EK01 "EK Routine-EKG"
* #EK01 ^property[0].code = #parent 
* #EK01 ^property[0].valueCode = #EK 
* #EK02 "EK Langzeit-EKG"
* #EK02 ^property[0].code = #parent 
* #EK02 ^property[0].valueCode = #EK 
* #EK03 "EK Event Recording"
* #EK03 ^property[0].code = #parent 
* #EK03 ^property[0].valueCode = #EK 
* #EK04 "EK Kardiales Monitoring (EKG, Blutdruck, Oxymetrie)"
* #EK04 ^property[0].code = #parent 
* #EK04 ^property[0].valueCode = #EK 
* #MR "MR Magnetresonanz"
* #MR ^property[0].code = #child 
* #MR ^property[0].valueCode = #MR01 
* #MR ^property[1].code = #child 
* #MR ^property[1].valueCode = #MR02 
* #MR ^property[2].code = #child 
* #MR ^property[2].valueCode = #MR03 
* #MR ^property[3].code = #child 
* #MR ^property[3].valueCode = #MR04 
* #MR ^property[4].code = #child 
* #MR ^property[4].valueCode = #MR05 
* #MR ^property[5].code = #child 
* #MR ^property[5].valueCode = #MR06 
* #MR ^property[6].code = #child 
* #MR ^property[6].valueCode = #MR07 
* #MR ^property[7].code = #child 
* #MR ^property[7].valueCode = #MR08 
* #MR ^property[8].code = #child 
* #MR ^property[8].valueCode = #MR09 
* #MR ^property[9].code = #child 
* #MR ^property[9].valueCode = #MR10 
* #MR ^property[10].code = #child 
* #MR ^property[10].valueCode = #MR11 
* #MR ^property[11].code = #child 
* #MR ^property[11].valueCode = #MR12 
* #MR ^property[12].code = #child 
* #MR ^property[12].valueCode = #MR13 
* #MR ^property[13].code = #child 
* #MR ^property[13].valueCode = #MR14 
* #MR ^property[14].code = #child 
* #MR ^property[14].valueCode = #MR15 
* #MR01 "MR Schädel"
* #MR01 ^property[0].code = #parent 
* #MR01 ^property[0].valueCode = #MR 
* #MR01 ^property[1].code = #child 
* #MR01 ^property[1].valueCode = #MR0101 
* #MR01 ^property[2].code = #child 
* #MR01 ^property[2].valueCode = #MR0102 
* #MR01 ^property[3].code = #child 
* #MR01 ^property[3].valueCode = #MR0103 
* #MR01 ^property[4].code = #child 
* #MR01 ^property[4].valueCode = #MR0104 
* #MR01 ^property[5].code = #child 
* #MR01 ^property[5].valueCode = #MR0105 
* #MR01 ^property[6].code = #child 
* #MR01 ^property[6].valueCode = #MR0106 
* #MR01 ^property[7].code = #child 
* #MR01 ^property[7].valueCode = #MR0107 
* #MR01 ^property[8].code = #child 
* #MR01 ^property[8].valueCode = #MR0108 
* #MR01 ^property[9].code = #child 
* #MR01 ^property[9].valueCode = #MR0109 
* #MR01 ^property[10].code = #child 
* #MR01 ^property[10].valueCode = #MR0110 
* #MR01 ^property[11].code = #child 
* #MR01 ^property[11].valueCode = #MR0111 
* #MR01 ^property[12].code = #child 
* #MR01 ^property[12].valueCode = #MR0112 
* #MR01 ^property[13].code = #child 
* #MR01 ^property[13].valueCode = #MR0113 
* #MR01 ^property[14].code = #child 
* #MR01 ^property[14].valueCode = #MR0114 
* #MR01 ^property[15].code = #child 
* #MR01 ^property[15].valueCode = #MR0115 
* #MR0101 "MR Schädel"
* #MR0101 ^property[0].code = #parent 
* #MR0101 ^property[0].valueCode = #MR01 
* #MR0102 "MR Schädel Perfusion"
* #MR0102 ^property[0].code = #parent 
* #MR0102 ^property[0].valueCode = #MR01 
* #MR0103 "MR Schädel Spektroskopie"
* #MR0103 ^property[0].code = #parent 
* #MR0103 ^property[0].valueCode = #MR01 
* #MR0104 "MR Schädel Flussquantifizierung"
* #MR0104 ^property[0].code = #parent 
* #MR0104 ^property[0].valueCode = #MR01 
* #MR0105 "MR Temporallappen"
* #MR0105 ^property[0].code = #parent 
* #MR0105 ^property[0].valueCode = #MR01 
* #MR0106 "MR Hypophyse"
* #MR0106 ^property[0].code = #parent 
* #MR0106 ^property[0].valueCode = #MR01 
* #MR0107 "MR Kleinhirnbrueckenwinkel"
* #MR0107 ^property[0].code = #parent 
* #MR0107 ^property[0].valueCode = #MR01 
* #MR0108 "MR Gesichtsschaedel"
* #MR0108 ^property[0].code = #parent 
* #MR0108 ^property[0].valueCode = #MR01 
* #MR0109 "MR Orbita"
* #MR0109 ^property[0].code = #parent 
* #MR0109 ^property[0].valueCode = #MR01 
* #MR0110 "MR NNH"
* #MR0110 ^property[0].code = #parent 
* #MR0110 ^property[0].valueCode = #MR01 
* #MR0111 "MR Kiefergelenke"
* #MR0111 ^property[0].code = #parent 
* #MR0111 ^property[0].valueCode = #MR01 
* #MR0112 "MR fMRI (funktionell)"
* #MR0112 ^property[0].code = #parent 
* #MR0112 ^property[0].valueCode = #MR01 
* #MR0113 "MR Schädel Navigation"
* #MR0113 ^property[0].code = #parent 
* #MR0113 ^property[0].valueCode = #MR01 
* #MR0114 "MR Diffusion"
* #MR0114 ^property[0].code = #parent 
* #MR0114 ^property[0].valueCode = #MR01 
* #MR0115 "MR DTI"
* #MR0115 ^property[0].code = #parent 
* #MR0115 ^property[0].valueCode = #MR01 
* #MR02 "MR WS / Becken"
* #MR02 ^property[0].code = #parent 
* #MR02 ^property[0].valueCode = #MR 
* #MR02 ^property[1].code = #child 
* #MR02 ^property[1].valueCode = #MR0201 
* #MR02 ^property[2].code = #child 
* #MR02 ^property[2].valueCode = #MR0202 
* #MR02 ^property[3].code = #child 
* #MR02 ^property[3].valueCode = #MR0203 
* #MR02 ^property[4].code = #child 
* #MR02 ^property[4].valueCode = #MR0204 
* #MR02 ^property[5].code = #child 
* #MR02 ^property[5].valueCode = #MR0205 
* #MR02 ^property[6].code = #child 
* #MR02 ^property[6].valueCode = #MR0206 
* #MR02 ^property[7].code = #child 
* #MR02 ^property[7].valueCode = #MR0207 
* #MR02 ^property[8].code = #child 
* #MR02 ^property[8].valueCode = #MR0208 
* #MR0201 "MR HWS"
* #MR0201 ^property[0].code = #parent 
* #MR0201 ^property[0].valueCode = #MR02 
* #MR0201 ^property[1].code = #child 
* #MR0201 ^property[1].valueCode = #MR020101 
* #MR020101 "MR Plexus cervicalis"
* #MR020101 ^property[0].code = #parent 
* #MR020101 ^property[0].valueCode = #MR0201 
* #MR0202 "MR BWS"
* #MR0202 ^property[0].code = #parent 
* #MR0202 ^property[0].valueCode = #MR02 
* #MR0203 "MR LWS"
* #MR0203 ^property[0].code = #parent 
* #MR0203 ^property[0].valueCode = #MR02 
* #MR0204 "MR HWS - BWS"
* #MR0204 ^property[0].code = #parent 
* #MR0204 ^property[0].valueCode = #MR02 
* #MR0205 "MR BWS - LWS"
* #MR0205 ^property[0].code = #parent 
* #MR0205 ^property[0].valueCode = #MR02 
* #MR0206 "MR gesamte Wirbelsäule"
* #MR0206 ^property[0].code = #parent 
* #MR0206 ^property[0].valueCode = #MR02 
* #MR0207 "MR Becken"
* #MR0207 ^property[0].code = #parent 
* #MR0207 ^property[0].valueCode = #MR02 
* #MR0207 ^property[1].code = #child 
* #MR0207 ^property[1].valueCode = #MR020701 
* #MR0207 ^property[2].code = #child 
* #MR0207 ^property[2].valueCode = #MR020702 
* #MR0207 ^property[3].code = #child 
* #MR0207 ^property[3].valueCode = #MR020703 
* #MR0207 ^property[4].code = #child 
* #MR0207 ^property[4].valueCode = #MR020704 
* #MR0207 ^property[5].code = #child 
* #MR0207 ^property[5].valueCode = #MR020705 
* #MR020701 "MR knöchernes Becken"
* #MR020701 ^property[0].code = #parent 
* #MR020701 ^property[0].valueCode = #MR0207 
* #MR020702 "MR Sacrum"
* #MR020702 ^property[0].code = #parent 
* #MR020702 ^property[0].valueCode = #MR0207 
* #MR020703 "MR ISG"
* #MR020703 ^property[0].code = #parent 
* #MR020703 ^property[0].valueCode = #MR0207 
* #MR020704 "MR Plexus lumbosacralis"
* #MR020704 ^property[0].code = #parent 
* #MR020704 ^property[0].valueCode = #MR0207 
* #MR020705 "MR kleines Becken"
* #MR020705 ^property[0].code = #parent 
* #MR020705 ^property[0].valueCode = #MR0207 
* #MR0208 "MR Myelographie"
* #MR0208 ^property[0].code = #parent 
* #MR0208 ^property[0].valueCode = #MR02 
* #MR03 "MR Obere Extremität"
* #MR03 ^property[0].code = #parent 
* #MR03 ^property[0].valueCode = #MR 
* #MR03 ^property[1].code = #child 
* #MR03 ^property[1].valueCode = #MR0301 
* #MR03 ^property[2].code = #child 
* #MR03 ^property[2].valueCode = #MR0302 
* #MR03 ^property[3].code = #child 
* #MR03 ^property[3].valueCode = #MR0303 
* #MR03 ^property[4].code = #child 
* #MR03 ^property[4].valueCode = #MR0304 
* #MR03 ^property[5].code = #child 
* #MR03 ^property[5].valueCode = #MR0305 
* #MR03 ^property[6].code = #child 
* #MR03 ^property[6].valueCode = #MR0306 
* #MR03 ^property[7].code = #child 
* #MR03 ^property[7].valueCode = #MR0307 
* #MR03 ^property[8].code = #child 
* #MR03 ^property[8].valueCode = #MR0308 
* #MR03 ^property[9].code = #child 
* #MR03 ^property[9].valueCode = #MR0309 
* #MR03 ^property[10].code = #child 
* #MR03 ^property[10].valueCode = #MR0310 
* #MR03 ^property[11].code = #child 
* #MR03 ^property[11].valueCode = #MR0311 
* #MR03 ^property[12].code = #child 
* #MR03 ^property[12].valueCode = #MR0312 
* #MR03 ^property[13].code = #child 
* #MR03 ^property[13].valueCode = #MR0313 
* #MR03 ^property[14].code = #child 
* #MR03 ^property[14].valueCode = #MR0314 
* #MR03 ^property[15].code = #child 
* #MR03 ^property[15].valueCode = #MR0315 
* #MR03 ^property[16].code = #child 
* #MR03 ^property[16].valueCode = #MR0316 
* #MR03 ^property[17].code = #child 
* #MR03 ^property[17].valueCode = #MR0317 
* #MR03 ^property[18].code = #child 
* #MR03 ^property[18].valueCode = #MR0318 
* #MR03 ^property[19].code = #child 
* #MR03 ^property[19].valueCode = #MR0319 
* #MR03 ^property[20].code = #child 
* #MR03 ^property[20].valueCode = #MR0320 
* #MR03 ^property[21].code = #child 
* #MR03 ^property[21].valueCode = #MR0321 
* #MR03 ^property[22].code = #child 
* #MR03 ^property[22].valueCode = #MR0322 
* #MR03 ^property[23].code = #child 
* #MR03 ^property[23].valueCode = #MR0323 
* #MR03 ^property[24].code = #child 
* #MR03 ^property[24].valueCode = #MR0324 
* #MR03 ^property[25].code = #child 
* #MR03 ^property[25].valueCode = #MR0325 
* #MR03 ^property[26].code = #child 
* #MR03 ^property[26].valueCode = #MR0326 
* #MR03 ^property[27].code = #child 
* #MR03 ^property[27].valueCode = #MR0327 
* #MR0301 "MR Schulter li."
* #MR0301 ^property[0].code = #parent 
* #MR0301 ^property[0].valueCode = #MR03 
* #MR0302 "MR Schulter re."
* #MR0302 ^property[0].code = #parent 
* #MR0302 ^property[0].valueCode = #MR03 
* #MR0303 "MR Schulter bds."
* #MR0303 ^property[0].code = #parent 
* #MR0303 ^property[0].valueCode = #MR03 
* #MR0304 "MR Arthro Schulter li."
* #MR0304 ^property[0].code = #parent 
* #MR0304 ^property[0].valueCode = #MR03 
* #MR0305 "MR Arthro Schulter re."
* #MR0305 ^property[0].code = #parent 
* #MR0305 ^property[0].valueCode = #MR03 
* #MR0306 "MR Arthro Schulter bds."
* #MR0306 ^property[0].code = #parent 
* #MR0306 ^property[0].valueCode = #MR03 
* #MR0307 "MR Oberarm li."
* #MR0307 ^property[0].code = #parent 
* #MR0307 ^property[0].valueCode = #MR03 
* #MR0308 "MR Oberarm re."
* #MR0308 ^property[0].code = #parent 
* #MR0308 ^property[0].valueCode = #MR03 
* #MR0309 "MR Oberarm bds."
* #MR0309 ^property[0].code = #parent 
* #MR0309 ^property[0].valueCode = #MR03 
* #MR0310 "MR Ellbogen li."
* #MR0310 ^property[0].code = #parent 
* #MR0310 ^property[0].valueCode = #MR03 
* #MR0311 "MR Ellbogen re."
* #MR0311 ^property[0].code = #parent 
* #MR0311 ^property[0].valueCode = #MR03 
* #MR0312 "MR Ellbogen bds."
* #MR0312 ^property[0].code = #parent 
* #MR0312 ^property[0].valueCode = #MR03 
* #MR0313 "MR Arthro Ellbogen li."
* #MR0313 ^property[0].code = #parent 
* #MR0313 ^property[0].valueCode = #MR03 
* #MR0314 "MR Arthro Ellbogen re."
* #MR0314 ^property[0].code = #parent 
* #MR0314 ^property[0].valueCode = #MR03 
* #MR0315 "MR Arthro Ellbogen bds."
* #MR0315 ^property[0].code = #parent 
* #MR0315 ^property[0].valueCode = #MR03 
* #MR0316 "MR Unterarm li."
* #MR0316 ^property[0].code = #parent 
* #MR0316 ^property[0].valueCode = #MR03 
* #MR0317 "MR Unterarm re."
* #MR0317 ^property[0].code = #parent 
* #MR0317 ^property[0].valueCode = #MR03 
* #MR0318 "MR Unterarm bds."
* #MR0318 ^property[0].code = #parent 
* #MR0318 ^property[0].valueCode = #MR03 
* #MR0319 "MR Handgelenk li."
* #MR0319 ^property[0].code = #parent 
* #MR0319 ^property[0].valueCode = #MR03 
* #MR0320 "MR Handgelenk re."
* #MR0320 ^property[0].code = #parent 
* #MR0320 ^property[0].valueCode = #MR03 
* #MR0321 "MR Handgelenk bds."
* #MR0321 ^property[0].code = #parent 
* #MR0321 ^property[0].valueCode = #MR03 
* #MR0322 "MR Arthro Handgelenk li."
* #MR0322 ^property[0].code = #parent 
* #MR0322 ^property[0].valueCode = #MR03 
* #MR0323 "MR Arthro Handgelenk re."
* #MR0323 ^property[0].code = #parent 
* #MR0323 ^property[0].valueCode = #MR03 
* #MR0324 "MR Arthro Handgelenk bds."
* #MR0324 ^property[0].code = #parent 
* #MR0324 ^property[0].valueCode = #MR03 
* #MR0325 "MR Hand li."
* #MR0325 ^property[0].code = #parent 
* #MR0325 ^property[0].valueCode = #MR03 
* #MR0326 "MR Hand re."
* #MR0326 ^property[0].code = #parent 
* #MR0326 ^property[0].valueCode = #MR03 
* #MR0327 "MR Hand bds."
* #MR0327 ^property[0].code = #parent 
* #MR0327 ^property[0].valueCode = #MR03 
* #MR04 "MR Untere Extremität"
* #MR04 ^property[0].code = #parent 
* #MR04 ^property[0].valueCode = #MR 
* #MR04 ^property[1].code = #child 
* #MR04 ^property[1].valueCode = #MR0401 
* #MR04 ^property[2].code = #child 
* #MR04 ^property[2].valueCode = #MR0402 
* #MR04 ^property[3].code = #child 
* #MR04 ^property[3].valueCode = #MR0403 
* #MR04 ^property[4].code = #child 
* #MR04 ^property[4].valueCode = #MR0404 
* #MR04 ^property[5].code = #child 
* #MR04 ^property[5].valueCode = #MR0405 
* #MR04 ^property[6].code = #child 
* #MR04 ^property[6].valueCode = #MR0406 
* #MR04 ^property[7].code = #child 
* #MR04 ^property[7].valueCode = #MR0407 
* #MR04 ^property[8].code = #child 
* #MR04 ^property[8].valueCode = #MR0408 
* #MR04 ^property[9].code = #child 
* #MR04 ^property[9].valueCode = #MR0409 
* #MR04 ^property[10].code = #child 
* #MR04 ^property[10].valueCode = #MR0410 
* #MR04 ^property[11].code = #child 
* #MR04 ^property[11].valueCode = #MR0411 
* #MR04 ^property[12].code = #child 
* #MR04 ^property[12].valueCode = #MR0412 
* #MR04 ^property[13].code = #child 
* #MR04 ^property[13].valueCode = #MR0413 
* #MR04 ^property[14].code = #child 
* #MR04 ^property[14].valueCode = #MR0414 
* #MR04 ^property[15].code = #child 
* #MR04 ^property[15].valueCode = #MR0415 
* #MR04 ^property[16].code = #child 
* #MR04 ^property[16].valueCode = #MR0416 
* #MR04 ^property[17].code = #child 
* #MR04 ^property[17].valueCode = #MR0417 
* #MR04 ^property[18].code = #child 
* #MR04 ^property[18].valueCode = #MR0418 
* #MR04 ^property[19].code = #child 
* #MR04 ^property[19].valueCode = #MR0419 
* #MR04 ^property[20].code = #child 
* #MR04 ^property[20].valueCode = #MR0420 
* #MR04 ^property[21].code = #child 
* #MR04 ^property[21].valueCode = #MR0421 
* #MR04 ^property[22].code = #child 
* #MR04 ^property[22].valueCode = #MR0422 
* #MR04 ^property[23].code = #child 
* #MR04 ^property[23].valueCode = #MR0423 
* #MR04 ^property[24].code = #child 
* #MR04 ^property[24].valueCode = #MR0424 
* #MR04 ^property[25].code = #child 
* #MR04 ^property[25].valueCode = #MR0425 
* #MR04 ^property[26].code = #child 
* #MR04 ^property[26].valueCode = #MR0426 
* #MR04 ^property[27].code = #child 
* #MR04 ^property[27].valueCode = #MR0427 
* #MR04 ^property[28].code = #child 
* #MR04 ^property[28].valueCode = #MR0428 
* #MR04 ^property[29].code = #child 
* #MR04 ^property[29].valueCode = #MR0429 
* #MR04 ^property[30].code = #child 
* #MR04 ^property[30].valueCode = #MR0430 
* #MR0401 "MR Hüfte li."
* #MR0401 ^property[0].code = #parent 
* #MR0401 ^property[0].valueCode = #MR04 
* #MR0402 "MR Hüfte re."
* #MR0402 ^property[0].code = #parent 
* #MR0402 ^property[0].valueCode = #MR04 
* #MR0403 "MR Hüfte bds."
* #MR0403 ^property[0].code = #parent 
* #MR0403 ^property[0].valueCode = #MR04 
* #MR0404 "MR Arthro Hüfte li."
* #MR0404 ^property[0].code = #parent 
* #MR0404 ^property[0].valueCode = #MR04 
* #MR0405 "MR Arthro Hüfte re."
* #MR0405 ^property[0].code = #parent 
* #MR0405 ^property[0].valueCode = #MR04 
* #MR0406 "MR Arthro Hüfte bds."
* #MR0406 ^property[0].code = #parent 
* #MR0406 ^property[0].valueCode = #MR04 
* #MR0407 "MR Oberschenkel li."
* #MR0407 ^property[0].code = #parent 
* #MR0407 ^property[0].valueCode = #MR04 
* #MR0408 "MR Oberschenkel re."
* #MR0408 ^property[0].code = #parent 
* #MR0408 ^property[0].valueCode = #MR04 
* #MR0409 "MR Oberschenkel bds."
* #MR0409 ^property[0].code = #parent 
* #MR0409 ^property[0].valueCode = #MR04 
* #MR0410 "MR Knie li."
* #MR0410 ^property[0].code = #parent 
* #MR0410 ^property[0].valueCode = #MR04 
* #MR0411 "MR Knie re."
* #MR0411 ^property[0].code = #parent 
* #MR0411 ^property[0].valueCode = #MR04 
* #MR0412 "MR Knie bds."
* #MR0412 ^property[0].code = #parent 
* #MR0412 ^property[0].valueCode = #MR04 
* #MR0413 "MR Arthro Knie li."
* #MR0413 ^property[0].code = #parent 
* #MR0413 ^property[0].valueCode = #MR04 
* #MR0414 "MR Arthro Knie re."
* #MR0414 ^property[0].code = #parent 
* #MR0414 ^property[0].valueCode = #MR04 
* #MR0415 "MR Arthro Knie bds."
* #MR0415 ^property[0].code = #parent 
* #MR0415 ^property[0].valueCode = #MR04 
* #MR0416 "MR Unterschenkel li."
* #MR0416 ^property[0].code = #parent 
* #MR0416 ^property[0].valueCode = #MR04 
* #MR0417 "MR Unterschenkel re."
* #MR0417 ^property[0].code = #parent 
* #MR0417 ^property[0].valueCode = #MR04 
* #MR0418 "MR Unterschenkel bds."
* #MR0418 ^property[0].code = #parent 
* #MR0418 ^property[0].valueCode = #MR04 
* #MR0419 "MR Achillessehne li."
* #MR0419 ^property[0].code = #parent 
* #MR0419 ^property[0].valueCode = #MR04 
* #MR0420 "MR Achillessehne re."
* #MR0420 ^property[0].code = #parent 
* #MR0420 ^property[0].valueCode = #MR04 
* #MR0421 "MR Achillessehne bds."
* #MR0421 ^property[0].code = #parent 
* #MR0421 ^property[0].valueCode = #MR04 
* #MR0422 "MR Sprunggelenk li."
* #MR0422 ^property[0].code = #parent 
* #MR0422 ^property[0].valueCode = #MR04 
* #MR0423 "MR Sprunggelenk re."
* #MR0423 ^property[0].code = #parent 
* #MR0423 ^property[0].valueCode = #MR04 
* #MR0424 "MR Sprunggelenk bds."
* #MR0424 ^property[0].code = #parent 
* #MR0424 ^property[0].valueCode = #MR04 
* #MR0425 "MR Arthro Sprunggelenk li."
* #MR0425 ^property[0].code = #parent 
* #MR0425 ^property[0].valueCode = #MR04 
* #MR0426 "MR Arthro Sprunggelenk re."
* #MR0426 ^property[0].code = #parent 
* #MR0426 ^property[0].valueCode = #MR04 
* #MR0427 "MR Arthro Sprunggelenk bds."
* #MR0427 ^property[0].code = #parent 
* #MR0427 ^property[0].valueCode = #MR04 
* #MR0428 "MR Fuß li."
* #MR0428 ^property[0].code = #parent 
* #MR0428 ^property[0].valueCode = #MR04 
* #MR0429 "MR Fuß re."
* #MR0429 ^property[0].code = #parent 
* #MR0429 ^property[0].valueCode = #MR04 
* #MR0430 "MR Fuß bds."
* #MR0430 ^property[0].code = #parent 
* #MR0430 ^property[0].valueCode = #MR04 
* #MR05 "MR Thorax"
* #MR05 ^property[0].code = #parent 
* #MR05 ^property[0].valueCode = #MR 
* #MR05 ^property[1].code = #child 
* #MR05 ^property[1].valueCode = #MR0501 
* #MR05 ^property[2].code = #child 
* #MR05 ^property[2].valueCode = #MR0502 
* #MR05 ^property[3].code = #child 
* #MR05 ^property[3].valueCode = #MR0503 
* #MR05 ^property[4].code = #child 
* #MR05 ^property[4].valueCode = #MR0504 
* #MR05 ^property[5].code = #child 
* #MR05 ^property[5].valueCode = #MR0505 
* #MR05 ^property[6].code = #child 
* #MR05 ^property[6].valueCode = #MR0506 
* #MR05 ^property[7].code = #child 
* #MR05 ^property[7].valueCode = #MR0507 
* #MR05 ^property[8].code = #child 
* #MR05 ^property[8].valueCode = #MR0508 
* #MR05 ^property[9].code = #child 
* #MR05 ^property[9].valueCode = #MR0509 
* #MR05 ^property[10].code = #child 
* #MR05 ^property[10].valueCode = #MR0510 
* #MR0501 "MR Hals"
* #MR0501 ^property[0].code = #parent 
* #MR0501 ^property[0].valueCode = #MR05 
* #MR0502 "MR Mediastinum"
* #MR0502 ^property[0].code = #parent 
* #MR0502 ^property[0].valueCode = #MR05 
* #MR0503 "MR Clavicula li."
* #MR0503 ^property[0].code = #parent 
* #MR0503 ^property[0].valueCode = #MR05 
* #MR0504 "MR Clavicula re."
* #MR0504 ^property[0].code = #parent 
* #MR0504 ^property[0].valueCode = #MR05 
* #MR0505 "MR Clavicula bds."
* #MR0505 ^property[0].code = #parent 
* #MR0505 ^property[0].valueCode = #MR05 
* #MR0506 "MR Thorax"
* #MR0506 ^property[0].code = #parent 
* #MR0506 ^property[0].valueCode = #MR05 
* #MR0507 "MR Sternum"
* #MR0507 ^property[0].code = #parent 
* #MR0507 ^property[0].valueCode = #MR05 
* #MR0508 "MR Thoraxwand"
* #MR0508 ^property[0].code = #parent 
* #MR0508 ^property[0].valueCode = #MR05 
* #MR0509 "MR Axilla"
* #MR0509 ^property[0].code = #parent 
* #MR0509 ^property[0].valueCode = #MR05 
* #MR0509 ^property[1].code = #child 
* #MR0509 ^property[1].valueCode = #MR050901 
* #MR0509 ^property[2].code = #child 
* #MR0509 ^property[2].valueCode = #MR050902 
* #MR0509 ^property[3].code = #child 
* #MR0509 ^property[3].valueCode = #MR050903 
* #MR050901 "MR Axilla li."
* #MR050901 ^property[0].code = #parent 
* #MR050901 ^property[0].valueCode = #MR0509 
* #MR050902 "MR Axilla re."
* #MR050902 ^property[0].code = #parent 
* #MR050902 ^property[0].valueCode = #MR0509 
* #MR050903 "MR Axilla bds."
* #MR050903 ^property[0].code = #parent 
* #MR050903 ^property[0].valueCode = #MR0509 
* #MR0510 "MR Plexus brachialis"
* #MR0510 ^property[0].code = #parent 
* #MR0510 ^property[0].valueCode = #MR05 
* #MR06 "MR Herz"
* #MR06 ^property[0].code = #parent 
* #MR06 ^property[0].valueCode = #MR 
* #MR06 ^property[1].code = #child 
* #MR06 ^property[1].valueCode = #MR0601 
* #MR06 ^property[2].code = #child 
* #MR06 ^property[2].valueCode = #MR0602 
* #MR06 ^property[3].code = #child 
* #MR06 ^property[3].valueCode = #MR0603 
* #MR06 ^property[4].code = #child 
* #MR06 ^property[4].valueCode = #MR0604 
* #MR06 ^property[5].code = #child 
* #MR06 ^property[5].valueCode = #MR0605 
* #MR0601 "MR Herz"
* #MR0601 ^property[0].code = #parent 
* #MR0601 ^property[0].valueCode = #MR06 
* #MR0602 "MR Herz Stressperfusion"
* #MR0602 ^property[0].code = #parent 
* #MR0602 ^property[0].valueCode = #MR06 
* #MR0603 "MR Herz Vitalität"
* #MR0603 ^property[0].code = #parent 
* #MR0603 ^property[0].valueCode = #MR06 
* #MR0604 "MR Herz Flussquantifizierung"
* #MR0604 ^property[0].code = #parent 
* #MR0604 ^property[0].valueCode = #MR06 
* #MR0605 "MR Herz Perfusion"
* #MR0605 ^property[0].code = #parent 
* #MR0605 ^property[0].valueCode = #MR06 
* #MR07 "MR Abdomen"
* #MR07 ^property[0].code = #parent 
* #MR07 ^property[0].valueCode = #MR 
* #MR07 ^property[1].code = #child 
* #MR07 ^property[1].valueCode = #MR0701 
* #MR07 ^property[2].code = #child 
* #MR07 ^property[2].valueCode = #MR0702 
* #MR07 ^property[3].code = #child 
* #MR07 ^property[3].valueCode = #MR0703 
* #MR07 ^property[4].code = #child 
* #MR07 ^property[4].valueCode = #MR0704 
* #MR07 ^property[5].code = #child 
* #MR07 ^property[5].valueCode = #MR0705 
* #MR0701 "MR Oberbauch"
* #MR0701 ^property[0].code = #parent 
* #MR0701 ^property[0].valueCode = #MR07 
* #MR0701 ^property[1].code = #child 
* #MR0701 ^property[1].valueCode = #MR070101 
* #MR0701 ^property[2].code = #child 
* #MR0701 ^property[2].valueCode = #MR070102 
* #MR0701 ^property[3].code = #child 
* #MR0701 ^property[3].valueCode = #MR070103 
* #MR0701 ^property[4].code = #child 
* #MR0701 ^property[4].valueCode = #MR070104 
* #MR070101 "MR Leber"
* #MR070101 ^property[0].code = #parent 
* #MR070101 ^property[0].valueCode = #MR0701 
* #MR070102 "MR Cholangiographie"
* #MR070102 ^property[0].code = #parent 
* #MR070102 ^property[0].valueCode = #MR0701 
* #MR070103 "MR Pankreas"
* #MR070103 ^property[0].code = #parent 
* #MR070103 ^property[0].valueCode = #MR0701 
* #MR070104 "MR Milz"
* #MR070104 ^property[0].code = #parent 
* #MR070104 ^property[0].valueCode = #MR0701 
* #MR0702 "MR Nieren"
* #MR0702 ^property[0].code = #parent 
* #MR0702 ^property[0].valueCode = #MR07 
* #MR0702 ^property[1].code = #child 
* #MR0702 ^property[1].valueCode = #MR070201 
* #MR070201 "MR Nebennieren"
* #MR070201 ^property[0].code = #parent 
* #MR070201 ^property[0].valueCode = #MR0702 
* #MR0703 "MR Unterbauch"
* #MR0703 ^property[0].code = #parent 
* #MR0703 ^property[0].valueCode = #MR07 
* #MR0703 ^property[1].code = #child 
* #MR0703 ^property[1].valueCode = #MR070301 
* #MR0703 ^property[2].code = #child 
* #MR0703 ^property[2].valueCode = #MR070302 
* #MR0703 ^property[3].code = #child 
* #MR0703 ^property[3].valueCode = #MR070303 
* #MR0703 ^property[4].code = #child 
* #MR0703 ^property[4].valueCode = #MR070304 
* #MR0703 ^property[5].code = #child 
* #MR0703 ^property[5].valueCode = #MR070305 
* #MR070301 "MR Scrotum - Penis"
* #MR070301 ^property[0].code = #parent 
* #MR070301 ^property[0].valueCode = #MR0703 
* #MR070302 "MR Prostata"
* #MR070302 ^property[0].code = #parent 
* #MR070302 ^property[0].valueCode = #MR0703 
* #MR070303 "MR Blase"
* #MR070303 ^property[0].code = #parent 
* #MR070303 ^property[0].valueCode = #MR0703 
* #MR070304 "MR Rectum"
* #MR070304 ^property[0].code = #parent 
* #MR070304 ^property[0].valueCode = #MR0703 
* #MR070305 "MR Uterus / Adnexen"
* #MR070305 ^property[0].code = #parent 
* #MR070305 ^property[0].valueCode = #MR0703 
* #MR0704 "MR Enteroklysma"
* #MR0704 ^property[0].code = #parent 
* #MR0704 ^property[0].valueCode = #MR07 
* #MR0705 "MR Urographie"
* #MR0705 ^property[0].code = #parent 
* #MR0705 ^property[0].valueCode = #MR07 
* #MR08 "MR Mamma"
* #MR08 ^property[0].code = #parent 
* #MR08 ^property[0].valueCode = #MR 
* #MR08 ^property[1].code = #child 
* #MR08 ^property[1].valueCode = #MR0801 
* #MR0801 "MR Mamma Spektroskopie"
* #MR0801 ^property[0].code = #parent 
* #MR0801 ^property[0].valueCode = #MR08 
* #MR09 "MR Diverses"
* #MR09 ^property[0].code = #parent 
* #MR09 ^property[0].valueCode = #MR 
* #MR09 ^property[1].code = #child 
* #MR09 ^property[1].valueCode = #MR0901 
* #MR0901 "MR Körperstamm"
* #MR0901 ^property[0].code = #parent 
* #MR0901 ^property[0].valueCode = #MR09 
* #MR10 "MR Angiographie"
* #MR10 ^property[0].code = #parent 
* #MR10 ^property[0].valueCode = #MR 
* #MR10 ^property[1].code = #child 
* #MR10 ^property[1].valueCode = #MR1001 
* #MR10 ^property[2].code = #child 
* #MR10 ^property[2].valueCode = #MR1002 
* #MR10 ^property[3].code = #child 
* #MR10 ^property[3].valueCode = #MR1003 
* #MR10 ^property[4].code = #child 
* #MR10 ^property[4].valueCode = #MR1004 
* #MR10 ^property[5].code = #child 
* #MR10 ^property[5].valueCode = #MR1005 
* #MR10 ^property[6].code = #child 
* #MR10 ^property[6].valueCode = #MR1006 
* #MR10 ^property[7].code = #child 
* #MR10 ^property[7].valueCode = #MR1007 
* #MR10 ^property[8].code = #child 
* #MR10 ^property[8].valueCode = #MR1008 
* #MR10 ^property[9].code = #child 
* #MR10 ^property[9].valueCode = #MR1009 
* #MR10 ^property[10].code = #child 
* #MR10 ^property[10].valueCode = #MR1010 
* #MR10 ^property[11].code = #child 
* #MR10 ^property[11].valueCode = #MR1011 
* #MR10 ^property[12].code = #child 
* #MR10 ^property[12].valueCode = #MR1012 
* #MR10 ^property[13].code = #child 
* #MR10 ^property[13].valueCode = #MR1013 
* #MR10 ^property[14].code = #child 
* #MR10 ^property[14].valueCode = #MR1014 
* #MR10 ^property[15].code = #child 
* #MR10 ^property[15].valueCode = #MR1015 
* #MR10 ^property[16].code = #child 
* #MR10 ^property[16].valueCode = #MR1016 
* #MR1001 "MRA intracranielle Gefäße"
* #MR1001 ^property[0].code = #parent 
* #MR1001 ^property[0].valueCode = #MR10 
* #MR1002 "MRA intracranielle Venen"
* #MR1002 ^property[0].code = #parent 
* #MR1002 ^property[0].valueCode = #MR10 
* #MR1003 "MRA Carotis / Vertebralis"
* #MR1003 ^property[0].code = #parent 
* #MR1003 ^property[0].valueCode = #MR10 
* #MR1004 "MRA Halsgefäße"
* #MR1004 ^property[0].code = #parent 
* #MR1004 ^property[0].valueCode = #MR10 
* #MR1005 "MRA A. Subclavia"
* #MR1005 ^property[0].code = #parent 
* #MR1005 ^property[0].valueCode = #MR10 
* #MR1006 "MRA A. Pulmonalis"
* #MR1006 ^property[0].code = #parent 
* #MR1006 ^property[0].valueCode = #MR10 
* #MR1007 "MRA Aorta thoracalis"
* #MR1007 ^property[0].code = #parent 
* #MR1007 ^property[0].valueCode = #MR10 
* #MR1008 "MRA Aorta abdominalis"
* #MR1008 ^property[0].code = #parent 
* #MR1008 ^property[0].valueCode = #MR10 
* #MR1009 "MRA Aortenbogen"
* #MR1009 ^property[0].code = #parent 
* #MR1009 ^property[0].valueCode = #MR10 
* #MR1010 "MRA Vena Cava"
* #MR1010 ^property[0].code = #parent 
* #MR1010 ^property[0].valueCode = #MR10 
* #MR1011 "MRA Nierenarterien"
* #MR1011 ^property[0].code = #parent 
* #MR1011 ^property[0].valueCode = #MR10 
* #MR1012 "MRA obere Extremität"
* #MR1012 ^property[0].code = #parent 
* #MR1012 ^property[0].valueCode = #MR10 
* #MR1013 "MRA untere Extremität"
* #MR1013 ^property[0].code = #parent 
* #MR1013 ^property[0].valueCode = #MR10 
* #MR1014 "MRA Becken - Beinregion"
* #MR1014 ^property[0].code = #parent 
* #MR1014 ^property[0].valueCode = #MR10 
* #MR1015 "MRA Beckenvenen"
* #MR1015 ^property[0].code = #parent 
* #MR1015 ^property[0].valueCode = #MR10 
* #MR1016 "MRA diverses"
* #MR1016 ^property[0].code = #parent 
* #MR1016 ^property[0].valueCode = #MR10 
* #MR11 "MR Intervention"
* #MR11 ^property[0].code = #parent 
* #MR11 ^property[0].valueCode = #MR 
* #MR11 ^property[1].code = #child 
* #MR11 ^property[1].valueCode = #MR1101 
* #MR11 ^property[2].code = #child 
* #MR11 ^property[2].valueCode = #MR1102 
* #MR11 ^property[3].code = #child 
* #MR11 ^property[3].valueCode = #MR1103 
* #MR11 ^property[4].code = #child 
* #MR11 ^property[4].valueCode = #MR1104 
* #MR1101 "MR Biopsie"
* #MR1101 ^property[0].code = #parent 
* #MR1101 ^property[0].valueCode = #MR11 
* #MR1102 "MR Drainage"
* #MR1102 ^property[0].code = #parent 
* #MR1102 ^property[0].valueCode = #MR11 
* #MR1103 "MR Markierung"
* #MR1103 ^property[0].code = #parent 
* #MR1103 ^property[0].valueCode = #MR11 
* #MR1104 "MR Intervention Diverses "
* #MR1104 ^property[0].code = #parent 
* #MR1104 ^property[0].valueCode = #MR11 
* #MR12 "MR Perfusion"
* #MR12 ^property[0].code = #parent 
* #MR12 ^property[0].valueCode = #MR 
* #MR13 "MR Spektroskopie"
* #MR13 ^property[0].code = #parent 
* #MR13 ^property[0].valueCode = #MR 
* #MR14 "MR Navigation"
* #MR14 ^property[0].code = #parent 
* #MR14 ^property[0].valueCode = #MR 
* #MR15 "MR Kinematik"
* #MR15 ^property[0].code = #parent 
* #MR15 ^property[0].valueCode = #MR 
* #NM "NM Nuklearmedizin"
* #NM ^property[0].code = #child 
* #NM ^property[0].valueCode = #NM01 
* #NM ^property[1].code = #child 
* #NM ^property[1].valueCode = #NM02 
* #NM ^property[2].code = #child 
* #NM ^property[2].valueCode = #NM03 
* #NM ^property[3].code = #child 
* #NM ^property[3].valueCode = #NM04 
* #NM ^property[4].code = #child 
* #NM ^property[4].valueCode = #NM05 
* #NM ^property[5].code = #child 
* #NM ^property[5].valueCode = #NM06 
* #NM01 "NM Szintigraphie"
* #NM01 ^property[0].code = #parent 
* #NM01 ^property[0].valueCode = #NM 
* #NM01 ^property[1].code = #child 
* #NM01 ^property[1].valueCode = #NM0102 
* #NM01 ^property[2].code = #child 
* #NM01 ^property[2].valueCode = #NM0107 
* #NM01 ^property[3].code = #child 
* #NM01 ^property[3].valueCode = #NM0108 
* #NM01 ^property[4].code = #child 
* #NM01 ^property[4].valueCode = #NM0109 
* #NM01 ^property[5].code = #child 
* #NM01 ^property[5].valueCode = #NM0110 
* #NM01 ^property[6].code = #child 
* #NM01 ^property[6].valueCode = #NM0112 
* #NM01 ^property[7].code = #child 
* #NM01 ^property[7].valueCode = #NM0113 
* #NM01 ^property[8].code = #child 
* #NM01 ^property[8].valueCode = #NM0117 
* #NM01 ^property[9].code = #child 
* #NM01 ^property[9].valueCode = #NM0118 
* #NM01 ^property[10].code = #child 
* #NM01 ^property[10].valueCode = #NM0119 
* #NM01 ^property[11].code = #child 
* #NM01 ^property[11].valueCode = #NM0121 
* #NM01 ^property[12].code = #child 
* #NM01 ^property[12].valueCode = #NM0126 
* #NM01 ^property[13].code = #child 
* #NM01 ^property[13].valueCode = #NM0127 
* #NM01 ^property[14].code = #child 
* #NM01 ^property[14].valueCode = #NM0129 
* #NM01 ^property[15].code = #child 
* #NM01 ^property[15].valueCode = #NM0130 
* #NM01 ^property[16].code = #child 
* #NM01 ^property[16].valueCode = #NM0131 
* #NM01 ^property[17].code = #child 
* #NM01 ^property[17].valueCode = #NM0132 
* #NM01 ^property[18].code = #child 
* #NM01 ^property[18].valueCode = #NM0133 
* #NM01 ^property[19].code = #child 
* #NM01 ^property[19].valueCode = #NM0135 
* #NM01 ^property[20].code = #child 
* #NM01 ^property[20].valueCode = #NM0136 
* #NM01 ^property[21].code = #child 
* #NM01 ^property[21].valueCode = #NM0138 
* #NM01 ^property[22].code = #child 
* #NM01 ^property[22].valueCode = #NM0139 
* #NM01 ^property[23].code = #child 
* #NM01 ^property[23].valueCode = #NM0140 
* #NM01 ^property[24].code = #child 
* #NM01 ^property[24].valueCode = #NM0141 
* #NM01 ^property[25].code = #child 
* #NM01 ^property[25].valueCode = #NM0142 
* #NM01 ^property[26].code = #child 
* #NM01 ^property[26].valueCode = #NM0143 
* #NM01 ^property[27].code = #child 
* #NM01 ^property[27].valueCode = #NM0144 
* #NM01 ^property[28].code = #child 
* #NM01 ^property[28].valueCode = #NM0145 
* #NM01 ^property[29].code = #child 
* #NM01 ^property[29].valueCode = #NM0146 
* #NM01 ^property[30].code = #child 
* #NM01 ^property[30].valueCode = #NM0147 
* #NM01 ^property[31].code = #child 
* #NM01 ^property[31].valueCode = #NM0148 
* #NM01 ^property[32].code = #child 
* #NM01 ^property[32].valueCode = #NM0149 
* #NM01 ^property[33].code = #child 
* #NM01 ^property[33].valueCode = #NM0150 
* #NM0102 "NM SD-Szintigraphie 99m-Tc mit Uptake/Markierung"
* #NM0102 ^property[0].code = #parent 
* #NM0102 ^property[0].valueCode = #NM01 
* #NM0107 "NM 131 Jod-GK-Szintigramm (Karzinomnachsorge)"
* #NM0107 ^property[0].code = #parent 
* #NM0107 ^property[0].valueCode = #NM01 
* #NM0108 "NM Ganzkörperknochenszintigraphie"
* #NM0108 ^property[0].code = #parent 
* #NM0108 ^property[0].valueCode = #NM01 
* #NM0109 "NM Mehrphasen Knochenszintigraphie"
* #NM0109 ^property[0].code = #parent 
* #NM0109 ^property[0].valueCode = #NM01 
* #NM0110 "NM Myokardszintigraphie mit Belastung"
* #NM0110 ^property[0].code = #parent 
* #NM0110 ^property[0].valueCode = #NM01 
* #NM0110 ^property[1].code = #child 
* #NM0110 ^property[1].valueCode = #NM011001 
* #NM011001 "NM Myokardszintigraphie ohne Belastung"
* #NM011001 ^property[0].code = #parent 
* #NM011001 ^property[0].valueCode = #NM0110 
* #NM0112 "NM Nierenszintigraphie (ING)"
* #NM0112 ^property[0].code = #parent 
* #NM0112 ^property[0].valueCode = #NM01 
* #NM0113 "NM Nierenszintigraphie mit pharmakologischer Intervention"
* #NM0113 ^property[0].code = #parent 
* #NM0113 ^property[0].valueCode = #NM01 
* #NM0117 "NM Gehirnszintigraphie"
* #NM0117 ^property[0].code = #parent 
* #NM0117 ^property[0].valueCode = #NM01 
* #NM0117 ^property[1].code = #child 
* #NM0117 ^property[1].valueCode = #NM011701 
* #NM0117 ^property[2].code = #child 
* #NM0117 ^property[2].valueCode = #NM011702 
* #NM0117 ^property[3].code = #child 
* #NM0117 ^property[3].valueCode = #NM011703 
* #NM011701 "NM Gehirnperfusionsszintigraphie (HMPAO, ECD) o. pharmakol. Interv."
* #NM011701 ^property[0].code = #parent 
* #NM011701 ^property[0].valueCode = #NM0117 
* #NM011702 "NM Gehirnperfusionsszintigraphie (HMPAO, ECD) m. pharmakol. Interv."
* #NM011702 ^property[0].code = #parent 
* #NM011702 ^property[0].valueCode = #NM0117 
* #NM011703 "NM Gehirnrezeptorszintigraphie (DAT, IBZM)"
* #NM011703 ^property[0].code = #parent 
* #NM011703 ^property[0].valueCode = #NM0117 
* #NM0118 "NM Lungenperfusionsszintigraphie"
* #NM0118 ^property[0].code = #parent 
* #NM0118 ^property[0].valueCode = #NM01 
* #NM0119 "NM Lungenventilationsszintigraphie"
* #NM0119 ^property[0].code = #parent 
* #NM0119 ^property[0].valueCode = #NM01 
* #NM0121 "NM 123 Jod GK-Szintigramm (ektope SD, Karzinomnachsorge)"
* #NM0121 ^property[0].code = #parent 
* #NM0121 ^property[0].valueCode = #NM01 
* #NM0126 "NM Nebenschilddrüsenszintigraphie mit 99m-Tc markierten Tracer (MIBI)"
* #NM0126 ^property[0].code = #parent 
* #NM0126 ^property[0].valueCode = #NM01 
* #NM0127 "NM Mammaszintiraphie mit 99m-Tc markierten Tracern"
* #NM0127 ^property[0].code = #parent 
* #NM0127 ^property[0].valueCode = #NM01 
* #NM0129 "NM Rezeptorszintigraphie mit 99m-Tc markierten Tracern"
* #NM0129 ^property[0].code = #parent 
* #NM0129 ^property[0].valueCode = #NM01 
* #NM0130 "NM Rezeptorszintigraphie m. 111-In o. 123-J mark. Tracern"
* #NM0130 ^property[0].code = #parent 
* #NM0130 ^property[0].valueCode = #NM01 
* #NM0131 "NM Szintigraphie mit markierten Blutbestandteilen"
* #NM0131 ^property[0].code = #parent 
* #NM0131 ^property[0].valueCode = #NM01 
* #NM0131 ^property[1].code = #child 
* #NM0131 ^property[1].valueCode = #NM013101 
* #NM0131 ^property[2].code = #child 
* #NM0131 ^property[2].valueCode = #NM013102 
* #NM0131 ^property[3].code = #child 
* #NM0131 ^property[3].valueCode = #NM013103 
* #NM013101 "NM Autologe Leukozyten zur Entzündungsszintigraphie"
* #NM013101 ^property[0].code = #parent 
* #NM013101 ^property[0].valueCode = #NM0131 
* #NM013102 "NM in vivo o. in vitro autologe Erythrozytenszinti z. Blutungssuche"
* #NM013102 ^property[0].code = #parent 
* #NM013102 ^property[0].valueCode = #NM0131 
* #NM013103 "NM 99m-Tc hitzegeschädigte Erys zur Milz/Nebenmilzszinti"
* #NM013103 ^property[0].code = #parent 
* #NM013103 ^property[0].valueCode = #NM0131 
* #NM0132 "NM Entzündungsszintigraphie (Anti-Granulozyten AK)"
* #NM0132 ^property[0].code = #parent 
* #NM0132 ^property[0].valueCode = #NM01 
* #NM0133 "NM zusätzliche Spät-/SPECT(/CT)-Aufn. o. Reinjektions-Aufn."
* #NM0133 ^property[0].code = #parent 
* #NM0133 ^property[0].valueCode = #NM01 
* #NM0135 "NM (Atem-, Herz-) getriggerte Untersuchung"
* #NM0135 ^property[0].code = #parent 
* #NM0135 ^property[0].valueCode = #NM01 
* #NM0136 "NM Low Dose CT"
* #NM0136 ^property[0].code = #parent 
* #NM0136 ^property[0].valueCode = #NM01 
* #NM0138 "NM Gallium-67 Szintigraphie"
* #NM0138 ^property[0].code = #parent 
* #NM0138 ^property[0].valueCode = #NM01 
* #NM0139 "NM Nierenparenchymszintigraphie (99m-TC DMSA)"
* #NM0139 ^property[0].code = #parent 
* #NM0139 ^property[0].valueCode = #NM01 
* #NM0140 "NM Szintigraphie Meckel Divertikel"
* #NM0140 ^property[0].code = #parent 
* #NM0140 ^property[0].valueCode = #NM01 
* #NM0141 "NM Speicheldrüsenszintigraphie / Nasolacrymales System"
* #NM0141 ^property[0].code = #parent 
* #NM0141 ^property[0].valueCode = #NM01 
* #NM0142 "NM Lymphabflussszintigraphie mit/ohne Quantifizierung"
* #NM0142 ^property[0].code = #parent 
* #NM0142 ^property[0].valueCode = #NM01 
* #NM0143 "NM Sentinel Markierung"
* #NM0143 ^property[0].code = #parent 
* #NM0143 ^property[0].valueCode = #NM01 
* #NM0144 "NM Knochenmarkszintigraphie"
* #NM0144 ^property[0].code = #parent 
* #NM0144 ^property[0].valueCode = #NM01 
* #NM0145 "NM Leber/Milz Szintigraphie 99m-Tc mark. Kolloiden"
* #NM0145 ^property[0].code = #parent 
* #NM0145 ^property[0].valueCode = #NM01 
* #NM0146 "NM Hepatobiliäre Funktionsszintigraphie"
* #NM0146 ^property[0].code = #parent 
* #NM0146 ^property[0].valueCode = #NM01 
* #NM0147 "NM Nebennierenszinti (MIBG/Norcholesterol m./o. pharmakol. Interv.)"
* #NM0147 ^property[0].code = #parent 
* #NM0147 ^property[0].valueCode = #NM01 
* #NM0148 "NM Magenentleerungsszintigraphie"
* #NM0148 ^property[0].code = #parent 
* #NM0148 ^property[0].valueCode = #NM01 
* #NM0149 "NM Ösophagusszintigraphie"
* #NM0149 ^property[0].code = #parent 
* #NM0149 ^property[0].valueCode = #NM01 
* #NM0150 "NM Hodenszintigraphie"
* #NM0150 ^property[0].code = #parent 
* #NM0150 ^property[0].valueCode = #NM01 
* #NM02 "NM molekulare Bildgebung PET/CT"
* #NM02 ^property[0].code = #parent 
* #NM02 ^property[0].valueCode = #NM 
* #NM02 ^property[1].code = #child 
* #NM02 ^property[1].valueCode = #NM0201 
* #NM02 ^property[2].code = #child 
* #NM02 ^property[2].valueCode = #NM0203 
* #NM02 ^property[3].code = #child 
* #NM02 ^property[3].valueCode = #NM0205 
* #NM02 ^property[4].code = #child 
* #NM02 ^property[4].valueCode = #NM0206 
* #NM0201 "NM PET/CT Ganzkörper"
* #NM0201 ^property[0].code = #parent 
* #NM0201 ^property[0].valueCode = #NM02 
* #NM0201 ^property[1].code = #child 
* #NM0201 ^property[1].valueCode = #NM020101 
* #NM0201 ^property[2].code = #child 
* #NM0201 ^property[2].valueCode = #NM020102 
* #NM0201 ^property[3].code = #child 
* #NM0201 ^property[3].valueCode = #NM020103 
* #NM0201 ^property[4].code = #child 
* #NM0201 ^property[4].valueCode = #NM020104 
* #NM0201 ^property[5].code = #child 
* #NM0201 ^property[5].valueCode = #NM020105 
* #NM0201 ^property[6].code = #child 
* #NM0201 ^property[6].valueCode = #NM020106 
* #NM0201 ^property[7].code = #child 
* #NM0201 ^property[7].valueCode = #NM020107 
* #NM0201 ^property[8].code = #child 
* #NM0201 ^property[8].valueCode = #NM020108 
* #NM020101 "NM PET/CT Ganzkörper m. F 18-FDG"
* #NM020101 ^property[0].code = #parent 
* #NM020101 ^property[0].valueCode = #NM0201 
* #NM020102 "NM PET/CT Ganzkörper m. 18-F DOPA"
* #NM020102 ^property[0].code = #parent 
* #NM020102 ^property[0].valueCode = #NM0201 
* #NM020103 "NM PET/CT Ganzkörper m. 18-F Cholin"
* #NM020103 ^property[0].code = #parent 
* #NM020103 ^property[0].valueCode = #NM0201 
* #NM020104 "NM PET/CT Ganzkörper m. PSMA (68-Ga o. 64-Cu o. 18-F mark.)"
* #NM020104 ^property[0].code = #parent 
* #NM020104 ^property[0].valueCode = #NM0201 
* #NM020105 "NM PET/CT Ganzkörper m. Somatostatinanaloga (68-Ga, 64-Cu mark.)"
* #NM020105 ^property[0].code = #parent 
* #NM020105 ^property[0].valueCode = #NM0201 
* #NM020106 "NM PET/CT Ganzkörper m. 18-F mark. Hypoxietracer (18F-MISO)"
* #NM020106 ^property[0].code = #parent 
* #NM020106 ^property[0].valueCode = #NM0201 
* #NM020107 "NM PET/CT Ganzkörper m. 124-Jod"
* #NM020107 ^property[0].code = #parent 
* #NM020107 ^property[0].valueCode = #NM0201 
* #NM020108 "NM PET/CT Ganzkörper m. 64-Cu Chlorid"
* #NM020108 ^property[0].code = #parent 
* #NM020108 ^property[0].valueCode = #NM0201 
* #NM0203 "NM PET/CT Teilkörper"
* #NM0203 ^property[0].code = #parent 
* #NM0203 ^property[0].valueCode = #NM02 
* #NM0203 ^property[1].code = #child 
* #NM0203 ^property[1].valueCode = #NM020301 
* #NM0203 ^property[2].code = #child 
* #NM0203 ^property[2].valueCode = #NM020302 
* #NM0203 ^property[3].code = #child 
* #NM0203 ^property[3].valueCode = #NM020303 
* #NM0203 ^property[4].code = #child 
* #NM0203 ^property[4].valueCode = #NM020304 
* #NM0203 ^property[5].code = #child 
* #NM0203 ^property[5].valueCode = #NM020305 
* #NM0203 ^property[6].code = #child 
* #NM0203 ^property[6].valueCode = #NM020306 
* #NM0203 ^property[7].code = #child 
* #NM0203 ^property[7].valueCode = #NM020307 
* #NM0203 ^property[8].code = #child 
* #NM0203 ^property[8].valueCode = #NM020308 
* #NM020301 "NM PET/CT Teilkörper m. F 18-FDG"
* #NM020301 ^property[0].code = #parent 
* #NM020301 ^property[0].valueCode = #NM0203 
* #NM020302 "NM PET/CT Teilkörper m. 18-F DOPA"
* #NM020302 ^property[0].code = #parent 
* #NM020302 ^property[0].valueCode = #NM0203 
* #NM020303 "NM PET/CT Teilkörper m. 18-F Cholin"
* #NM020303 ^property[0].code = #parent 
* #NM020303 ^property[0].valueCode = #NM0203 
* #NM020304 "NM PET/CT Teilkörper m. PSMA (68-Ga o. 64-Cu o. 18-F mark.)"
* #NM020304 ^property[0].code = #parent 
* #NM020304 ^property[0].valueCode = #NM0203 
* #NM020305 "NM PET/CT Teilkörper m. Somatostatinanaloga (68-Ga, 64-Cu mark.)"
* #NM020305 ^property[0].code = #parent 
* #NM020305 ^property[0].valueCode = #NM0203 
* #NM020306 "NM PET/CT Teilkörper m. 18-F mark. Hypoxietracer (18F-MISO)"
* #NM020306 ^property[0].code = #parent 
* #NM020306 ^property[0].valueCode = #NM0203 
* #NM020307 "NM PET/CT Teilkörper m. 124-Jod"
* #NM020307 ^property[0].code = #parent 
* #NM020307 ^property[0].valueCode = #NM0203 
* #NM020308 "NM PET/CT Teilkörper m. 64-Cu Chlorid"
* #NM020308 ^property[0].code = #parent 
* #NM020308 ^property[0].valueCode = #NM0203 
* #NM0205 "NM PET/CT Hirn"
* #NM0205 ^property[0].code = #parent 
* #NM0205 ^property[0].valueCode = #NM02 
* #NM0205 ^property[1].code = #child 
* #NM0205 ^property[1].valueCode = #NM020501 
* #NM0205 ^property[2].code = #child 
* #NM0205 ^property[2].valueCode = #NM020502 
* #NM0205 ^property[3].code = #child 
* #NM0205 ^property[3].valueCode = #NM020503 
* #NM0205 ^property[4].code = #child 
* #NM0205 ^property[4].valueCode = #NM020504 
* #NM020501 "NM PET/CT Hirn m. 18-F-FDG"
* #NM020501 ^property[0].code = #parent 
* #NM020501 ^property[0].valueCode = #NM0205 
* #NM020502 "NM PET/CT Hirn m. 18-F-FLT"
* #NM020502 ^property[0].code = #parent 
* #NM020502 ^property[0].valueCode = #NM0205 
* #NM020503 "NM PET/CT Hirn m. 18-F-FET"
* #NM020503 ^property[0].code = #parent 
* #NM020503 ^property[0].valueCode = #NM0205 
* #NM020504 "NM PET/CT Hirn m. 18-F-Amyloid (PIB, etc.)"
* #NM020504 ^property[0].code = #parent 
* #NM020504 ^property[0].valueCode = #NM0205 
* #NM0206 "NM PET-MRT"
* #NM0206 ^property[0].code = #parent 
* #NM0206 ^property[0].valueCode = #NM02 
* #NM0206 ^property[1].code = #child 
* #NM0206 ^property[1].valueCode = #NM020601 
* #NM0206 ^property[2].code = #child 
* #NM0206 ^property[2].valueCode = #NM020602 
* #NM0206 ^property[3].code = #child 
* #NM0206 ^property[3].valueCode = #NM020603 
* #NM020601 "NM PET-MRT Ganzkörper "
* #NM020601 ^property[0].code = #parent 
* #NM020601 ^property[0].valueCode = #NM0206 
* #NM020602 "NM PET-MRT Teilkörper"
* #NM020602 ^property[0].code = #parent 
* #NM020602 ^property[0].valueCode = #NM0206 
* #NM020603 "NM PET-MRT Hirn"
* #NM020603 ^property[0].code = #parent 
* #NM020603 ^property[0].valueCode = #NM0206 
* #NM03 "NM Radionuklidtherapie"
* #NM03 ^property[0].code = #parent 
* #NM03 ^property[0].valueCode = #NM 
* #NM03 ^property[1].code = #child 
* #NM03 ^property[1].valueCode = #NM0303 
* #NM03 ^property[2].code = #child 
* #NM03 ^property[2].valueCode = #NM0304 
* #NM03 ^property[3].code = #child 
* #NM03 ^property[3].valueCode = #NM0305 
* #NM03 ^property[4].code = #child 
* #NM03 ^property[4].valueCode = #NM0306 
* #NM03 ^property[5].code = #child 
* #NM03 ^property[5].valueCode = #NM0307 
* #NM03 ^property[6].code = #child 
* #NM03 ^property[6].valueCode = #NM0308 
* #NM03 ^property[7].code = #child 
* #NM03 ^property[7].valueCode = #NM0309 
* #NM03 ^property[8].code = #child 
* #NM03 ^property[8].valueCode = #NM0310 
* #NM03 ^property[9].code = #child 
* #NM03 ^property[9].valueCode = #NM0311 
* #NM03 ^property[10].code = #child 
* #NM03 ^property[10].valueCode = #NM0312 
* #NM0303 "NM Selektive Interne Radiotherapie (SIRT) mit Y90-Mikrosphären"
* #NM0303 ^property[0].code = #parent 
* #NM0303 ^property[0].valueCode = #NM03 
* #NM0304 "NM 131 Jod Therapie unterhalb der Freigrenze"
* #NM0304 ^property[0].code = #parent 
* #NM0304 ^property[0].valueCode = #NM03 
* #NM0305 "NM 131 Jod Therapie oberhalb der Freigrenze"
* #NM0305 ^property[0].code = #parent 
* #NM0305 ^property[0].valueCode = #NM03 
* #NM0306 "NM 153-Samarium palliative Knochenmetastasentherapie"
* #NM0306 ^property[0].code = #parent 
* #NM0306 ^property[0].valueCode = #NM03 
* #NM0307 "NM 90Y-Zevalin Therapie"
* #NM0307 ^property[0].code = #parent 
* #NM0307 ^property[0].valueCode = #NM03 
* #NM0308 "NM Radiosynoviorthese"
* #NM0308 ^property[0].code = #parent 
* #NM0308 ^property[0].valueCode = #NM03 
* #NM0309 "NM andere spezielle Radionuklidtherapien"
* #NM0309 ^property[0].code = #parent 
* #NM0309 ^property[0].valueCode = #NM03 
* #NM0310 "NM zusätzliche aufwändige Dosimetrie"
* #NM0310 ^property[0].code = #parent 
* #NM0310 ^property[0].valueCode = #NM03 
* #NM0311 "NM Thyrogen z. exog. TSH-Stimul. (vor SD-Ca Therapie o. NS-Diagnostik)"
* #NM0311 ^property[0].code = #parent 
* #NM0311 ^property[0].valueCode = #NM03 
* #NM0312 "NM aufwändige Lagerung u. Positionierung für Strahlentherapieplanung"
* #NM0312 ^property[0].code = #parent 
* #NM0312 ^property[0].valueCode = #NM03 
* #NM04 "NM DEXA Knochendichte"
* #NM04 ^property[0].code = #parent 
* #NM04 ^property[0].valueCode = #NM 
* #NM05 "NM Sonographie"
* #NM05 ^property[0].code = #parent 
* #NM05 ^property[0].valueCode = #NM 
* #NM05 ^property[1].code = #child 
* #NM05 ^property[1].valueCode = #NM0501 
* #NM05 ^property[2].code = #child 
* #NM05 ^property[2].valueCode = #NM0502 
* #NM05 ^property[3].code = #child 
* #NM05 ^property[3].valueCode = #NM0503 
* #NM0501 "NM Halssonographie (SD, NSD, etc.)"
* #NM0501 ^property[0].code = #parent 
* #NM0501 ^property[0].valueCode = #NM05 
* #NM0502 "NM sonographisch kontrollierte Feinnadelpunktion (SD, etc.)"
* #NM0502 ^property[0].code = #parent 
* #NM0502 ^property[0].valueCode = #NM05 
* #NM0503 "NM Schilddrüse CA NASU"
* #NM0503 ^property[0].code = #parent 
* #NM0503 ^property[0].valueCode = #NM05 
* #NM06 "NM Labor bzw. in vitro"
* #NM06 ^property[0].code = #parent 
* #NM06 ^property[0].valueCode = #NM 
* #NM06 ^property[1].code = #child 
* #NM06 ^property[1].valueCode = #NM0601 
* #NM06 ^property[2].code = #child 
* #NM06 ^property[2].valueCode = #NM0602 
* #NM06 ^property[3].code = #child 
* #NM06 ^property[3].valueCode = #NM0603 
* #NM06 ^property[4].code = #child 
* #NM06 ^property[4].valueCode = #NM0604 
* #NM06 ^property[5].code = #child 
* #NM06 ^property[5].valueCode = #NM0605 
* #NM06 ^property[6].code = #child 
* #NM06 ^property[6].valueCode = #NM0606 
* #NM0601 "NM Schilddrüsenlabor"
* #NM0601 ^property[0].code = #parent 
* #NM0601 ^property[0].valueCode = #NM06 
* #NM0602 "NM TRH / Prolaktin Stimulationstest"
* #NM0602 ^property[0].code = #parent 
* #NM0602 ^property[0].valueCode = #NM06 
* #NM0603 "NM Calcitonin Belastungstest"
* #NM0603 ^property[0].code = #parent 
* #NM0603 ^property[0].valueCode = #NM06 
* #NM0604 "NM quantitative Clearance Bestimmung (GFR)"
* #NM0604 ^property[0].code = #parent 
* #NM0604 ^property[0].valueCode = #NM06 
* #NM0605 "NM aufwändige in vitro Untersuchungen (Erythrozytenüberlebenszeit,…)"
* #NM0605 ^property[0].code = #parent 
* #NM0605 ^property[0].valueCode = #NM06 
* #NM0606 "NM Nuklearmedizinisches Labor (ACTH, Aldosteron, CortisolPTH, Renin,…)"
* #NM0606 ^property[0].code = #parent 
* #NM0606 ^property[0].valueCode = #NM06 
* #OT "OT Andere"
* #OT ^property[0].code = #child 
* #OT ^property[0].valueCode = #OT01 
* #OT ^property[1].code = #child 
* #OT ^property[1].valueCode = #OT02 
* #OT01 "OT Medizinisch Diverses"
* #OT01 ^property[0].code = #parent 
* #OT01 ^property[0].valueCode = #OT 
* #OT01 ^property[1].code = #child 
* #OT01 ^property[1].valueCode = #OT0101 
* #OT01 ^property[2].code = #child 
* #OT01 ^property[2].valueCode = #OT0102 
* #OT01 ^property[3].code = #child 
* #OT01 ^property[3].valueCode = #OT0106 
* #OT01 ^property[4].code = #child 
* #OT01 ^property[4].valueCode = #OT0107 
* #OT01 ^property[5].code = #child 
* #OT01 ^property[5].valueCode = #OT0108 
* #OT0101 "OT Second Opinion (Mammographie)"
* #OT0101 ^property[0].code = #parent 
* #OT0101 ^property[0].valueCode = #OT01 
* #OT0102 "OT Teleradiologie (Anforderung oder Befundung)"
* #OT0102 ^property[0].code = #parent 
* #OT0102 ^property[0].valueCode = #OT01 
* #OT0106 "OT Narkose (nur bei ambulanten Patienten)"
* #OT0106 ^property[0].code = #parent 
* #OT0106 ^property[0].valueCode = #OT01 
* #OT0107 "OT Kontrastmittelgabe iv (RÖ,US,CT,MR,AN)"
* #OT0107 ^property[0].code = #parent 
* #OT0107 ^property[0].valueCode = #OT01 
* #OT0108 "OT Befundung / Second Opinion (Fremdbilder)"
* #OT0108 ^property[0].code = #parent 
* #OT0108 ^property[0].valueCode = #OT01 
* #OT02 "OT Admin"
* #OT02 ^property[0].code = #parent 
* #OT02 ^property[0].valueCode = #OT 
* #OT02 ^property[1].code = #child 
* #OT02 ^property[1].valueCode = #OT0201 
* #OT02 ^property[2].code = #child 
* #OT02 ^property[2].valueCode = #OT0202 
* #OT02 ^property[3].code = #child 
* #OT02 ^property[3].valueCode = #OT0205 
* #OT02 ^property[4].code = #child 
* #OT02 ^property[4].valueCode = #OT0208 
* #OT02 ^property[5].code = #child 
* #OT02 ^property[5].valueCode = #OT0209 
* #OT02 ^property[6].code = #child 
* #OT02 ^property[6].valueCode = #OT0210 
* #OT02 ^property[7].code = #child 
* #OT02 ^property[7].valueCode = #OT0211 
* #OT02 ^property[8].code = #child 
* #OT02 ^property[8].valueCode = #OT0212 
* #OT02 ^property[9].code = #child 
* #OT02 ^property[9].valueCode = #OT0214 
* #OT0201 "OT Bildimport (CD, DVD, USB-Stick, Scan, etc.) Import"
* #OT0201 ^property[0].code = #parent 
* #OT0201 ^property[0].valueCode = #OT02 
* #OT0202 "OT Bildexport (CD, DVD, USB-Stick, Versand, Druck etc.)"
* #OT0202 ^property[0].code = #parent 
* #OT0202 ^property[0].valueCode = #OT02 
* #OT0205 "OT Fotodokumentation Digitalkamera"
* #OT0205 ^property[0].code = #parent 
* #OT0205 ^property[0].valueCode = #OT02 
* #OT0208 "OT Pat. Gespräch amb. Vorstellung- u. Bespr. (therapeutisches Gespr.)"
* #OT0208 ^property[0].code = #parent 
* #OT0208 ^property[0].valueCode = #OT02 
* #OT0209 "OT CT Befundung/Vidierung PET-CT"
* #OT0209 ^property[0].code = #parent 
* #OT0209 ^property[0].valueCode = #OT02 
* #OT0210 "OT PET Befundung/Vidierung PET-CT"
* #OT0210 ^property[0].code = #parent 
* #OT0210 ^property[0].valueCode = #OT02 
* #OT0211 "OT MRT Befundung/Vidierung PET-MRT"
* #OT0211 ^property[0].code = #parent 
* #OT0211 ^property[0].valueCode = #OT02 
* #OT0212 "OT PET Befundung/Vidierung PET-MRT"
* #OT0212 ^property[0].code = #parent 
* #OT0212 ^property[0].valueCode = #OT02 
* #OT0214 "OT Software Bildfusion (SPECT/PET mit US/CT/MRT)"
* #OT0214 ^property[0].code = #parent 
* #OT0214 ^property[0].valueCode = #OT02 
* #PM "PM Schrittmacher"
* #PM ^property[0].code = #child 
* #PM ^property[0].valueCode = #PM01 
* #PM ^property[1].code = #child 
* #PM ^property[1].valueCode = #PM02 
* #PM ^property[2].code = #child 
* #PM ^property[2].valueCode = #PM03 
* #PM ^property[3].code = #child 
* #PM ^property[3].valueCode = #PM04 
* #PM ^property[4].code = #child 
* #PM ^property[4].valueCode = #PM05 
* #PM ^property[5].code = #child 
* #PM ^property[5].valueCode = #PM06 
* #PM ^property[6].code = #child 
* #PM ^property[6].valueCode = #PM07 
* #PM ^property[7].code = #child 
* #PM ^property[7].valueCode = #PM08 
* #PM ^property[8].code = #child 
* #PM ^property[8].valueCode = #PM09 
* #PM ^property[9].code = #child 
* #PM ^property[9].valueCode = #PM10 
* #PM ^property[10].code = #child 
* #PM ^property[10].valueCode = #PM11 
* #PM ^property[11].code = #child 
* #PM ^property[11].valueCode = #PM12 
* #PM ^property[12].code = #child 
* #PM ^property[12].valueCode = #PM13 
* #PM ^property[13].code = #child 
* #PM ^property[13].valueCode = #PM14 
* #PM ^property[14].code = #child 
* #PM ^property[14].valueCode = #PM15 
* #PM ^property[15].code = #child 
* #PM ^property[15].valueCode = #PM16 
* #PM ^property[16].code = #child 
* #PM ^property[16].valueCode = #PM17 
* #PM ^property[17].code = #child 
* #PM ^property[17].valueCode = #PM18 
* #PM ^property[18].code = #child 
* #PM ^property[18].valueCode = #PM19 
* #PM ^property[19].code = #child 
* #PM ^property[19].valueCode = #PM20 
* #PM ^property[20].code = #child 
* #PM ^property[20].valueCode = #PM21 
* #PM ^property[21].code = #child 
* #PM ^property[21].valueCode = #PM22 
* #PM ^property[22].code = #child 
* #PM ^property[22].valueCode = #PM23 
* #PM ^property[23].code = #child 
* #PM ^property[23].valueCode = #PM24 
* #PM ^property[24].code = #child 
* #PM ^property[24].valueCode = #PM25 
* #PM ^property[25].code = #child 
* #PM ^property[25].valueCode = #PM26 
* #PM01 "PM Implantation e. kardialen Monitors"
* #PM01 ^property[0].code = #parent 
* #PM01 ^property[0].valueCode = #PM 
* #PM02 "PM Looprecorder-Implantation"
* #PM02 ^property[0].code = #parent 
* #PM02 ^property[0].valueCode = #PM 
* #PM03 "PM Implantation e. Herzschrittmachers, Einkammersys."
* #PM03 ^property[0].code = #parent 
* #PM03 ^property[0].valueCode = #PM 
* #PM04 "PM Implantation e. Herzschrittmachers, Einkammersys., MR-tauglich"
* #PM04 ^property[0].code = #parent 
* #PM04 ^property[0].valueCode = #PM 
* #PM05 "PM Implantation e. Herzschrittmachers, Zweikammersystem"
* #PM05 ^property[0].code = #parent 
* #PM05 ^property[0].valueCode = #PM 
* #PM06 "PM Implantation e. Herzschrittmachers, Zweikammersys., MR-tauglich"
* #PM06 ^property[0].code = #parent 
* #PM06 ^property[0].valueCode = #PM 
* #PM07 "PM Implantation e. Systems zur kardialen Resynchronisationstherapie"
* #PM07 ^property[0].code = #parent 
* #PM07 ^property[0].valueCode = #PM 
* #PM08 "PM Implantation e. autom. Kardioverter-Defibrillators"
* #PM08 ^property[0].code = #parent 
* #PM08 ^property[0].valueCode = #PM 
* #PM09 "PM Implantation e. autom. Kardioverter-Defibrillators, MR-tauglich"
* #PM09 ^property[0].code = #parent 
* #PM09 ^property[0].valueCode = #PM 
* #PM10 "PM Impl. e. autom. Kardioverter-Defibrillators, subkutane Elektroden "
* #PM10 ^property[0].code = #parent 
* #PM10 ^property[0].valueCode = #PM 
* #PM11 "PM Impl. autom. Kardioverter-Defibrillator m. kard. Resynch-funktion"
* #PM11 ^property[0].code = #parent 
* #PM11 ^property[0].valueCode = #PM 
* #PM12 "PM Wechsel kardialer Schrittmachersonden"
* #PM12 ^property[0].code = #parent 
* #PM12 ^property[0].valueCode = #PM 
* #PM13 "PM Aggregatwechsel b. e. Herzschrittmacher, Einkammersystem"
* #PM13 ^property[0].code = #parent 
* #PM13 ^property[0].valueCode = #PM 
* #PM14 "PM Aggregatwechsel b. e. Herzschrittmacher, Einkammersystem, MR-taugl."
* #PM14 ^property[0].code = #parent 
* #PM14 ^property[0].valueCode = #PM 
* #PM15 "PM Aggregatwechsel b. e. Herzschrittmacher, Zweikammersystem"
* #PM15 ^property[0].code = #parent 
* #PM15 ^property[0].valueCode = #PM 
* #PM16 "PM Aggregatwechsel b. e. Herzschrittmacher, Zweikammersys., MR-taugl."
* #PM16 ^property[0].code = #parent 
* #PM16 ^property[0].valueCode = #PM 
* #PM17 "PM Aggregatwechsel b. e. System zur kard. Resynchronisationstherapie "
* #PM17 ^property[0].code = #parent 
* #PM17 ^property[0].valueCode = #PM 
* #PM18 "PM Aggregatwechsel b. e. autom. Kardioverter-Defibrillator"
* #PM18 ^property[0].code = #parent 
* #PM18 ^property[0].valueCode = #PM 
* #PM19 "PM Aggregatwechsel b. e. autom. Kardioverter-Defibrillator, MR-taugl"
* #PM19 ^property[0].code = #parent 
* #PM19 ^property[0].valueCode = #PM 
* #PM20 "PM Aggregatw. b. autom. Kardioverter-Defi m. kard. Resynch-funktion"
* #PM20 ^property[0].code = #parent 
* #PM20 ^property[0].valueCode = #PM 
* #PM21 "PM Kontrolle eines Herzschrittmachers (Ein- und Zweikammersystem)"
* #PM21 ^property[0].code = #parent 
* #PM21 ^property[0].valueCode = #PM 
* #PM22 "PM Kontrolle e. impl. Defibrillators/Systems zur CRT/kard. Monitors"
* #PM22 ^property[0].code = #parent 
* #PM22 ^property[0].valueCode = #PM 
* #PM23 "PM Kontrolle eines Loop-Recorders"
* #PM23 ^property[0].code = #parent 
* #PM23 ^property[0].valueCode = #PM 
* #PM24 "PM Passager Schrittmacher"
* #PM24 ^property[0].code = #parent 
* #PM24 ^property[0].valueCode = #PM 
* #PM25 "PM Implantation eines Trikuspidalklappenclips – perkutan"
* #PM25 ^property[0].code = #parent 
* #PM25 ^property[0].valueCode = #PM 
* #PM26 "PM Implantation eines sondenlosen Herzschrittmachers – perkutan"
* #PM26 ^property[0].code = #parent 
* #PM26 ^property[0].valueCode = #PM 
* #RO "RO Röntgen"
* #RO ^property[0].code = #child 
* #RO ^property[0].valueCode = #RO01 
* #RO ^property[1].code = #child 
* #RO ^property[1].valueCode = #RO02 
* #RO ^property[2].code = #child 
* #RO ^property[2].valueCode = #RO03 
* #RO ^property[3].code = #child 
* #RO ^property[3].valueCode = #RO04 
* #RO ^property[4].code = #child 
* #RO ^property[4].valueCode = #RO05 
* #RO ^property[5].code = #child 
* #RO ^property[5].valueCode = #RO06 
* #RO ^property[6].code = #child 
* #RO ^property[6].valueCode = #RO07 
* #RO ^property[7].code = #child 
* #RO ^property[7].valueCode = #RO08 
* #RO ^property[8].code = #child 
* #RO ^property[8].valueCode = #RO09 
* #RO ^property[9].code = #child 
* #RO ^property[9].valueCode = #RO10 
* #RO ^property[10].code = #child 
* #RO ^property[10].valueCode = #RO11 
* #RO01 "RO Thorax"
* #RO01 ^property[0].code = #parent 
* #RO01 ^property[0].valueCode = #RO 
* #RO01 ^property[1].code = #child 
* #RO01 ^property[1].valueCode = #RO0101 
* #RO01 ^property[2].code = #child 
* #RO01 ^property[2].valueCode = #RO0102 
* #RO01 ^property[3].code = #child 
* #RO01 ^property[3].valueCode = #RO0103 
* #RO01 ^property[4].code = #child 
* #RO01 ^property[4].valueCode = #RO0104 
* #RO01 ^property[5].code = #child 
* #RO01 ^property[5].valueCode = #RO0105 
* #RO01 ^property[6].code = #child 
* #RO01 ^property[6].valueCode = #RO0106 
* #RO01 ^property[7].code = #child 
* #RO01 ^property[7].valueCode = #RO0107 
* #RO01 ^property[8].code = #child 
* #RO01 ^property[8].valueCode = #RO0108 
* #RO01 ^property[9].code = #child 
* #RO01 ^property[9].valueCode = #RO0109 
* #RO01 ^property[10].code = #child 
* #RO01 ^property[10].valueCode = #RO0110 
* #RO0101 "RO Thorax pa/stl."
* #RO0101 ^property[0].code = #parent 
* #RO0101 ^property[0].valueCode = #RO01 
* #RO0102 "RO Thorax pa."
* #RO0102 ^property[0].code = #parent 
* #RO0102 ^property[0].valueCode = #RO01 
* #RO0103 "RO Thorax ap."
* #RO0103 ^property[0].code = #parent 
* #RO0103 ^property[0].valueCode = #RO01 
* #RO0104 "RO Thorax auf der Station"
* #RO0104 ^property[0].code = #parent 
* #RO0104 ^property[0].valueCode = #RO01 
* #RO0105 "RO Sternum"
* #RO0105 ^property[0].code = #parent 
* #RO0105 ^property[0].valueCode = #RO01 
* #RO0106 "RO Hemithorax (Rippen) li."
* #RO0106 ^property[0].code = #parent 
* #RO0106 ^property[0].valueCode = #RO01 
* #RO0106 ^property[1].code = #child 
* #RO0106 ^property[1].valueCode = #RO010601 
* #RO010601 "RO Hemithorax (Rippen) 45° li. "
* #RO010601 ^property[0].code = #parent 
* #RO010601 ^property[0].valueCode = #RO0106 
* #RO0107 "RO Hemithorax (Rippen) re."
* #RO0107 ^property[0].code = #parent 
* #RO0107 ^property[0].valueCode = #RO01 
* #RO0107 ^property[1].code = #child 
* #RO0107 ^property[1].valueCode = #RO010701 
* #RO010701 "RO Hemithorax (Rippen) 45° re. "
* #RO010701 ^property[0].code = #parent 
* #RO010701 ^property[0].valueCode = #RO0107 
* #RO0108 "RO Hemithorax bds."
* #RO0108 ^property[0].code = #parent 
* #RO0108 ^property[0].valueCode = #RO01 
* #RO0109 "RO VP-Shunt Gesamtdarstellung (Kopf, Hals)"
* #RO0109 ^property[0].code = #parent 
* #RO0109 ^property[0].valueCode = #RO01 
* #RO0109 ^property[1].code = #child 
* #RO0109 ^property[1].valueCode = #RO010901 
* #RO010901 "RO VP-Shunt Ventil"
* #RO010901 ^property[0].code = #parent 
* #RO010901 ^property[0].valueCode = #RO0109 
* #RO0110 "RO Thorax seitlich"
* #RO0110 ^property[0].code = #parent 
* #RO0110 ^property[0].valueCode = #RO01 
* #RO02 "RO Abdomen/Retroperitoneum"
* #RO02 ^property[0].code = #parent 
* #RO02 ^property[0].valueCode = #RO 
* #RO02 ^property[1].code = #child 
* #RO02 ^property[1].valueCode = #RO0201 
* #RO02 ^property[2].code = #child 
* #RO02 ^property[2].valueCode = #RO0202 
* #RO02 ^property[3].code = #child 
* #RO02 ^property[3].valueCode = #RO0203 
* #RO02 ^property[4].code = #child 
* #RO02 ^property[4].valueCode = #RO0204 
* #RO02 ^property[5].code = #child 
* #RO02 ^property[5].valueCode = #RO0205 
* #RO02 ^property[6].code = #child 
* #RO02 ^property[6].valueCode = #RO0206 
* #RO0201 "RO Babygramm"
* #RO0201 ^property[0].code = #parent 
* #RO0201 ^property[0].valueCode = #RO02 
* #RO0202 "RO Abdomen nativ"
* #RO0202 ^property[0].code = #parent 
* #RO0202 ^property[0].valueCode = #RO02 
* #RO0202 ^property[1].code = #child 
* #RO0202 ^property[1].valueCode = #RO020201 
* #RO0202 ^property[2].code = #child 
* #RO0202 ^property[2].valueCode = #RO020202 
* #RO0202 ^property[3].code = #child 
* #RO0202 ^property[3].valueCode = #RO020203 
* #RO020201 "RO Abdomen nativ im Liegen"
* #RO020201 ^property[0].code = #parent 
* #RO020201 ^property[0].valueCode = #RO0202 
* #RO020202 "RO Abdomen nativ stehend"
* #RO020202 ^property[0].code = #parent 
* #RO020202 ^property[0].valueCode = #RO0202 
* #RO020203 "RO Abdomen links Seitenlage"
* #RO020203 ^property[0].code = #parent 
* #RO020203 ^property[0].valueCode = #RO0202 
* #RO0203 "RO Abdomen auf Station"
* #RO0203 ^property[0].code = #parent 
* #RO0203 ^property[0].valueCode = #RO02 
* #RO0203 ^property[1].code = #child 
* #RO0203 ^property[1].valueCode = #RO020301 
* #RO0203 ^property[2].code = #child 
* #RO0203 ^property[2].valueCode = #RO020302 
* #RO020301 "RO Abdomen auf Station liegend"
* #RO020301 ^property[0].code = #parent 
* #RO020301 ^property[0].valueCode = #RO0203 
* #RO020302 "RO Abdomen auf Station Quertisch"
* #RO020302 ^property[0].code = #parent 
* #RO020302 ^property[0].valueCode = #RO0203 
* #RO0204 "RO Galle"
* #RO0204 ^property[0].code = #parent 
* #RO0204 ^property[0].valueCode = #RO02 
* #RO0204 ^property[1].code = #child 
* #RO0204 ^property[1].valueCode = #RO020401 
* #RO0204 ^property[2].code = #child 
* #RO0204 ^property[2].valueCode = #RO020402 
* #RO0204 ^property[3].code = #child 
* #RO0204 ^property[3].valueCode = #RO020404 
* #RO020401 "RO Cholangiographie - T-Drain"
* #RO020401 ^property[0].code = #parent 
* #RO020401 ^property[0].valueCode = #RO0204 
* #RO020402 "RO PTC Kontrolle"
* #RO020402 ^property[0].code = #parent 
* #RO020402 ^property[0].valueCode = #RO0204 
* #RO020404 "RO ERCP"
* #RO020404 ^property[0].code = #parent 
* #RO020404 ^property[0].valueCode = #RO0204 
* #RO0205 "RO UG-Trakt"
* #RO0205 ^property[0].code = #parent 
* #RO0205 ^property[0].valueCode = #RO02 
* #RO0205 ^property[1].code = #child 
* #RO0205 ^property[1].valueCode = #RO020501 
* #RO0205 ^property[2].code = #child 
* #RO0205 ^property[2].valueCode = #RO020502 
* #RO0205 ^property[3].code = #child 
* #RO0205 ^property[3].valueCode = #RO020503 
* #RO0205 ^property[4].code = #child 
* #RO0205 ^property[4].valueCode = #RO020504 
* #RO0205 ^property[5].code = #child 
* #RO0205 ^property[5].valueCode = #RO020505 
* #RO0205 ^property[6].code = #child 
* #RO0205 ^property[6].valueCode = #RO020506 
* #RO0205 ^property[7].code = #child 
* #RO0205 ^property[7].valueCode = #RO020507 
* #RO0205 ^property[8].code = #child 
* #RO0205 ^property[8].valueCode = #RO020508 
* #RO020501 "RO Harntrakt nativ"
* #RO020501 ^property[0].code = #parent 
* #RO020501 ^property[0].valueCode = #RO0205 
* #RO020502 "RO IVP"
* #RO020502 ^property[0].code = #parent 
* #RO020502 ^property[0].valueCode = #RO0205 
* #RO020502 ^property[1].code = #child 
* #RO020502 ^property[1].valueCode = #RO020502a 
* #RO020502a "RO IVP Spätaufnahme"
* #RO020502a ^property[0].code = #parent 
* #RO020502a ^property[0].valueCode = #RO020502 
* #RO020503 "RO UCG"
* #RO020503 ^property[0].code = #parent 
* #RO020503 ^property[0].valueCode = #RO0205 
* #RO020504 "RO MCU"
* #RO020504 ^property[0].code = #parent 
* #RO020504 ^property[0].valueCode = #RO0205 
* #RO020505 "RO HSG"
* #RO020505 ^property[0].code = #parent 
* #RO020505 ^property[0].valueCode = #RO0205 
* #RO020506 "RO Nephrostomiesetzung, PCN"
* #RO020506 ^property[0].code = #parent 
* #RO020506 ^property[0].valueCode = #RO0205 
* #RO020507 "RO Nephrostomiedarstellung, Pyelogramm"
* #RO020507 ^property[0].code = #parent 
* #RO020507 ^property[0].valueCode = #RO0205 
* #RO020508 "RO retrograde Pyelographie"
* #RO020508 ^property[0].code = #parent 
* #RO020508 ^property[0].valueCode = #RO0205 
* #RO0206 "RO Abdomen nach Kontrastmittelgabe"
* #RO0206 ^property[0].code = #parent 
* #RO0206 ^property[0].valueCode = #RO02 
* #RO03 "RO GI-Trakt"
* #RO03 ^property[0].code = #parent 
* #RO03 ^property[0].valueCode = #RO 
* #RO03 ^property[1].code = #child 
* #RO03 ^property[1].valueCode = #RO0301 
* #RO03 ^property[2].code = #child 
* #RO03 ^property[2].valueCode = #RO0302 
* #RO03 ^property[3].code = #child 
* #RO03 ^property[3].valueCode = #RO0303 
* #RO03 ^property[4].code = #child 
* #RO03 ^property[4].valueCode = #RO0304 
* #RO03 ^property[5].code = #child 
* #RO03 ^property[5].valueCode = #RO0305 
* #RO03 ^property[6].code = #child 
* #RO03 ^property[6].valueCode = #RO0306 
* #RO03 ^property[7].code = #child 
* #RO03 ^property[7].valueCode = #RO0307 
* #RO03 ^property[8].code = #child 
* #RO03 ^property[8].valueCode = #RO0308 
* #RO03 ^property[9].code = #child 
* #RO03 ^property[9].valueCode = #RO0309 
* #RO0301 "RO Halsorgane"
* #RO0301 ^property[0].code = #parent 
* #RO0301 ^property[0].valueCode = #RO03 
* #RO0301 ^property[1].code = #child 
* #RO0301 ^property[1].valueCode = #RO030101 
* #RO0301 ^property[2].code = #child 
* #RO0301 ^property[2].valueCode = #RO030102 
* #RO030101 "RO Halsweichteile"
* #RO030101 ^property[0].code = #parent 
* #RO030101 ^property[0].valueCode = #RO0301 
* #RO030102 "RO Trachea"
* #RO030102 ^property[0].code = #parent 
* #RO030102 ^property[0].valueCode = #RO0301 
* #RO0302 "RO DL Videokinematografie"
* #RO0302 ^property[0].code = #parent 
* #RO0302 ^property[0].valueCode = #RO03 
* #RO0303 "RO DL Ösophagus"
* #RO0303 ^property[0].code = #parent 
* #RO0303 ^property[0].valueCode = #RO03 
* #RO0303 ^property[1].code = #child 
* #RO0303 ^property[1].valueCode = #RO030301 
* #RO030301 "RO DL Breischluck"
* #RO030301 ^property[0].code = #parent 
* #RO030301 ^property[0].valueCode = #RO0303 
* #RO0304 "RO DL Magen"
* #RO0304 ^property[0].code = #parent 
* #RO0304 ^property[0].valueCode = #RO03 
* #RO0305 "RO DL Magen-Darm Passage"
* #RO0305 ^property[0].code = #parent 
* #RO0305 ^property[0].valueCode = #RO03 
* #RO0306 "RO DL Enteroclysma (Sellink)"
* #RO0306 ^property[0].code = #parent 
* #RO0306 ^property[0].valueCode = #RO03 
* #RO0307 "RO DL Dickdarm (Irrigoskopie)"
* #RO0307 ^property[0].code = #parent 
* #RO0307 ^property[0].valueCode = #RO03 
* #RO0308 "RO DL Defäkographie"
* #RO0308 ^property[0].code = #parent 
* #RO0308 ^property[0].valueCode = #RO03 
* #RO0309 "RO DL Barium Plombe"
* #RO0309 ^property[0].code = #parent 
* #RO0309 ^property[0].valueCode = #RO03 
* #RO04 "RO Durchleuchtung sonstiges"
* #RO04 ^property[0].code = #parent 
* #RO04 ^property[0].valueCode = #RO 
* #RO04 ^property[1].code = #child 
* #RO04 ^property[1].valueCode = #RO0401 
* #RO04 ^property[2].code = #child 
* #RO04 ^property[2].valueCode = #RO0402 
* #RO04 ^property[3].code = #child 
* #RO04 ^property[3].valueCode = #RO0403 
* #RO04 ^property[4].code = #child 
* #RO04 ^property[4].valueCode = #RO0404 
* #RO04 ^property[5].code = #child 
* #RO04 ^property[5].valueCode = #RO0405 
* #RO04 ^property[6].code = #child 
* #RO04 ^property[6].valueCode = #RO0406 
* #RO04 ^property[7].code = #child 
* #RO04 ^property[7].valueCode = #RO0407 
* #RO04 ^property[8].code = #child 
* #RO04 ^property[8].valueCode = #RO0408 
* #RO0401 "RO DL Sondenlage"
* #RO0401 ^property[0].code = #parent 
* #RO0401 ^property[0].valueCode = #RO04 
* #RO0402 "RO DL Port a Cath"
* #RO0402 ^property[0].code = #parent 
* #RO0402 ^property[0].valueCode = #RO04 
* #RO0403 "RO DL Stent Kontrolle"
* #RO0403 ^property[0].code = #parent 
* #RO0403 ^property[0].valueCode = #RO04 
* #RO0404 "RO DL Gastric Banding"
* #RO0404 ^property[0].code = #parent 
* #RO0404 ^property[0].valueCode = #RO04 
* #RO0405 "RO DL ZVK"
* #RO0405 ^property[0].code = #parent 
* #RO0405 ^property[0].valueCode = #RO04 
* #RO0406 "RO DL Thorax"
* #RO0406 ^property[0].code = #parent 
* #RO0406 ^property[0].valueCode = #RO04 
* #RO0407 "RO DL Abdomen"
* #RO0407 ^property[0].code = #parent 
* #RO0407 ^property[0].valueCode = #RO04 
* #RO0408 "RO Durchleuchtung Diverses"
* #RO0408 ^property[0].code = #parent 
* #RO0408 ^property[0].valueCode = #RO04 
* #RO05 "RO Durchleuchtung Intervention"
* #RO05 ^property[0].code = #parent 
* #RO05 ^property[0].valueCode = #RO 
* #RO05 ^property[1].code = #child 
* #RO05 ^property[1].valueCode = #RO0501 
* #RO05 ^property[2].code = #child 
* #RO05 ^property[2].valueCode = #RO0502 
* #RO05 ^property[3].code = #child 
* #RO05 ^property[3].valueCode = #RO0503 
* #RO05 ^property[4].code = #child 
* #RO05 ^property[4].valueCode = #RO0504 
* #RO05 ^property[5].code = #child 
* #RO05 ^property[5].valueCode = #RO0505 
* #RO05 ^property[6].code = #child 
* #RO05 ^property[6].valueCode = #RO0506 
* #RO05 ^property[7].code = #child 
* #RO05 ^property[7].valueCode = #RO0507 
* #RO05 ^property[8].code = #child 
* #RO05 ^property[8].valueCode = #RO0508 
* #RO0501 "RO DL Fistelfüllung"
* #RO0501 ^property[0].code = #parent 
* #RO0501 ^property[0].valueCode = #RO05 
* #RO0502 "RO DL Bougierung"
* #RO0502 ^property[0].code = #parent 
* #RO0502 ^property[0].valueCode = #RO05 
* #RO0503 "RO DL Sialographie"
* #RO0503 ^property[0].code = #parent 
* #RO0503 ^property[0].valueCode = #RO05 
* #RO0504 "RO DL Gelenksarthrographie"
* #RO0504 ^property[0].code = #parent 
* #RO0504 ^property[0].valueCode = #RO05 
* #RO0505 "RO DL Myelographie"
* #RO0505 ^property[0].code = #parent 
* #RO0505 ^property[0].valueCode = #RO05 
* #RO0506 "RO Diverses"
* #RO0506 ^property[0].code = #parent 
* #RO0506 ^property[0].valueCode = #RO05 
* #RO0507 "RO DL Nervenwurzelinfiltration"
* #RO0507 ^property[0].code = #parent 
* #RO0507 ^property[0].valueCode = #RO05 
* #RO0508 "RO DL Gelenkspunktion"
* #RO0508 ^property[0].code = #parent 
* #RO0508 ^property[0].valueCode = #RO05 
* #RO06 "RO Durchleuchtungen OP"
* #RO06 ^property[0].code = #parent 
* #RO06 ^property[0].valueCode = #RO 
* #RO06 ^property[1].code = #child 
* #RO06 ^property[1].valueCode = #RO0601 
* #RO06 ^property[2].code = #child 
* #RO06 ^property[2].valueCode = #RO0602 
* #RO06 ^property[3].code = #child 
* #RO06 ^property[3].valueCode = #RO0603 
* #RO06 ^property[4].code = #child 
* #RO06 ^property[4].valueCode = #RO0604 
* #RO06 ^property[5].code = #child 
* #RO06 ^property[5].valueCode = #RO0605 
* #RO06 ^property[6].code = #child 
* #RO06 ^property[6].valueCode = #RO0606 
* #RO06 ^property[7].code = #child 
* #RO06 ^property[7].valueCode = #RO0607 
* #RO0601 "RO OP DL Skelett"
* #RO0601 ^property[0].code = #parent 
* #RO0601 ^property[0].valueCode = #RO06 
* #RO0602 "RO OP DL Gelenk"
* #RO0602 ^property[0].code = #parent 
* #RO0602 ^property[0].valueCode = #RO06 
* #RO0603 "RO OP Durchleuchtung sonstiges"
* #RO0603 ^property[0].code = #parent 
* #RO0603 ^property[0].valueCode = #RO06 
* #RO0604 "RO OP DL Abdomen"
* #RO0604 ^property[0].code = #parent 
* #RO0604 ^property[0].valueCode = #RO06 
* #RO0605 "RO OP DL Thorax"
* #RO0605 ^property[0].code = #parent 
* #RO0605 ^property[0].valueCode = #RO06 
* #RO0606 "RO OP DL Gefäße"
* #RO0606 ^property[0].code = #parent 
* #RO0606 ^property[0].valueCode = #RO06 
* #RO0607 "RO OP DL Urologie"
* #RO0607 ^property[0].code = #parent 
* #RO0607 ^property[0].valueCode = #RO06 
* #RO0607 ^property[1].code = #child 
* #RO0607 ^property[1].valueCode = #RO060701 
* #RO0607 ^property[2].code = #child 
* #RO0607 ^property[2].valueCode = #RO060702 
* #RO0607 ^property[3].code = #child 
* #RO0607 ^property[3].valueCode = #RO060703 
* #RO0607 ^property[4].code = #child 
* #RO0607 ^property[4].valueCode = #RO060704 
* #RO0607 ^property[5].code = #child 
* #RO0607 ^property[5].valueCode = #RO060705 
* #RO0607 ^property[6].code = #child 
* #RO0607 ^property[6].valueCode = #RO060706 
* #RO0607 ^property[7].code = #child 
* #RO0607 ^property[7].valueCode = #RO060707 
* #RO0607 ^property[8].code = #child 
* #RO0607 ^property[8].valueCode = #RO060708 
* #RO0607 ^property[9].code = #child 
* #RO0607 ^property[9].valueCode = #RO060709 
* #RO0607 ^property[10].code = #child 
* #RO0607 ^property[10].valueCode = #RO060710 
* #RO060701 "RO OP DL Urologie Tauber"
* #RO060701 ^property[0].code = #parent 
* #RO060701 ^property[0].valueCode = #RO0607 
* #RO060702 "RO OP DL Urologie Lithotripsie"
* #RO060702 ^property[0].code = #parent 
* #RO060702 ^property[0].valueCode = #RO0607 
* #RO060703 "RO OP DL Urologie Splint"
* #RO060703 ^property[0].code = #parent 
* #RO060703 ^property[0].valueCode = #RO0607 
* #RO060704 "RO OP DL Urologie Splintwechsel"
* #RO060704 ^property[0].code = #parent 
* #RO060704 ^property[0].valueCode = #RO0607 
* #RO060705 "RO OP DL Urologie Pro Act"
* #RO060705 ^property[0].code = #parent 
* #RO060705 ^property[0].valueCode = #RO0607 
* #RO060706 "RO OP DL Urologie ESWL"
* #RO060706 ^property[0].code = #parent 
* #RO060706 ^property[0].valueCode = #RO0607 
* #RO060707 "RO OP DL Urologie Goldmarker"
* #RO060707 ^property[0].code = #parent 
* #RO060707 ^property[0].valueCode = #RO0607 
* #RO060708 "RO OP DL Urologie Sakrale Neuromodulation"
* #RO060708 ^property[0].code = #parent 
* #RO060708 ^property[0].valueCode = #RO0607 
* #RO060709 "RO OP DL Urologie Retrogrades Urethrogramm"
* #RO060709 ^property[0].code = #parent 
* #RO060709 ^property[0].valueCode = #RO0607 
* #RO060710 "RO OP DL Urologie perkutane Lithotripsie"
* #RO060710 ^property[0].code = #parent 
* #RO060710 ^property[0].valueCode = #RO0607 
* #RO07 "RO Phlebographie"
* #RO07 ^property[0].code = #parent 
* #RO07 ^property[0].valueCode = #RO 
* #RO07 ^property[1].code = #child 
* #RO07 ^property[1].valueCode = #RO0701 
* #RO07 ^property[2].code = #child 
* #RO07 ^property[2].valueCode = #RO0702 
* #RO07 ^property[3].code = #child 
* #RO07 ^property[3].valueCode = #RO0703 
* #RO07 ^property[4].code = #child 
* #RO07 ^property[4].valueCode = #RO0704 
* #RO07 ^property[5].code = #child 
* #RO07 ^property[5].valueCode = #RO0705 
* #RO07 ^property[6].code = #child 
* #RO07 ^property[6].valueCode = #RO0706 
* #RO07 ^property[7].code = #child 
* #RO07 ^property[7].valueCode = #RO0707 
* #RO0701 "RO Cavographie"
* #RO0701 ^property[0].code = #parent 
* #RO0701 ^property[0].valueCode = #RO07 
* #RO0702 "RO Phlebographie obere Extremität li."
* #RO0702 ^property[0].code = #parent 
* #RO0702 ^property[0].valueCode = #RO07 
* #RO0703 "RO Phlebographie obere Extremität re."
* #RO0703 ^property[0].code = #parent 
* #RO0703 ^property[0].valueCode = #RO07 
* #RO0704 "RO Phlebographie obere Extremität bds."
* #RO0704 ^property[0].code = #parent 
* #RO0704 ^property[0].valueCode = #RO07 
* #RO0705 "RO Phlebographie untere Extremität li."
* #RO0705 ^property[0].code = #parent 
* #RO0705 ^property[0].valueCode = #RO07 
* #RO0706 "RO Phlebographie untere Extremität re."
* #RO0706 ^property[0].code = #parent 
* #RO0706 ^property[0].valueCode = #RO07 
* #RO0707 "RO Phlebographie untere Extremität bds."
* #RO0707 ^property[0].code = #parent 
* #RO0707 ^property[0].valueCode = #RO07 
* #RO08 "RO Mamma"
* #RO08 ^property[0].code = #parent 
* #RO08 ^property[0].valueCode = #RO 
* #RO08 ^property[1].code = #child 
* #RO08 ^property[1].valueCode = #RO0801 
* #RO08 ^property[2].code = #child 
* #RO08 ^property[2].valueCode = #RO0802 
* #RO08 ^property[3].code = #child 
* #RO08 ^property[3].valueCode = #RO0803 
* #RO08 ^property[4].code = #child 
* #RO08 ^property[4].valueCode = #RO0804 
* #RO0801 "RO Mammographie li."
* #RO0801 ^property[0].code = #parent 
* #RO0801 ^property[0].valueCode = #RO08 
* #RO0802 "RO Mammographie re."
* #RO0802 ^property[0].code = #parent 
* #RO0802 ^property[0].valueCode = #RO08 
* #RO0803 "RO Mammographie bds."
* #RO0803 ^property[0].code = #parent 
* #RO0803 ^property[0].valueCode = #RO08 
* #RO0804 "RO Mamma Intervention"
* #RO0804 ^property[0].code = #parent 
* #RO0804 ^property[0].valueCode = #RO08 
* #RO0804 ^property[1].code = #child 
* #RO0804 ^property[1].valueCode = #RO080401 
* #RO0804 ^property[2].code = #child 
* #RO0804 ^property[2].valueCode = #RO080402 
* #RO0804 ^property[3].code = #child 
* #RO0804 ^property[3].valueCode = #RO080403 
* #RO0804 ^property[4].code = #child 
* #RO0804 ^property[4].valueCode = #RO080404 
* #RO0804 ^property[5].code = #child 
* #RO0804 ^property[5].valueCode = #RO080405 
* #RO0804 ^property[6].code = #child 
* #RO0804 ^property[6].valueCode = #RO080406 
* #RO080401 "RO Mammabiopsie"
* #RO080401 ^property[0].code = #parent 
* #RO080401 ^property[0].valueCode = #RO0804 
* #RO080401 ^property[1].code = #child 
* #RO080401 ^property[1].valueCode = #RO080401a 
* #RO080401 ^property[2].code = #child 
* #RO080401 ^property[2].valueCode = #RO080401b 
* #RO080401 ^property[3].code = #child 
* #RO080401 ^property[3].valueCode = #RO080401c 
* #RO080401 ^property[4].code = #child 
* #RO080401 ^property[4].valueCode = #RO080401d 
* #RO080401 ^property[5].code = #child 
* #RO080401 ^property[5].valueCode = #RO080401e 
* #RO080401 ^property[6].code = #child 
* #RO080401 ^property[6].valueCode = #RO080401f 
* #RO080401a "RO Mamma Vacuumbiopsie"
* #RO080401a ^property[0].code = #parent 
* #RO080401a ^property[0].valueCode = #RO080401 
* #RO080401b "RO Mamma Stanzbiopsie"
* #RO080401b ^property[0].code = #parent 
* #RO080401b ^property[0].valueCode = #RO080401 
* #RO080401c "RO Mammabiopsie - ultraschallgezielt"
* #RO080401c ^property[0].code = #parent 
* #RO080401c ^property[0].valueCode = #RO080401 
* #RO080401d "RO Mammabiopsie - bildwandlergezielt"
* #RO080401d ^property[0].code = #parent 
* #RO080401d ^property[0].valueCode = #RO080401 
* #RO080401e "RO Mammabiopsie - CT gezielt"
* #RO080401e ^property[0].code = #parent 
* #RO080401e ^property[0].valueCode = #RO080401 
* #RO080401f "RO Mammabiopsie - MR gezielt"
* #RO080401f ^property[0].code = #parent 
* #RO080401f ^property[0].valueCode = #RO080401 
* #RO080402 "RO Mammamarkierung präoperativ"
* #RO080402 ^property[0].code = #parent 
* #RO080402 ^property[0].valueCode = #RO0804 
* #RO080402 ^property[1].code = #child 
* #RO080402 ^property[1].valueCode = #RO080402a 
* #RO080402 ^property[2].code = #child 
* #RO080402 ^property[2].valueCode = #RO080402b 
* #RO080402 ^property[3].code = #child 
* #RO080402 ^property[3].valueCode = #RO080402c 
* #RO080402 ^property[4].code = #child 
* #RO080402 ^property[4].valueCode = #RO080402d 
* #RO080402a "RO Mammamarkierung präoperativ - ultraschallgezielt "
* #RO080402a ^property[0].code = #parent 
* #RO080402a ^property[0].valueCode = #RO080402 
* #RO080402b "RO Mammamarkierung präoperativ - bildwandlergezielt "
* #RO080402b ^property[0].code = #parent 
* #RO080402b ^property[0].valueCode = #RO080402 
* #RO080402c "RO Mammamarkierung präoperativ - CT gezielt "
* #RO080402c ^property[0].code = #parent 
* #RO080402c ^property[0].valueCode = #RO080402 
* #RO080402d "RO Mammamarkierung präoperativ - MR gezielt "
* #RO080402d ^property[0].code = #parent 
* #RO080402d ^property[0].valueCode = #RO080402 
* #RO080403 "RO MammaDrainage"
* #RO080403 ^property[0].code = #parent 
* #RO080403 ^property[0].valueCode = #RO0804 
* #RO080403 ^property[1].code = #child 
* #RO080403 ^property[1].valueCode = #RO080403a 
* #RO080403 ^property[2].code = #child 
* #RO080403 ^property[2].valueCode = #RO080403b 
* #RO080403 ^property[3].code = #child 
* #RO080403 ^property[3].valueCode = #RO080403c 
* #RO080403 ^property[4].code = #child 
* #RO080403 ^property[4].valueCode = #RO080403d 
* #RO080403a "RO MammaDrainage - ultraschallgezielt"
* #RO080403a ^property[0].code = #parent 
* #RO080403a ^property[0].valueCode = #RO080403 
* #RO080403b "RO MammaDrainage - bildwandlergezielt"
* #RO080403b ^property[0].code = #parent 
* #RO080403b ^property[0].valueCode = #RO080403 
* #RO080403c "RO MammaDrainage - CT gezielt"
* #RO080403c ^property[0].code = #parent 
* #RO080403c ^property[0].valueCode = #RO080403 
* #RO080403d "RO MammaDrainage - MR gezielt"
* #RO080403d ^property[0].code = #parent 
* #RO080403d ^property[0].valueCode = #RO080403 
* #RO080404 "RO Galaktographie"
* #RO080404 ^property[0].code = #parent 
* #RO080404 ^property[0].valueCode = #RO0804 
* #RO080404 ^property[1].code = #child 
* #RO080404 ^property[1].valueCode = #RO080404a 
* #RO080404 ^property[2].code = #child 
* #RO080404 ^property[2].valueCode = #RO080404b 
* #RO080404a "RO Galaktographie li."
* #RO080404a ^property[0].code = #parent 
* #RO080404a ^property[0].valueCode = #RO080404 
* #RO080404b "RO Galaktographie re."
* #RO080404b ^property[0].code = #parent 
* #RO080404b ^property[0].valueCode = #RO080404 
* #RO080405 "RO Mamma Pneumocystografie"
* #RO080405 ^property[0].code = #parent 
* #RO080405 ^property[0].valueCode = #RO0804 
* #RO080405 ^property[1].code = #child 
* #RO080405 ^property[1].valueCode = #RO080405a 
* #RO080405 ^property[2].code = #child 
* #RO080405 ^property[2].valueCode = #RO080405b 
* #RO080405a "RO Mamma Pneumocystografie li."
* #RO080405a ^property[0].code = #parent 
* #RO080405a ^property[0].valueCode = #RO080405 
* #RO080405b "RO Mamma Pneumocystografie re."
* #RO080405b ^property[0].code = #parent 
* #RO080405b ^property[0].valueCode = #RO080405 
* #RO080406 "RO Mamma Präparataufnahme"
* #RO080406 ^property[0].code = #parent 
* #RO080406 ^property[0].valueCode = #RO0804 
* #RO09 "RO Skelett"
* #RO09 ^property[0].code = #parent 
* #RO09 ^property[0].valueCode = #RO 
* #RO09 ^property[1].code = #child 
* #RO09 ^property[1].valueCode = #RO0901 
* #RO09 ^property[2].code = #child 
* #RO09 ^property[2].valueCode = #RO0902 
* #RO09 ^property[3].code = #child 
* #RO09 ^property[3].valueCode = #RO0903 
* #RO09 ^property[4].code = #child 
* #RO09 ^property[4].valueCode = #RO0904 
* #RO09 ^property[5].code = #child 
* #RO09 ^property[5].valueCode = #RO0905 
* #RO09 ^property[6].code = #child 
* #RO09 ^property[6].valueCode = #RO0906 
* #RO09 ^property[7].code = #child 
* #RO09 ^property[7].valueCode = #RO0907 
* #RO09 ^property[8].code = #child 
* #RO09 ^property[8].valueCode = #RO0908 
* #RO09 ^property[9].code = #child 
* #RO09 ^property[9].valueCode = #RO0909 
* #RO0901 "RO Hirnschädel"
* #RO0901 ^property[0].code = #parent 
* #RO0901 ^property[0].valueCode = #RO09 
* #RO0901 ^property[1].code = #child 
* #RO0901 ^property[1].valueCode = #RO090101 
* #RO0901 ^property[2].code = #child 
* #RO0901 ^property[2].valueCode = #RO090102 
* #RO0901 ^property[3].code = #child 
* #RO0901 ^property[3].valueCode = #RO090103 
* #RO0901 ^property[4].code = #child 
* #RO0901 ^property[4].valueCode = #RO090104 
* #RO0901 ^property[5].code = #child 
* #RO0901 ^property[5].valueCode = #RO090105 
* #RO090101 "RO Schädel"
* #RO090101 ^property[0].code = #parent 
* #RO090101 ^property[0].valueCode = #RO0901 
* #RO090101 ^property[1].code = #child 
* #RO090101 ^property[1].valueCode = #RO090101a 
* #RO090101 ^property[2].code = #child 
* #RO090101 ^property[2].valueCode = #RO090101b 
* #RO090101a "RO Hinterhaupt"
* #RO090101a ^property[0].code = #parent 
* #RO090101a ^property[0].valueCode = #RO090101 
* #RO090101b "RO Schädelbasis"
* #RO090101b ^property[0].code = #parent 
* #RO090101b ^property[0].valueCode = #RO090101 
* #RO090102 "RO Sella"
* #RO090102 ^property[0].code = #parent 
* #RO090102 ^property[0].valueCode = #RO0901 
* #RO090103 "RO Felsenbein li."
* #RO090103 ^property[0].code = #parent 
* #RO090103 ^property[0].valueCode = #RO0901 
* #RO090103 ^property[1].code = #child 
* #RO090103 ^property[1].valueCode = #RO090103a 
* #RO090103a "RO Felsenbein li. nach Chausse"
* #RO090103a ^property[0].code = #parent 
* #RO090103a ^property[0].valueCode = #RO090103 
* #RO090104 "RO Felsenbein re."
* #RO090104 ^property[0].code = #parent 
* #RO090104 ^property[0].valueCode = #RO0901 
* #RO090104 ^property[1].code = #child 
* #RO090104 ^property[1].valueCode = #RO090104a 
* #RO090104a "RO Felsenbein re. nach Chausse"
* #RO090104a ^property[0].code = #parent 
* #RO090104a ^property[0].valueCode = #RO090104 
* #RO090105 "RO Felsenbein bds."
* #RO090105 ^property[0].code = #parent 
* #RO090105 ^property[0].valueCode = #RO0901 
* #RO090105 ^property[1].code = #child 
* #RO090105 ^property[1].valueCode = #RO090105a 
* #RO090105a "RO Felsenbein bds. nach Chausse"
* #RO090105a ^property[0].code = #parent 
* #RO090105a ^property[0].valueCode = #RO090105 
* #RO0902 "RO Gesichtsschädel"
* #RO0902 ^property[0].code = #parent 
* #RO0902 ^property[0].valueCode = #RO09 
* #RO0902 ^property[1].code = #child 
* #RO0902 ^property[1].valueCode = #RO090201 
* #RO0902 ^property[2].code = #child 
* #RO0902 ^property[2].valueCode = #RO090202 
* #RO0902 ^property[3].code = #child 
* #RO0902 ^property[3].valueCode = #RO090203 
* #RO0902 ^property[4].code = #child 
* #RO0902 ^property[4].valueCode = #RO090204 
* #RO0902 ^property[5].code = #child 
* #RO0902 ^property[5].valueCode = #RO090205 
* #RO0902 ^property[6].code = #child 
* #RO0902 ^property[6].valueCode = #RO090206 
* #RO0902 ^property[7].code = #child 
* #RO0902 ^property[7].valueCode = #RO090207 
* #RO0902 ^property[8].code = #child 
* #RO0902 ^property[8].valueCode = #RO090208 
* #RO0902 ^property[9].code = #child 
* #RO0902 ^property[9].valueCode = #RO090209 
* #RO0902 ^property[10].code = #child 
* #RO0902 ^property[10].valueCode = #RO090210 
* #RO0902 ^property[11].code = #child 
* #RO0902 ^property[11].valueCode = #RO090211 
* #RO0902 ^property[12].code = #child 
* #RO0902 ^property[12].valueCode = #RO090212 
* #RO0902 ^property[13].code = #child 
* #RO0902 ^property[13].valueCode = #RO090213 
* #RO0902 ^property[14].code = #child 
* #RO0902 ^property[14].valueCode = #RO090214 
* #RO090201 "RO Kiefergelenk li."
* #RO090201 ^property[0].code = #parent 
* #RO090201 ^property[0].valueCode = #RO0902 
* #RO090201 ^property[1].code = #child 
* #RO090201 ^property[1].valueCode = #RO090201a 
* #RO090201a "RO Kieferköpfchen li."
* #RO090201a ^property[0].code = #parent 
* #RO090201a ^property[0].valueCode = #RO090201 
* #RO090202 "RO Kiefergelenk re."
* #RO090202 ^property[0].code = #parent 
* #RO090202 ^property[0].valueCode = #RO0902 
* #RO090202 ^property[1].code = #child 
* #RO090202 ^property[1].valueCode = #RO090202a 
* #RO090202a "RO Kieferköpfchen re."
* #RO090202a ^property[0].code = #parent 
* #RO090202a ^property[0].valueCode = #RO090202 
* #RO090203 "RO Kiefergelenk bds."
* #RO090203 ^property[0].code = #parent 
* #RO090203 ^property[0].valueCode = #RO0902 
* #RO090203 ^property[1].code = #child 
* #RO090203 ^property[1].valueCode = #RO090203a 
* #RO090203a "RO Kieferköpfchen bds."
* #RO090203a ^property[0].code = #parent 
* #RO090203a ^property[0].valueCode = #RO090203 
* #RO090204 "RO Orbita li."
* #RO090204 ^property[0].code = #parent 
* #RO090204 ^property[0].valueCode = #RO0902 
* #RO090205 "RO Orbita re."
* #RO090205 ^property[0].code = #parent 
* #RO090205 ^property[0].valueCode = #RO0902 
* #RO090206 "RO Orbita bds."
* #RO090206 ^property[0].code = #parent 
* #RO090206 ^property[0].valueCode = #RO0902 
* #RO090207 "RO Jochbogen li."
* #RO090207 ^property[0].code = #parent 
* #RO090207 ^property[0].valueCode = #RO0902 
* #RO090207 ^property[1].code = #child 
* #RO090207 ^property[1].valueCode = #RO090207a 
* #RO090207 ^property[2].code = #child 
* #RO090207 ^property[2].valueCode = #RO090207b 
* #RO090207a "RO Jochbogen li. Henkeltopf"
* #RO090207a ^property[0].code = #parent 
* #RO090207a ^property[0].valueCode = #RO090207 
* #RO090207b "RO Jochbogen li. Zimmer"
* #RO090207b ^property[0].code = #parent 
* #RO090207b ^property[0].valueCode = #RO090207 
* #RO090208 "RO Jochbogen re."
* #RO090208 ^property[0].code = #parent 
* #RO090208 ^property[0].valueCode = #RO0902 
* #RO090208 ^property[1].code = #child 
* #RO090208 ^property[1].valueCode = #RO090208a 
* #RO090208 ^property[2].code = #child 
* #RO090208 ^property[2].valueCode = #RO090208b 
* #RO090208a "RO Jochbogen re. Henkeltopf"
* #RO090208a ^property[0].code = #parent 
* #RO090208a ^property[0].valueCode = #RO090208 
* #RO090208b "RO Jochbogen re. Zimmer"
* #RO090208b ^property[0].code = #parent 
* #RO090208b ^property[0].valueCode = #RO090208 
* #RO090209 "RO Jochbogen bds."
* #RO090209 ^property[0].code = #parent 
* #RO090209 ^property[0].valueCode = #RO0902 
* #RO090209 ^property[1].code = #child 
* #RO090209 ^property[1].valueCode = #RO090209a 
* #RO090209 ^property[2].code = #child 
* #RO090209 ^property[2].valueCode = #RO090209b 
* #RO090209a "RO Jochbogen bds. Henkeltopf"
* #RO090209a ^property[0].code = #parent 
* #RO090209a ^property[0].valueCode = #RO090209 
* #RO090209b "RO Jochbogen bds. Zimmer"
* #RO090209b ^property[0].code = #parent 
* #RO090209b ^property[0].valueCode = #RO090209 
* #RO090210 "RO Nasenbein"
* #RO090210 ^property[0].code = #parent 
* #RO090210 ^property[0].valueCode = #RO0902 
* #RO090211 "RO Unterkiefer bds."
* #RO090211 ^property[0].code = #parent 
* #RO090211 ^property[0].valueCode = #RO0902 
* #RO090211 ^property[1].code = #child 
* #RO090211 ^property[1].valueCode = #RO090211a 
* #RO090211 ^property[2].code = #child 
* #RO090211 ^property[2].valueCode = #RO090211b 
* #RO090211 ^property[3].code = #child 
* #RO090211 ^property[3].valueCode = #RO090211c 
* #RO090211a "RO Unterkiefer bds. Eisler"
* #RO090211a ^property[0].code = #parent 
* #RO090211a ^property[0].valueCode = #RO090211 
* #RO090211b "RO Unterkiefer bds. Altschuluffenorde"
* #RO090211b ^property[0].code = #parent 
* #RO090211b ^property[0].valueCode = #RO090211 
* #RO090211c "RO Unterkiefer bds. Clementschitsch"
* #RO090211c ^property[0].code = #parent 
* #RO090211c ^property[0].valueCode = #RO090211 
* #RO090212 "RO NNH"
* #RO090212 ^property[0].code = #parent 
* #RO090212 ^property[0].valueCode = #RO0902 
* #RO090213 "RO Unterkiefer li."
* #RO090213 ^property[0].code = #parent 
* #RO090213 ^property[0].valueCode = #RO0902 
* #RO090213 ^property[1].code = #child 
* #RO090213 ^property[1].valueCode = #RO090213a 
* #RO090213 ^property[2].code = #child 
* #RO090213 ^property[2].valueCode = #RO090213b 
* #RO090213 ^property[3].code = #child 
* #RO090213 ^property[3].valueCode = #RO090213c 
* #RO090213a "RO Unterkiefer li. Eisler"
* #RO090213a ^property[0].code = #parent 
* #RO090213a ^property[0].valueCode = #RO090213 
* #RO090213b "RO Unterkiefer li. Altschuluffenorde"
* #RO090213b ^property[0].code = #parent 
* #RO090213b ^property[0].valueCode = #RO090213 
* #RO090213c "RO Unterkiefer li. Clementschitsch"
* #RO090213c ^property[0].code = #parent 
* #RO090213c ^property[0].valueCode = #RO090213 
* #RO090214 "RO Unterkiefer re."
* #RO090214 ^property[0].code = #parent 
* #RO090214 ^property[0].valueCode = #RO0902 
* #RO090214 ^property[1].code = #child 
* #RO090214 ^property[1].valueCode = #RO090214a 
* #RO090214 ^property[2].code = #child 
* #RO090214 ^property[2].valueCode = #RO090214b 
* #RO090214 ^property[3].code = #child 
* #RO090214 ^property[3].valueCode = #RO090214c 
* #RO090214a "RO Unterkiefer re. Eisler"
* #RO090214a ^property[0].code = #parent 
* #RO090214a ^property[0].valueCode = #RO090214 
* #RO090214b "RO Unterkiefer re. Altschuluffenorde"
* #RO090214b ^property[0].code = #parent 
* #RO090214b ^property[0].valueCode = #RO090214 
* #RO090214c "RO Unterkiefer re. Clementschitsch"
* #RO090214c ^property[0].code = #parent 
* #RO090214c ^property[0].valueCode = #RO090214 
* #RO0903 "RO Obere Extremität"
* #RO0903 ^property[0].code = #parent 
* #RO0903 ^property[0].valueCode = #RO09 
* #RO0903 ^property[1].code = #child 
* #RO0903 ^property[1].valueCode = #RO090301 
* #RO0903 ^property[2].code = #child 
* #RO0903 ^property[2].valueCode = #RO090302 
* #RO0903 ^property[3].code = #child 
* #RO0903 ^property[3].valueCode = #RO090303 
* #RO0903 ^property[4].code = #child 
* #RO0903 ^property[4].valueCode = #RO090304 
* #RO0903 ^property[5].code = #child 
* #RO0903 ^property[5].valueCode = #RO090305 
* #RO0903 ^property[6].code = #child 
* #RO0903 ^property[6].valueCode = #RO090306 
* #RO0903 ^property[7].code = #child 
* #RO0903 ^property[7].valueCode = #RO090307 
* #RO0903 ^property[8].code = #child 
* #RO0903 ^property[8].valueCode = #RO090308 
* #RO0903 ^property[9].code = #child 
* #RO0903 ^property[9].valueCode = #RO090309 
* #RO0903 ^property[10].code = #child 
* #RO0903 ^property[10].valueCode = #RO090310 
* #RO0903 ^property[11].code = #child 
* #RO0903 ^property[11].valueCode = #RO090311 
* #RO0903 ^property[12].code = #child 
* #RO0903 ^property[12].valueCode = #RO090312 
* #RO0903 ^property[13].code = #child 
* #RO0903 ^property[13].valueCode = #RO090313 
* #RO0903 ^property[14].code = #child 
* #RO0903 ^property[14].valueCode = #RO090314 
* #RO0903 ^property[15].code = #child 
* #RO0903 ^property[15].valueCode = #RO090315 
* #RO0903 ^property[16].code = #child 
* #RO0903 ^property[16].valueCode = #RO090316 
* #RO0903 ^property[17].code = #child 
* #RO0903 ^property[17].valueCode = #RO090317 
* #RO0903 ^property[18].code = #child 
* #RO0903 ^property[18].valueCode = #RO090318 
* #RO0903 ^property[19].code = #child 
* #RO0903 ^property[19].valueCode = #RO090319 
* #RO0903 ^property[20].code = #child 
* #RO0903 ^property[20].valueCode = #RO090320 
* #RO0903 ^property[21].code = #child 
* #RO0903 ^property[21].valueCode = #RO090321 
* #RO0903 ^property[22].code = #child 
* #RO0903 ^property[22].valueCode = #RO090322 
* #RO0903 ^property[23].code = #child 
* #RO0903 ^property[23].valueCode = #RO090323 
* #RO0903 ^property[24].code = #child 
* #RO0903 ^property[24].valueCode = #RO090324 
* #RO0903 ^property[25].code = #child 
* #RO0903 ^property[25].valueCode = #RO090325 
* #RO0903 ^property[26].code = #child 
* #RO0903 ^property[26].valueCode = #RO090326 
* #RO0903 ^property[27].code = #child 
* #RO0903 ^property[27].valueCode = #RO090327 
* #RO0903 ^property[28].code = #child 
* #RO0903 ^property[28].valueCode = #RO090328 
* #RO0903 ^property[29].code = #child 
* #RO0903 ^property[29].valueCode = #RO090329 
* #RO0903 ^property[30].code = #child 
* #RO0903 ^property[30].valueCode = #RO090330 
* #RO0903 ^property[31].code = #child 
* #RO0903 ^property[31].valueCode = #RO090331 
* #RO0903 ^property[32].code = #child 
* #RO0903 ^property[32].valueCode = #RO090332 
* #RO0903 ^property[33].code = #child 
* #RO0903 ^property[33].valueCode = #RO090333 
* #RO0903 ^property[34].code = #child 
* #RO0903 ^property[34].valueCode = #RO090334 
* #RO0903 ^property[35].code = #child 
* #RO0903 ^property[35].valueCode = #RO090335 
* #RO0903 ^property[36].code = #child 
* #RO0903 ^property[36].valueCode = #RO090336 
* #RO090301 "RO Clavicula li."
* #RO090301 ^property[0].code = #parent 
* #RO090301 ^property[0].valueCode = #RO0903 
* #RO090301 ^property[1].code = #child 
* #RO090301 ^property[1].valueCode = #RO090301a 
* #RO090301 ^property[2].code = #child 
* #RO090301 ^property[2].valueCode = #RO090301b 
* #RO090301a "RO Clavicula Belastung li."
* #RO090301a ^property[0].code = #parent 
* #RO090301a ^property[0].valueCode = #RO090301 
* #RO090301b "RO Clavicula tangential li."
* #RO090301b ^property[0].code = #parent 
* #RO090301b ^property[0].valueCode = #RO090301 
* #RO090302 "RO Clavicula re."
* #RO090302 ^property[0].code = #parent 
* #RO090302 ^property[0].valueCode = #RO0903 
* #RO090302 ^property[1].code = #child 
* #RO090302 ^property[1].valueCode = #RO090302a 
* #RO090302 ^property[2].code = #child 
* #RO090302 ^property[2].valueCode = #RO090302b 
* #RO090302a "RO Clavicula Belastung re."
* #RO090302a ^property[0].code = #parent 
* #RO090302a ^property[0].valueCode = #RO090302 
* #RO090302b "RO Clavicula tangential re."
* #RO090302b ^property[0].code = #parent 
* #RO090302b ^property[0].valueCode = #RO090302 
* #RO090303 "RO Clavicula bds."
* #RO090303 ^property[0].code = #parent 
* #RO090303 ^property[0].valueCode = #RO0903 
* #RO090304 "RO AC Gelenk li."
* #RO090304 ^property[0].code = #parent 
* #RO090304 ^property[0].valueCode = #RO0903 
* #RO090305 "RO AC Gelenk re."
* #RO090305 ^property[0].code = #parent 
* #RO090305 ^property[0].valueCode = #RO0903 
* #RO090306 "RO AC Gelenk bds."
* #RO090306 ^property[0].code = #parent 
* #RO090306 ^property[0].valueCode = #RO0903 
* #RO090307 "RO Sternoclavikular Gelenk li."
* #RO090307 ^property[0].code = #parent 
* #RO090307 ^property[0].valueCode = #RO0903 
* #RO090308 "RO Sternoclavikular Gelenk re."
* #RO090308 ^property[0].code = #parent 
* #RO090308 ^property[0].valueCode = #RO0903 
* #RO090309 "RO Sternoclavikular Gelenk bds."
* #RO090309 ^property[0].code = #parent 
* #RO090309 ^property[0].valueCode = #RO0903 
* #RO090310 "RO Scapula li."
* #RO090310 ^property[0].code = #parent 
* #RO090310 ^property[0].valueCode = #RO0903 
* #RO090311 "RO Scapula re."
* #RO090311 ^property[0].code = #parent 
* #RO090311 ^property[0].valueCode = #RO0903 
* #RO090312 "RO Scapula bds"
* #RO090312 ^property[0].code = #parent 
* #RO090312 ^property[0].valueCode = #RO0903 
* #RO090313 "RO Schulter li."
* #RO090313 ^property[0].code = #parent 
* #RO090313 ^property[0].valueCode = #RO0903 
* #RO090313 ^property[1].code = #child 
* #RO090313 ^property[1].valueCode = #RO090313a 
* #RO090313 ^property[2].code = #child 
* #RO090313 ^property[2].valueCode = #RO090313b 
* #RO090313 ^property[3].code = #child 
* #RO090313 ^property[3].valueCode = #RO090313c 
* #RO090313 ^property[4].code = #child 
* #RO090313 ^property[4].valueCode = #RO090313d 
* #RO090313 ^property[5].code = #child 
* #RO090313 ^property[5].valueCode = #RO090313e 
* #RO090313 ^property[6].code = #child 
* #RO090313 ^property[6].valueCode = #RO090313f 
* #RO090313 ^property[7].code = #child 
* #RO090313 ^property[7].valueCode = #RO090313g 
* #RO090313a "RO Schulter Outlet View li."
* #RO090313a ^property[0].code = #parent 
* #RO090313a ^property[0].valueCode = #RO090313 
* #RO090313b "RO Schulter axial li."
* #RO090313b ^property[0].code = #parent 
* #RO090313b ^property[0].valueCode = #RO090313 
* #RO090313c "RO Schulter ap li."
* #RO090313c ^property[0].code = #parent 
* #RO090313c ^property[0].valueCode = #RO090313 
* #RO090313d "RO Schulter Y-Aufnahme li."
* #RO090313d ^property[0].code = #parent 
* #RO090313d ^property[0].valueCode = #RO090313 
* #RO090313e "RO Schulter Neer-Lache li."
* #RO090313e ^property[0].code = #parent 
* #RO090313e ^property[0].valueCode = #RO090313 
* #RO090313f "RO Schulter transthorakal li."
* #RO090313f ^property[0].code = #parent 
* #RO090313f ^property[0].valueCode = #RO090313 
* #RO090313g "RO Schulter Velpeau li."
* #RO090313g ^property[0].code = #parent 
* #RO090313g ^property[0].valueCode = #RO090313 
* #RO090314 "RO Schulter re."
* #RO090314 ^property[0].code = #parent 
* #RO090314 ^property[0].valueCode = #RO0903 
* #RO090314 ^property[1].code = #child 
* #RO090314 ^property[1].valueCode = #RO090314a 
* #RO090314 ^property[2].code = #child 
* #RO090314 ^property[2].valueCode = #RO090314b 
* #RO090314 ^property[3].code = #child 
* #RO090314 ^property[3].valueCode = #RO090314c 
* #RO090314 ^property[4].code = #child 
* #RO090314 ^property[4].valueCode = #RO090314d 
* #RO090314 ^property[5].code = #child 
* #RO090314 ^property[5].valueCode = #RO090314e 
* #RO090314 ^property[6].code = #child 
* #RO090314 ^property[6].valueCode = #RO090314f 
* #RO090314 ^property[7].code = #child 
* #RO090314 ^property[7].valueCode = #RO090314g 
* #RO090314a "RO Schulter Outlet View re."
* #RO090314a ^property[0].code = #parent 
* #RO090314a ^property[0].valueCode = #RO090314 
* #RO090314b "RO Schulter axial re."
* #RO090314b ^property[0].code = #parent 
* #RO090314b ^property[0].valueCode = #RO090314 
* #RO090314c "RO Schulter ap re."
* #RO090314c ^property[0].code = #parent 
* #RO090314c ^property[0].valueCode = #RO090314 
* #RO090314d "RO Schulter Y-Aufnahme re."
* #RO090314d ^property[0].code = #parent 
* #RO090314d ^property[0].valueCode = #RO090314 
* #RO090314e "RO Schulter Neer-Lache re."
* #RO090314e ^property[0].code = #parent 
* #RO090314e ^property[0].valueCode = #RO090314 
* #RO090314f "RO Schulter transthorakal re."
* #RO090314f ^property[0].code = #parent 
* #RO090314f ^property[0].valueCode = #RO090314 
* #RO090314g "RO Schulter Velpeau re."
* #RO090314g ^property[0].code = #parent 
* #RO090314g ^property[0].valueCode = #RO090314 
* #RO090315 "RO Schulter bds."
* #RO090315 ^property[0].code = #parent 
* #RO090315 ^property[0].valueCode = #RO0903 
* #RO090315 ^property[1].code = #child 
* #RO090315 ^property[1].valueCode = #RO090315a 
* #RO090315 ^property[2].code = #child 
* #RO090315 ^property[2].valueCode = #RO090315b 
* #RO090315 ^property[3].code = #child 
* #RO090315 ^property[3].valueCode = #RO090315c 
* #RO090315 ^property[4].code = #child 
* #RO090315 ^property[4].valueCode = #RO090315d 
* #RO090315 ^property[5].code = #child 
* #RO090315 ^property[5].valueCode = #RO090315e 
* #RO090315 ^property[6].code = #child 
* #RO090315 ^property[6].valueCode = #RO090315f 
* #RO090315 ^property[7].code = #child 
* #RO090315 ^property[7].valueCode = #RO090315g 
* #RO090315a "RO Schulter Outlet bds."
* #RO090315a ^property[0].code = #parent 
* #RO090315a ^property[0].valueCode = #RO090315 
* #RO090315b "RO Schulter axial bds."
* #RO090315b ^property[0].code = #parent 
* #RO090315b ^property[0].valueCode = #RO090315 
* #RO090315c "RO Schulter ap bds."
* #RO090315c ^property[0].code = #parent 
* #RO090315c ^property[0].valueCode = #RO090315 
* #RO090315d "RO Schulter Y-Aufnahme bds."
* #RO090315d ^property[0].code = #parent 
* #RO090315d ^property[0].valueCode = #RO090315 
* #RO090315e "RO Schulter Neer-Lache bds."
* #RO090315e ^property[0].code = #parent 
* #RO090315e ^property[0].valueCode = #RO090315 
* #RO090315f "RO Schulter transthorakal bds."
* #RO090315f ^property[0].code = #parent 
* #RO090315f ^property[0].valueCode = #RO090315 
* #RO090315g "RO Schulter Velpeau bds."
* #RO090315g ^property[0].code = #parent 
* #RO090315g ^property[0].valueCode = #RO090315 
* #RO090316 "RO obere Thoraxapertur"
* #RO090316 ^property[0].code = #parent 
* #RO090316 ^property[0].valueCode = #RO0903 
* #RO090317 "RO Oberarm li."
* #RO090317 ^property[0].code = #parent 
* #RO090317 ^property[0].valueCode = #RO0903 
* #RO090318 "RO Oberarm re."
* #RO090318 ^property[0].code = #parent 
* #RO090318 ^property[0].valueCode = #RO0903 
* #RO090319 "RO Oberarm bds."
* #RO090319 ^property[0].code = #parent 
* #RO090319 ^property[0].valueCode = #RO0903 
* #RO090320 "RO Ellbogen li."
* #RO090320 ^property[0].code = #parent 
* #RO090320 ^property[0].valueCode = #RO0903 
* #RO090320 ^property[1].code = #child 
* #RO090320 ^property[1].valueCode = #RO090320a 
* #RO090320 ^property[2].code = #child 
* #RO090320 ^property[2].valueCode = #RO090320b 
* #RO090320 ^property[3].code = #child 
* #RO090320 ^property[3].valueCode = #RO090320c 
* #RO090320a "RO Ellbogen Olecranon axial li. "
* #RO090320a ^property[0].code = #parent 
* #RO090320a ^property[0].valueCode = #RO090320 
* #RO090320b "RO Ellbogen Processus Coronideus 45 Grad li."
* #RO090320b ^property[0].code = #parent 
* #RO090320b ^property[0].valueCode = #RO090320 
* #RO090320c "RO Ellbogen Radiusköpfchen 45 Grad li."
* #RO090320c ^property[0].code = #parent 
* #RO090320c ^property[0].valueCode = #RO090320 
* #RO090321 "RO Ellbogen re."
* #RO090321 ^property[0].code = #parent 
* #RO090321 ^property[0].valueCode = #RO0903 
* #RO090321 ^property[1].code = #child 
* #RO090321 ^property[1].valueCode = #RO090321a 
* #RO090321 ^property[2].code = #child 
* #RO090321 ^property[2].valueCode = #RO090321b 
* #RO090321 ^property[3].code = #child 
* #RO090321 ^property[3].valueCode = #RO090321c 
* #RO090321a "RO Ellbogen Olecranon axial re."
* #RO090321a ^property[0].code = #parent 
* #RO090321a ^property[0].valueCode = #RO090321 
* #RO090321b "RO Ellbogen Processus Coronideus 45 Grad re."
* #RO090321b ^property[0].code = #parent 
* #RO090321b ^property[0].valueCode = #RO090321 
* #RO090321c "RO Ellbogen Radiusköpfchen 45 Grad re."
* #RO090321c ^property[0].code = #parent 
* #RO090321c ^property[0].valueCode = #RO090321 
* #RO090322 "RO Ellbogen bds."
* #RO090322 ^property[0].code = #parent 
* #RO090322 ^property[0].valueCode = #RO0903 
* #RO090322 ^property[1].code = #child 
* #RO090322 ^property[1].valueCode = #RO090322a 
* #RO090322 ^property[2].code = #child 
* #RO090322 ^property[2].valueCode = #RO090322b 
* #RO090322 ^property[3].code = #child 
* #RO090322 ^property[3].valueCode = #RO090322c 
* #RO090322a "RO Ellbogen Olecranon axial bds."
* #RO090322a ^property[0].code = #parent 
* #RO090322a ^property[0].valueCode = #RO090322 
* #RO090322b "RO Ellbogen Processus Coronideus 45 Grad bds."
* #RO090322b ^property[0].code = #parent 
* #RO090322b ^property[0].valueCode = #RO090322 
* #RO090322c "RO Ellbogen Radiusköpfchen 45 Grad bds."
* #RO090322c ^property[0].code = #parent 
* #RO090322c ^property[0].valueCode = #RO090322 
* #RO090323 "RO Unterarm li."
* #RO090323 ^property[0].code = #parent 
* #RO090323 ^property[0].valueCode = #RO0903 
* #RO090324 "RO Unterarm re."
* #RO090324 ^property[0].code = #parent 
* #RO090324 ^property[0].valueCode = #RO0903 
* #RO090325 "RO Unterarm bds."
* #RO090325 ^property[0].code = #parent 
* #RO090325 ^property[0].valueCode = #RO0903 
* #RO090326 "RO Handgelenk li."
* #RO090326 ^property[0].code = #parent 
* #RO090326 ^property[0].valueCode = #RO0903 
* #RO090326 ^property[1].code = #child 
* #RO090326 ^property[1].valueCode = #RO090326a 
* #RO090326 ^property[2].code = #child 
* #RO090326 ^property[2].valueCode = #RO090326b 
* #RO090326a "RO Handgelenk schräg li."
* #RO090326a ^property[0].code = #parent 
* #RO090326a ^property[0].valueCode = #RO090326 
* #RO090326b "RO Handgelenk Carpaltunnel li."
* #RO090326b ^property[0].code = #parent 
* #RO090326b ^property[0].valueCode = #RO090326 
* #RO090327 "RO Handgelenk re."
* #RO090327 ^property[0].code = #parent 
* #RO090327 ^property[0].valueCode = #RO0903 
* #RO090327 ^property[1].code = #child 
* #RO090327 ^property[1].valueCode = #RO090327a 
* #RO090327 ^property[2].code = #child 
* #RO090327 ^property[2].valueCode = #RO090327b 
* #RO090327a "RO Handgelenk schräg re."
* #RO090327a ^property[0].code = #parent 
* #RO090327a ^property[0].valueCode = #RO090327 
* #RO090327b "RO Handgelenk Carpaltunnel re."
* #RO090327b ^property[0].code = #parent 
* #RO090327b ^property[0].valueCode = #RO090327 
* #RO090328 "RO Handgelenk bds."
* #RO090328 ^property[0].code = #parent 
* #RO090328 ^property[0].valueCode = #RO0903 
* #RO090328 ^property[1].code = #child 
* #RO090328 ^property[1].valueCode = #RO090328a 
* #RO090328 ^property[2].code = #child 
* #RO090328 ^property[2].valueCode = #RO090328b 
* #RO090328a "RO Handgelenk schräg bds."
* #RO090328a ^property[0].code = #parent 
* #RO090328a ^property[0].valueCode = #RO090328 
* #RO090328b "RO Handgelenk Carpaltunnel bds."
* #RO090328b ^property[0].code = #parent 
* #RO090328b ^property[0].valueCode = #RO090328 
* #RO090329 "RO Navicularserie li."
* #RO090329 ^property[0].code = #parent 
* #RO090329 ^property[0].valueCode = #RO0903 
* #RO090330 "RO Navicularserie re."
* #RO090330 ^property[0].code = #parent 
* #RO090330 ^property[0].valueCode = #RO0903 
* #RO090331 "RO Navicularserie bds."
* #RO090331 ^property[0].code = #parent 
* #RO090331 ^property[0].valueCode = #RO0903 
* #RO090332 "RO Knochenalter"
* #RO090332 ^property[0].code = #parent 
* #RO090332 ^property[0].valueCode = #RO0903 
* #RO090333 "RO Hand li."
* #RO090333 ^property[0].code = #parent 
* #RO090333 ^property[0].valueCode = #RO0903 
* #RO090333 ^property[1].code = #child 
* #RO090333 ^property[1].valueCode = #RO090333a 
* #RO090333 ^property[2].code = #child 
* #RO090333 ^property[2].valueCode = #RO090333b 
* #RO090333 ^property[3].code = #child 
* #RO090333 ^property[3].valueCode = #RO090333c 
* #RO090333a "RO Handwurzel li."
* #RO090333a ^property[0].code = #parent 
* #RO090333a ^property[0].valueCode = #RO090333 
* #RO090333b "RO Mittelhand li."
* #RO090333b ^property[0].code = #parent 
* #RO090333b ^property[0].valueCode = #RO090333 
* #RO090333c "RO Carpaltunnel li."
* #RO090333c ^property[0].code = #parent 
* #RO090333c ^property[0].valueCode = #RO090333 
* #RO090334 "RO Hand re."
* #RO090334 ^property[0].code = #parent 
* #RO090334 ^property[0].valueCode = #RO0903 
* #RO090334 ^property[1].code = #child 
* #RO090334 ^property[1].valueCode = #RO090334a 
* #RO090334 ^property[2].code = #child 
* #RO090334 ^property[2].valueCode = #RO090334b 
* #RO090334 ^property[3].code = #child 
* #RO090334 ^property[3].valueCode = #RO090334c 
* #RO090334a "RO Handwurzel re."
* #RO090334a ^property[0].code = #parent 
* #RO090334a ^property[0].valueCode = #RO090334 
* #RO090334b "RO Mittelhand re."
* #RO090334b ^property[0].code = #parent 
* #RO090334b ^property[0].valueCode = #RO090334 
* #RO090334c "RO Carpaltunnel re"
* #RO090334c ^property[0].code = #parent 
* #RO090334c ^property[0].valueCode = #RO090334 
* #RO090335 "RO Hand bds."
* #RO090335 ^property[0].code = #parent 
* #RO090335 ^property[0].valueCode = #RO0903 
* #RO090335 ^property[1].code = #child 
* #RO090335 ^property[1].valueCode = #RO090335a 
* #RO090335 ^property[2].code = #child 
* #RO090335 ^property[2].valueCode = #RO090335b 
* #RO090335 ^property[3].code = #child 
* #RO090335 ^property[3].valueCode = #RO090335c 
* #RO090335a "RO Handwurzel bds."
* #RO090335a ^property[0].code = #parent 
* #RO090335a ^property[0].valueCode = #RO090335 
* #RO090335b "RO Mittelhand bds."
* #RO090335b ^property[0].code = #parent 
* #RO090335b ^property[0].valueCode = #RO090335 
* #RO090335c "RO Carpaltunnel bds."
* #RO090335c ^property[0].code = #parent 
* #RO090335c ^property[0].valueCode = #RO090335 
* #RO090336 "RO Finger"
* #RO090336 ^property[0].code = #parent 
* #RO090336 ^property[0].valueCode = #RO0903 
* #RO090336 ^property[1].code = #child 
* #RO090336 ^property[1].valueCode = #RO090336a 
* #RO090336 ^property[2].code = #child 
* #RO090336 ^property[2].valueCode = #RO090336b 
* #RO090336 ^property[3].code = #child 
* #RO090336 ^property[3].valueCode = #RO090336c 
* #RO090336 ^property[4].code = #child 
* #RO090336 ^property[4].valueCode = #RO090336d 
* #RO090336 ^property[5].code = #child 
* #RO090336 ^property[5].valueCode = #RO090336e 
* #RO090336 ^property[6].code = #child 
* #RO090336 ^property[6].valueCode = #RO090336f 
* #RO090336 ^property[7].code = #child 
* #RO090336 ^property[7].valueCode = #RO090336g 
* #RO090336 ^property[8].code = #child 
* #RO090336 ^property[8].valueCode = #RO090336h 
* #RO090336 ^property[9].code = #child 
* #RO090336 ^property[9].valueCode = #RO090336i 
* #RO090336 ^property[10].code = #child 
* #RO090336 ^property[10].valueCode = #RO090336j 
* #RO090336 ^property[11].code = #child 
* #RO090336 ^property[11].valueCode = #RO090336k 
* #RO090336 ^property[12].code = #child 
* #RO090336 ^property[12].valueCode = #RO090336l 
* #RO090336a "RO Finger I li."
* #RO090336a ^property[0].code = #parent 
* #RO090336a ^property[0].valueCode = #RO090336 
* #RO090336b "RO Finger I re."
* #RO090336b ^property[0].code = #parent 
* #RO090336b ^property[0].valueCode = #RO090336 
* #RO090336c "RO Finger II li."
* #RO090336c ^property[0].code = #parent 
* #RO090336c ^property[0].valueCode = #RO090336 
* #RO090336d "RO Finger II re."
* #RO090336d ^property[0].code = #parent 
* #RO090336d ^property[0].valueCode = #RO090336 
* #RO090336e "RO Finger III li."
* #RO090336e ^property[0].code = #parent 
* #RO090336e ^property[0].valueCode = #RO090336 
* #RO090336f "RO Finger III re."
* #RO090336f ^property[0].code = #parent 
* #RO090336f ^property[0].valueCode = #RO090336 
* #RO090336g "RO Finger IV li."
* #RO090336g ^property[0].code = #parent 
* #RO090336g ^property[0].valueCode = #RO090336 
* #RO090336h "RO Finger IV re."
* #RO090336h ^property[0].code = #parent 
* #RO090336h ^property[0].valueCode = #RO090336 
* #RO090336i "RO Finger V li."
* #RO090336i ^property[0].code = #parent 
* #RO090336i ^property[0].valueCode = #RO090336 
* #RO090336j "RO Finger V re."
* #RO090336j ^property[0].code = #parent 
* #RO090336j ^property[0].valueCode = #RO090336 
* #RO090336k "RO Finger I gehalten li."
* #RO090336k ^property[0].code = #parent 
* #RO090336k ^property[0].valueCode = #RO090336 
* #RO090336l "RO Finger I gehalten re."
* #RO090336l ^property[0].code = #parent 
* #RO090336l ^property[0].valueCode = #RO090336 
* #RO0904 "RO Wirbelsäule/Beckenbereich"
* #RO0904 ^property[0].code = #parent 
* #RO0904 ^property[0].valueCode = #RO09 
* #RO0904 ^property[1].code = #child 
* #RO0904 ^property[1].valueCode = #RO090401 
* #RO0904 ^property[2].code = #child 
* #RO0904 ^property[2].valueCode = #RO090402 
* #RO0904 ^property[3].code = #child 
* #RO0904 ^property[3].valueCode = #RO090403 
* #RO0904 ^property[4].code = #child 
* #RO0904 ^property[4].valueCode = #RO090404 
* #RO0904 ^property[5].code = #child 
* #RO0904 ^property[5].valueCode = #RO090405 
* #RO0904 ^property[6].code = #child 
* #RO0904 ^property[6].valueCode = #RO090406 
* #RO0904 ^property[7].code = #child 
* #RO0904 ^property[7].valueCode = #RO090407 
* #RO0904 ^property[8].code = #child 
* #RO0904 ^property[8].valueCode = #RO090408 
* #RO0904 ^property[9].code = #child 
* #RO0904 ^property[9].valueCode = #RO090409 
* #RO0904 ^property[10].code = #child 
* #RO0904 ^property[10].valueCode = #RO090410 
* #RO090401 "RO HWS ap/seitl"
* #RO090401 ^property[0].code = #parent 
* #RO090401 ^property[0].valueCode = #RO0904 
* #RO090401 ^property[1].code = #child 
* #RO090401 ^property[1].valueCode = #RO090401a 
* #RO090401 ^property[2].code = #child 
* #RO090401 ^property[2].valueCode = #RO090401b 
* #RO090401 ^property[3].code = #child 
* #RO090401 ^property[3].valueCode = #RO090401c 
* #RO090401a "RO HWS Dens"
* #RO090401a ^property[0].code = #parent 
* #RO090401a ^property[0].valueCode = #RO090401 
* #RO090401b "RO HWS Funktion"
* #RO090401b ^property[0].code = #parent 
* #RO090401b ^property[0].valueCode = #RO090401 
* #RO090401c "RO HWS schräg"
* #RO090401c ^property[0].code = #parent 
* #RO090401c ^property[0].valueCode = #RO090401 
* #RO090402 "RO BWS ap/seitl."
* #RO090402 ^property[0].code = #parent 
* #RO090402 ^property[0].valueCode = #RO0904 
* #RO090402 ^property[1].code = #child 
* #RO090402 ^property[1].valueCode = #RO090402a 
* #RO090402 ^property[2].code = #child 
* #RO090402 ^property[2].valueCode = #RO090402b 
* #RO090402a "RO Übergang HWS/BWS"
* #RO090402a ^property[0].code = #parent 
* #RO090402a ^property[0].valueCode = #RO090402 
* #RO090402b "RO Übergang BWS/LWS"
* #RO090402b ^property[0].code = #parent 
* #RO090402b ^property[0].valueCode = #RO090402 
* #RO090403 "RO LWS ap/seitl."
* #RO090403 ^property[0].code = #parent 
* #RO090403 ^property[0].valueCode = #RO0904 
* #RO090403 ^property[1].code = #child 
* #RO090403 ^property[1].valueCode = #RO090403a 
* #RO090403 ^property[2].code = #child 
* #RO090403 ^property[2].valueCode = #RO090403b 
* #RO090403 ^property[3].code = #child 
* #RO090403 ^property[3].valueCode = #RO090403c 
* #RO090403a "RO LWS schräg"
* #RO090403a ^property[0].code = #parent 
* #RO090403a ^property[0].valueCode = #RO090403 
* #RO090403b "RO LWS Funktion"
* #RO090403b ^property[0].code = #parent 
* #RO090403b ^property[0].valueCode = #RO090403 
* #RO090403c "RO LWS Übergang"
* #RO090403c ^property[0].code = #parent 
* #RO090403c ^property[0].valueCode = #RO090403 
* #RO090404 "RO WS-Gesamt stehend"
* #RO090404 ^property[0].code = #parent 
* #RO090404 ^property[0].valueCode = #RO0904 
* #RO090404 ^property[1].code = #child 
* #RO090404 ^property[1].valueCode = #RO090404a 
* #RO090404a "RO WS-Gesamt Bendingaufnahme"
* #RO090404a ^property[0].code = #parent 
* #RO090404a ^property[0].valueCode = #RO090404 
* #RO090405 "RO Kreuz-Steissbein"
* #RO090405 ^property[0].code = #parent 
* #RO090405 ^property[0].valueCode = #RO0904 
* #RO090406 "RO Iliosacralgelenk li."
* #RO090406 ^property[0].code = #parent 
* #RO090406 ^property[0].valueCode = #RO0904 
* #RO090407 "RO Iliosacralgelenk re."
* #RO090407 ^property[0].code = #parent 
* #RO090407 ^property[0].valueCode = #RO0904 
* #RO090408 "RO Iliosacralgelenk bds."
* #RO090408 ^property[0].code = #parent 
* #RO090408 ^property[0].valueCode = #RO0904 
* #RO090409 "RO Beckenübersicht"
* #RO090409 ^property[0].code = #parent 
* #RO090409 ^property[0].valueCode = #RO0904 
* #RO090409 ^property[1].code = #child 
* #RO090409 ^property[1].valueCode = #RO090409a 
* #RO090409 ^property[2].code = #child 
* #RO090409 ^property[2].valueCode = #RO090409b 
* #RO090409 ^property[3].code = #child 
* #RO090409 ^property[3].valueCode = #RO090409c 
* #RO090409 ^property[4].code = #child 
* #RO090409 ^property[4].valueCode = #RO090409d 
* #RO090409 ^property[5].code = #child 
* #RO090409 ^property[5].valueCode = #RO090409e 
* #RO090409 ^property[6].code = #child 
* #RO090409 ^property[6].valueCode = #RO090409f 
* #RO090409 ^property[7].code = #child 
* #RO090409 ^property[7].valueCode = #RO090409g 
* #RO090409 ^property[8].code = #child 
* #RO090409 ^property[8].valueCode = #RO090409h 
* #RO090409 ^property[9].code = #child 
* #RO090409 ^property[9].valueCode = #RO090409i 
* #RO090409a "RO Becken Ala-Aufnahme"
* #RO090409a ^property[0].code = #parent 
* #RO090409a ^property[0].valueCode = #RO090409 
* #RO090409b "RO Becken Foramen Obturator"
* #RO090409b ^property[0].code = #parent 
* #RO090409b ^property[0].valueCode = #RO090409 
* #RO090409c "RO Schambein"
* #RO090409c ^property[0].code = #parent 
* #RO090409c ^property[0].valueCode = #RO090409 
* #RO090409d "RO Becken Inlet-Aufnahme"
* #RO090409d ^property[0].code = #parent 
* #RO090409d ^property[0].valueCode = #RO090409 
* #RO090409e "RO Becken Outlet-Aufnahme"
* #RO090409e ^property[0].code = #parent 
* #RO090409e ^property[0].valueCode = #RO090409 
* #RO090409f "RO Becken Ala-Aufnahme li."
* #RO090409f ^property[0].code = #parent 
* #RO090409f ^property[0].valueCode = #RO090409 
* #RO090409g "RO Becken Foramen Obturator li."
* #RO090409g ^property[0].code = #parent 
* #RO090409g ^property[0].valueCode = #RO090409 
* #RO090409h "RO Becken Ala-Aufnahme re."
* #RO090409h ^property[0].code = #parent 
* #RO090409h ^property[0].valueCode = #RO090409 
* #RO090409i "RO Becken Foramen Obturator re."
* #RO090409i ^property[0].code = #parent 
* #RO090409i ^property[0].valueCode = #RO090409 
* #RO090410 "RO Schablonenaufnahme (Becken)"
* #RO090410 ^property[0].code = #parent 
* #RO090410 ^property[0].valueCode = #RO0904 
* #RO0905 "RO Untere Extremität"
* #RO0905 ^property[0].code = #parent 
* #RO0905 ^property[0].valueCode = #RO09 
* #RO0905 ^property[1].code = #child 
* #RO0905 ^property[1].valueCode = #RO090501 
* #RO0905 ^property[2].code = #child 
* #RO0905 ^property[2].valueCode = #RO090502 
* #RO0905 ^property[3].code = #child 
* #RO0905 ^property[3].valueCode = #RO090503 
* #RO0905 ^property[4].code = #child 
* #RO0905 ^property[4].valueCode = #RO090504 
* #RO0905 ^property[5].code = #child 
* #RO0905 ^property[5].valueCode = #RO090505 
* #RO0905 ^property[6].code = #child 
* #RO0905 ^property[6].valueCode = #RO090506 
* #RO0905 ^property[7].code = #child 
* #RO0905 ^property[7].valueCode = #RO090507 
* #RO0905 ^property[8].code = #child 
* #RO0905 ^property[8].valueCode = #RO090508 
* #RO0905 ^property[9].code = #child 
* #RO0905 ^property[9].valueCode = #RO090509 
* #RO0905 ^property[10].code = #child 
* #RO0905 ^property[10].valueCode = #RO090511 
* #RO0905 ^property[11].code = #child 
* #RO0905 ^property[11].valueCode = #RO090512 
* #RO0905 ^property[12].code = #child 
* #RO0905 ^property[12].valueCode = #RO090513 
* #RO0905 ^property[13].code = #child 
* #RO0905 ^property[13].valueCode = #RO090514 
* #RO0905 ^property[14].code = #child 
* #RO0905 ^property[14].valueCode = #RO090515 
* #RO0905 ^property[15].code = #child 
* #RO0905 ^property[15].valueCode = #RO090516 
* #RO0905 ^property[16].code = #child 
* #RO0905 ^property[16].valueCode = #RO090517 
* #RO0905 ^property[17].code = #child 
* #RO0905 ^property[17].valueCode = #RO090518 
* #RO0905 ^property[18].code = #child 
* #RO0905 ^property[18].valueCode = #RO090519 
* #RO0905 ^property[19].code = #child 
* #RO0905 ^property[19].valueCode = #RO090520 
* #RO0905 ^property[20].code = #child 
* #RO0905 ^property[20].valueCode = #RO090521 
* #RO0905 ^property[21].code = #child 
* #RO0905 ^property[21].valueCode = #RO090522 
* #RO0905 ^property[22].code = #child 
* #RO0905 ^property[22].valueCode = #RO090533 
* #RO0905 ^property[23].code = #child 
* #RO0905 ^property[23].valueCode = #RO090534 
* #RO0905 ^property[24].code = #child 
* #RO0905 ^property[24].valueCode = #RO090535 
* #RO0905 ^property[25].code = #child 
* #RO0905 ^property[25].valueCode = #RO090536 
* #RO090501 "RO Hüfte li."
* #RO090501 ^property[0].code = #parent 
* #RO090501 ^property[0].valueCode = #RO0905 
* #RO090501 ^property[1].code = #child 
* #RO090501 ^property[1].valueCode = #RO090501a 
* #RO090501 ^property[2].code = #child 
* #RO090501 ^property[2].valueCode = #RO090501b 
* #RO090501 ^property[3].code = #child 
* #RO090501 ^property[3].valueCode = #RO090501c 
* #RO090501 ^property[4].code = #child 
* #RO090501 ^property[4].valueCode = #RO090501d 
* #RO090501 ^property[5].code = #child 
* #RO090501 ^property[5].valueCode = #RO090501e 
* #RO090501a "RO Hüfte Faux Profil li."
* #RO090501a ^property[0].code = #parent 
* #RO090501a ^property[0].valueCode = #RO090501 
* #RO090501b "RO Hüfte Lauenstein li."
* #RO090501b ^property[0].code = #parent 
* #RO090501b ^property[0].valueCode = #RO090501 
* #RO090501c "RO Hüfte Ala-Obturator li."
* #RO090501c ^property[0].code = #parent 
* #RO090501c ^property[0].valueCode = #RO090501 
* #RO090501d "RO Hüfte Darmbeinschaufel 30 Grad li."
* #RO090501d ^property[0].code = #parent 
* #RO090501d ^property[0].valueCode = #RO090501 
* #RO090501e "RO Hüfte mit Oberschenkel li."
* #RO090501e ^property[0].code = #parent 
* #RO090501e ^property[0].valueCode = #RO090501 
* #RO090502 "RO Hüfte re."
* #RO090502 ^property[0].code = #parent 
* #RO090502 ^property[0].valueCode = #RO0905 
* #RO090502 ^property[1].code = #child 
* #RO090502 ^property[1].valueCode = #RO090502a 
* #RO090502 ^property[2].code = #child 
* #RO090502 ^property[2].valueCode = #RO090502b 
* #RO090502 ^property[3].code = #child 
* #RO090502 ^property[3].valueCode = #RO090502c 
* #RO090502 ^property[4].code = #child 
* #RO090502 ^property[4].valueCode = #RO090502d 
* #RO090502 ^property[5].code = #child 
* #RO090502 ^property[5].valueCode = #RO090502e 
* #RO090502a "RO Hüfte Faux Profil re."
* #RO090502a ^property[0].code = #parent 
* #RO090502a ^property[0].valueCode = #RO090502 
* #RO090502b "RO Hüfte Lauenstein re."
* #RO090502b ^property[0].code = #parent 
* #RO090502b ^property[0].valueCode = #RO090502 
* #RO090502c "RO Hüfte Ala-Obturator re."
* #RO090502c ^property[0].code = #parent 
* #RO090502c ^property[0].valueCode = #RO090502 
* #RO090502d "RO Hüfte Darmbeinschaufel 30 Grad re."
* #RO090502d ^property[0].code = #parent 
* #RO090502d ^property[0].valueCode = #RO090502 
* #RO090502e "RO Hüfte mit Oberschenkel re."
* #RO090502e ^property[0].code = #parent 
* #RO090502e ^property[0].valueCode = #RO090502 
* #RO090503 "RO Hüfte bds."
* #RO090503 ^property[0].code = #parent 
* #RO090503 ^property[0].valueCode = #RO0905 
* #RO090503 ^property[1].code = #child 
* #RO090503 ^property[1].valueCode = #RO090503a 
* #RO090503 ^property[2].code = #child 
* #RO090503 ^property[2].valueCode = #RO090503b 
* #RO090503 ^property[3].code = #child 
* #RO090503 ^property[3].valueCode = #RO090503c 
* #RO090503 ^property[4].code = #child 
* #RO090503 ^property[4].valueCode = #RO090503d 
* #RO090503a "RO Hüfte Faux Profil bds."
* #RO090503a ^property[0].code = #parent 
* #RO090503a ^property[0].valueCode = #RO090503 
* #RO090503b "RO Hüfte Lauenstein bds."
* #RO090503b ^property[0].code = #parent 
* #RO090503b ^property[0].valueCode = #RO090503 
* #RO090503c "RO Hüfte Ala-Obturator bds."
* #RO090503c ^property[0].code = #parent 
* #RO090503c ^property[0].valueCode = #RO090503 
* #RO090503d "RO Hüfte Darmbeinschaufel 30 Grad bds."
* #RO090503d ^property[0].code = #parent 
* #RO090503d ^property[0].valueCode = #RO090503 
* #RO090504 "RO Oberschenkel li."
* #RO090504 ^property[0].code = #parent 
* #RO090504 ^property[0].valueCode = #RO0905 
* #RO090505 "RO Oberschenkel re."
* #RO090505 ^property[0].code = #parent 
* #RO090505 ^property[0].valueCode = #RO0905 
* #RO090506 "RO Oberschenkel bds."
* #RO090506 ^property[0].code = #parent 
* #RO090506 ^property[0].valueCode = #RO0905 
* #RO090507 "RO Knie li."
* #RO090507 ^property[0].code = #parent 
* #RO090507 ^property[0].valueCode = #RO0905 
* #RO090507 ^property[1].code = #child 
* #RO090507 ^property[1].valueCode = #RO090507b 
* #RO090507 ^property[2].code = #child 
* #RO090507 ^property[2].valueCode = #RO090507c 
* #RO090507 ^property[3].code = #child 
* #RO090507 ^property[3].valueCode = #RO090507d 
* #RO090507 ^property[4].code = #child 
* #RO090507 ^property[4].valueCode = #RO090507e 
* #RO090507 ^property[5].code = #child 
* #RO090507 ^property[5].valueCode = #RO090507f 
* #RO090507 ^property[6].code = #child 
* #RO090507 ^property[6].valueCode = #RO090507g 
* #RO090507 ^property[7].code = #child 
* #RO090507 ^property[7].valueCode = #RO090507h 
* #RO090507b "RO Knie stehend li."
* #RO090507b ^property[0].code = #parent 
* #RO090507b ^property[0].valueCode = #RO090507 
* #RO090507c "RO Knie Patella defile li."
* #RO090507c ^property[0].code = #parent 
* #RO090507c ^property[0].valueCode = #RO090507 
* #RO090507d "RO Knie Patella tangential li."
* #RO090507d ^property[0].code = #parent 
* #RO090507d ^property[0].valueCode = #RO090507 
* #RO090507e "RO Knie Rosenberg li."
* #RO090507e ^property[0].code = #parent 
* #RO090507e ^property[0].valueCode = #RO090507 
* #RO090507f "RO Knie gehalten li."
* #RO090507f ^property[0].code = #parent 
* #RO090507f ^property[0].valueCode = #RO090507 
* #RO090507g "RO Knie mit Oberschenkel li."
* #RO090507g ^property[0].code = #parent 
* #RO090507g ^property[0].valueCode = #RO090507 
* #RO090507h "RO Knie mit Unterschenkel li."
* #RO090507h ^property[0].code = #parent 
* #RO090507h ^property[0].valueCode = #RO090507 
* #RO090508 "RO Knie re."
* #RO090508 ^property[0].code = #parent 
* #RO090508 ^property[0].valueCode = #RO0905 
* #RO090508 ^property[1].code = #child 
* #RO090508 ^property[1].valueCode = #RO090508b 
* #RO090508 ^property[2].code = #child 
* #RO090508 ^property[2].valueCode = #RO090508c 
* #RO090508 ^property[3].code = #child 
* #RO090508 ^property[3].valueCode = #RO090508d 
* #RO090508 ^property[4].code = #child 
* #RO090508 ^property[4].valueCode = #RO090508e 
* #RO090508 ^property[5].code = #child 
* #RO090508 ^property[5].valueCode = #RO090508f 
* #RO090508 ^property[6].code = #child 
* #RO090508 ^property[6].valueCode = #RO090508g 
* #RO090508 ^property[7].code = #child 
* #RO090508 ^property[7].valueCode = #RO090508h 
* #RO090508b "RO Knie stehend re."
* #RO090508b ^property[0].code = #parent 
* #RO090508b ^property[0].valueCode = #RO090508 
* #RO090508c "RO Knie Patella defile re."
* #RO090508c ^property[0].code = #parent 
* #RO090508c ^property[0].valueCode = #RO090508 
* #RO090508d "RO Knie Patella tangential re."
* #RO090508d ^property[0].code = #parent 
* #RO090508d ^property[0].valueCode = #RO090508 
* #RO090508e "RO Knie Rosenberg re."
* #RO090508e ^property[0].code = #parent 
* #RO090508e ^property[0].valueCode = #RO090508 
* #RO090508f "RO Knie gehalten re."
* #RO090508f ^property[0].code = #parent 
* #RO090508f ^property[0].valueCode = #RO090508 
* #RO090508g "RO Knie mit Oberschenkel re."
* #RO090508g ^property[0].code = #parent 
* #RO090508g ^property[0].valueCode = #RO090508 
* #RO090508h "RO Knie mit Unterschenkel re."
* #RO090508h ^property[0].code = #parent 
* #RO090508h ^property[0].valueCode = #RO090508 
* #RO090509 "RO Knie bds."
* #RO090509 ^property[0].code = #parent 
* #RO090509 ^property[0].valueCode = #RO0905 
* #RO090509 ^property[1].code = #child 
* #RO090509 ^property[1].valueCode = #RO090509b 
* #RO090509 ^property[2].code = #child 
* #RO090509 ^property[2].valueCode = #RO090509c 
* #RO090509 ^property[3].code = #child 
* #RO090509 ^property[3].valueCode = #RO090509d 
* #RO090509 ^property[4].code = #child 
* #RO090509 ^property[4].valueCode = #RO090509e 
* #RO090509b "RO Knie stehend bds."
* #RO090509b ^property[0].code = #parent 
* #RO090509b ^property[0].valueCode = #RO090509 
* #RO090509c "RO Knie Patella defile bds."
* #RO090509c ^property[0].code = #parent 
* #RO090509c ^property[0].valueCode = #RO090509 
* #RO090509d "RO Knie Patella tangential bds."
* #RO090509d ^property[0].code = #parent 
* #RO090509d ^property[0].valueCode = #RO090509 
* #RO090509e "RO Knie Rosenberg bds."
* #RO090509e ^property[0].code = #parent 
* #RO090509e ^property[0].valueCode = #RO090509 
* #RO090511 "RO Unterschenkel li."
* #RO090511 ^property[0].code = #parent 
* #RO090511 ^property[0].valueCode = #RO0905 
* #RO090511 ^property[1].code = #child 
* #RO090511 ^property[1].valueCode = #RO090511a 
* #RO090511 ^property[2].code = #child 
* #RO090511 ^property[2].valueCode = #RO090511b 
* #RO090511 ^property[3].code = #child 
* #RO090511 ^property[3].valueCode = #RO090511c 
* #RO090511a "RO Unterschenkel hohe Fibula li."
* #RO090511a ^property[0].code = #parent 
* #RO090511a ^property[0].valueCode = #RO090511 
* #RO090511b "RO Unterschenkel schräg li."
* #RO090511b ^property[0].code = #parent 
* #RO090511b ^property[0].valueCode = #RO090511 
* #RO090511c "RO Unterschenkel mit Sprunggelenk li."
* #RO090511c ^property[0].code = #parent 
* #RO090511c ^property[0].valueCode = #RO090511 
* #RO090512 "RO Unterschenkel re."
* #RO090512 ^property[0].code = #parent 
* #RO090512 ^property[0].valueCode = #RO0905 
* #RO090512 ^property[1].code = #child 
* #RO090512 ^property[1].valueCode = #RO090512a 
* #RO090512 ^property[2].code = #child 
* #RO090512 ^property[2].valueCode = #RO090512b 
* #RO090512 ^property[3].code = #child 
* #RO090512 ^property[3].valueCode = #RO090512c 
* #RO090512a "RO Unterschenkel hohe Fibula re."
* #RO090512a ^property[0].code = #parent 
* #RO090512a ^property[0].valueCode = #RO090512 
* #RO090512b "RO Unterschenkel schräg re."
* #RO090512b ^property[0].code = #parent 
* #RO090512b ^property[0].valueCode = #RO090512 
* #RO090512c "RO Unterschenkel mit Sprunggelenk re."
* #RO090512c ^property[0].code = #parent 
* #RO090512c ^property[0].valueCode = #RO090512 
* #RO090513 "RO Unterschenkel bds."
* #RO090513 ^property[0].code = #parent 
* #RO090513 ^property[0].valueCode = #RO0905 
* #RO090513 ^property[1].code = #child 
* #RO090513 ^property[1].valueCode = #RO090513a 
* #RO090513 ^property[2].code = #child 
* #RO090513 ^property[2].valueCode = #RO090513b 
* #RO090513a "RO Unterschenkel hohe Fibula bds."
* #RO090513a ^property[0].code = #parent 
* #RO090513a ^property[0].valueCode = #RO090513 
* #RO090513b "RO Unterschenkel schräg bds."
* #RO090513b ^property[0].code = #parent 
* #RO090513b ^property[0].valueCode = #RO090513 
* #RO090514 "RO Sprunggelenk li."
* #RO090514 ^property[0].code = #parent 
* #RO090514 ^property[0].valueCode = #RO0905 
* #RO090514 ^property[1].code = #child 
* #RO090514 ^property[1].valueCode = #RO090514a 
* #RO090514 ^property[2].code = #child 
* #RO090514 ^property[2].valueCode = #RO090514b 
* #RO090514 ^property[3].code = #child 
* #RO090514 ^property[3].valueCode = #RO090514c 
* #RO090514 ^property[4].code = #child 
* #RO090514 ^property[4].valueCode = #RO090514d 
* #RO090514a "RO Sprunggelenk Ziel li."
* #RO090514a ^property[0].code = #parent 
* #RO090514a ^property[0].valueCode = #RO090514 
* #RO090514b "RO Sprunggelenk Suppination li."
* #RO090514b ^property[0].code = #parent 
* #RO090514b ^property[0].valueCode = #RO090514 
* #RO090514c "RO Sprunggelenk Ventralvorschub li."
* #RO090514c ^property[0].code = #parent 
* #RO090514c ^property[0].valueCode = #RO090514 
* #RO090514d "RO Sprunggelenk laterale Verschiebung li."
* #RO090514d ^property[0].code = #parent 
* #RO090514d ^property[0].valueCode = #RO090514 
* #RO090515 "RO Sprunggelenk re."
* #RO090515 ^property[0].code = #parent 
* #RO090515 ^property[0].valueCode = #RO0905 
* #RO090515 ^property[1].code = #child 
* #RO090515 ^property[1].valueCode = #RO090515a 
* #RO090515 ^property[2].code = #child 
* #RO090515 ^property[2].valueCode = #RO090515b 
* #RO090515 ^property[3].code = #child 
* #RO090515 ^property[3].valueCode = #RO090515c 
* #RO090515 ^property[4].code = #child 
* #RO090515 ^property[4].valueCode = #RO090515d 
* #RO090515a "RO Sprunggelenk Ziel re."
* #RO090515a ^property[0].code = #parent 
* #RO090515a ^property[0].valueCode = #RO090515 
* #RO090515b "RO Sprunggelenk Suppination re."
* #RO090515b ^property[0].code = #parent 
* #RO090515b ^property[0].valueCode = #RO090515 
* #RO090515c "RO Sprunggelenk Ventralvorschub re."
* #RO090515c ^property[0].code = #parent 
* #RO090515c ^property[0].valueCode = #RO090515 
* #RO090515d "RO Sprunggelenk laterale Verschiebung re."
* #RO090515d ^property[0].code = #parent 
* #RO090515d ^property[0].valueCode = #RO090515 
* #RO090516 "RO Sprunggelenk bds."
* #RO090516 ^property[0].code = #parent 
* #RO090516 ^property[0].valueCode = #RO0905 
* #RO090516 ^property[1].code = #child 
* #RO090516 ^property[1].valueCode = #RO090516a 
* #RO090516 ^property[2].code = #child 
* #RO090516 ^property[2].valueCode = #RO090516b 
* #RO090516 ^property[3].code = #child 
* #RO090516 ^property[3].valueCode = #RO090516c 
* #RO090516 ^property[4].code = #child 
* #RO090516 ^property[4].valueCode = #RO090516d 
* #RO090516a "RO Sprunggelenk Ziel bds."
* #RO090516a ^property[0].code = #parent 
* #RO090516a ^property[0].valueCode = #RO090516 
* #RO090516b "RO Sprunggelenk Suppination bds."
* #RO090516b ^property[0].code = #parent 
* #RO090516b ^property[0].valueCode = #RO090516 
* #RO090516c "RO Sprunggelenk Ventralvorschub bds."
* #RO090516c ^property[0].code = #parent 
* #RO090516c ^property[0].valueCode = #RO090516 
* #RO090516d "RO Sprunggelenk laterale Verschiebung bds."
* #RO090516d ^property[0].code = #parent 
* #RO090516d ^property[0].valueCode = #RO090516 
* #RO090517 "RO Fuß li."
* #RO090517 ^property[0].code = #parent 
* #RO090517 ^property[0].valueCode = #RO0905 
* #RO090517 ^property[1].code = #child 
* #RO090517 ^property[1].valueCode = #RO090517a 
* #RO090517 ^property[2].code = #child 
* #RO090517 ^property[2].valueCode = #RO090517b 
* #RO090517 ^property[3].code = #child 
* #RO090517 ^property[3].valueCode = #RO090517c 
* #RO090517a "RO Vorfuß li."
* #RO090517a ^property[0].code = #parent 
* #RO090517a ^property[0].valueCode = #RO090517 
* #RO090517b "RO Mittelfuß li."
* #RO090517b ^property[0].code = #parent 
* #RO090517b ^property[0].valueCode = #RO090517 
* #RO090517c "RO Fußwurzel li."
* #RO090517c ^property[0].code = #parent 
* #RO090517c ^property[0].valueCode = #RO090517 
* #RO090518 "RO Fuß re."
* #RO090518 ^property[0].code = #parent 
* #RO090518 ^property[0].valueCode = #RO0905 
* #RO090518 ^property[1].code = #child 
* #RO090518 ^property[1].valueCode = #RO090518a 
* #RO090518 ^property[2].code = #child 
* #RO090518 ^property[2].valueCode = #RO090518b 
* #RO090518 ^property[3].code = #child 
* #RO090518 ^property[3].valueCode = #RO090518c 
* #RO090518a "RO Vorfuß re."
* #RO090518a ^property[0].code = #parent 
* #RO090518a ^property[0].valueCode = #RO090518 
* #RO090518b "RO Mittelfuß re."
* #RO090518b ^property[0].code = #parent 
* #RO090518b ^property[0].valueCode = #RO090518 
* #RO090518c "RO Fußwurzel re."
* #RO090518c ^property[0].code = #parent 
* #RO090518c ^property[0].valueCode = #RO090518 
* #RO090519 "RO Fuß bds."
* #RO090519 ^property[0].code = #parent 
* #RO090519 ^property[0].valueCode = #RO0905 
* #RO090519 ^property[1].code = #child 
* #RO090519 ^property[1].valueCode = #RO090519a 
* #RO090519 ^property[2].code = #child 
* #RO090519 ^property[2].valueCode = #RO090519b 
* #RO090519 ^property[3].code = #child 
* #RO090519 ^property[3].valueCode = #RO090519c 
* #RO090519a "RO Vorfuß  bds."
* #RO090519a ^property[0].code = #parent 
* #RO090519a ^property[0].valueCode = #RO090519 
* #RO090519b "RO Mittelfuß bds."
* #RO090519b ^property[0].code = #parent 
* #RO090519b ^property[0].valueCode = #RO090519 
* #RO090519c "RO Fußwurzel bds."
* #RO090519c ^property[0].code = #parent 
* #RO090519c ^property[0].valueCode = #RO090519 
* #RO090520 "RO Fersenbein li."
* #RO090520 ^property[0].code = #parent 
* #RO090520 ^property[0].valueCode = #RO0905 
* #RO090520 ^property[1].code = #child 
* #RO090520 ^property[1].valueCode = #RO090520a 
* #RO090520a "RO Fersenbein Broden li."
* #RO090520a ^property[0].code = #parent 
* #RO090520a ^property[0].valueCode = #RO090520 
* #RO090521 "RO Fersenbein re."
* #RO090521 ^property[0].code = #parent 
* #RO090521 ^property[0].valueCode = #RO0905 
* #RO090521 ^property[1].code = #child 
* #RO090521 ^property[1].valueCode = #RO090521a 
* #RO090521a "RO Fersenbein Broden re."
* #RO090521a ^property[0].code = #parent 
* #RO090521a ^property[0].valueCode = #RO090521 
* #RO090522 "RO Fersenbein bds."
* #RO090522 ^property[0].code = #parent 
* #RO090522 ^property[0].valueCode = #RO0905 
* #RO090533 "RO Ganzbein li."
* #RO090533 ^property[0].code = #parent 
* #RO090533 ^property[0].valueCode = #RO0905 
* #RO090534 "RO Ganzbein re."
* #RO090534 ^property[0].code = #parent 
* #RO090534 ^property[0].valueCode = #RO0905 
* #RO090535 "RO Ganzbein bds."
* #RO090535 ^property[0].code = #parent 
* #RO090535 ^property[0].valueCode = #RO0905 
* #RO090536 "RO Zehen"
* #RO090536 ^property[0].code = #parent 
* #RO090536 ^property[0].valueCode = #RO0905 
* #RO090536 ^property[1].code = #child 
* #RO090536 ^property[1].valueCode = #RO090536a 
* #RO090536 ^property[2].code = #child 
* #RO090536 ^property[2].valueCode = #RO090536b 
* #RO090536 ^property[3].code = #child 
* #RO090536 ^property[3].valueCode = #RO090536c 
* #RO090536 ^property[4].code = #child 
* #RO090536 ^property[4].valueCode = #RO090536d 
* #RO090536 ^property[5].code = #child 
* #RO090536 ^property[5].valueCode = #RO090536e 
* #RO090536 ^property[6].code = #child 
* #RO090536 ^property[6].valueCode = #RO090536f 
* #RO090536 ^property[7].code = #child 
* #RO090536 ^property[7].valueCode = #RO090536g 
* #RO090536 ^property[8].code = #child 
* #RO090536 ^property[8].valueCode = #RO090536h 
* #RO090536 ^property[9].code = #child 
* #RO090536 ^property[9].valueCode = #RO090536i 
* #RO090536 ^property[10].code = #child 
* #RO090536 ^property[10].valueCode = #RO090536j 
* #RO090536a "RO Zehen I li."
* #RO090536a ^property[0].code = #parent 
* #RO090536a ^property[0].valueCode = #RO090536 
* #RO090536b "RO Zehen I re."
* #RO090536b ^property[0].code = #parent 
* #RO090536b ^property[0].valueCode = #RO090536 
* #RO090536c "RO Zehen II li."
* #RO090536c ^property[0].code = #parent 
* #RO090536c ^property[0].valueCode = #RO090536 
* #RO090536d "RO Zehen II re."
* #RO090536d ^property[0].code = #parent 
* #RO090536d ^property[0].valueCode = #RO090536 
* #RO090536e "RO Zehen III li."
* #RO090536e ^property[0].code = #parent 
* #RO090536e ^property[0].valueCode = #RO090536 
* #RO090536f "RO Zehen III re."
* #RO090536f ^property[0].code = #parent 
* #RO090536f ^property[0].valueCode = #RO090536 
* #RO090536g "RO Zehen IV li."
* #RO090536g ^property[0].code = #parent 
* #RO090536g ^property[0].valueCode = #RO090536 
* #RO090536h "RO Zehen IV re."
* #RO090536h ^property[0].code = #parent 
* #RO090536h ^property[0].valueCode = #RO090536 
* #RO090536i "RO Zehen V li."
* #RO090536i ^property[0].code = #parent 
* #RO090536i ^property[0].valueCode = #RO090536 
* #RO090536j "RO Zehen V re."
* #RO090536j ^property[0].code = #parent 
* #RO090536j ^property[0].valueCode = #RO090536 
* #RO0906 "RO Gesamtes Skelett/Staging"
* #RO0906 ^property[0].code = #parent 
* #RO0906 ^property[0].valueCode = #RO09 
* #RO0907 "RO Drehaufnahme"
* #RO0907 ^property[0].code = #parent 
* #RO0907 ^property[0].valueCode = #RO09 
* #RO0908 "RO Stressaufnahme"
* #RO0908 ^property[0].code = #parent 
* #RO0908 ^property[0].valueCode = #RO09 
* #RO0909 "RO Zielaufnahme"
* #RO0909 ^property[0].code = #parent 
* #RO0909 ^property[0].valueCode = #RO09 
* #RO10 "RO Tomografie"
* #RO10 ^property[0].code = #parent 
* #RO10 ^property[0].valueCode = #RO 
* #RO11 "RO Mund-Kieferbereich"
* #RO11 ^property[0].code = #parent 
* #RO11 ^property[0].valueCode = #RO 
* #RO11 ^property[1].code = #child 
* #RO11 ^property[1].valueCode = #RO1101 
* #RO11 ^property[2].code = #child 
* #RO11 ^property[2].valueCode = #RO1102 
* #RO11 ^property[3].code = #child 
* #RO11 ^property[3].valueCode = #RO1103 
* #RO1101 "RO Zahnpanorama"
* #RO1101 ^property[0].code = #parent 
* #RO1101 ^property[0].valueCode = #RO11 
* #RO1102 "RO Zahn Einzelaufnahme"
* #RO1102 ^property[0].code = #parent 
* #RO1102 ^property[0].valueCode = #RO11 
* #RO1103 "RO Profil Fernaufnahme"
* #RO1103 ^property[0].code = #parent 
* #RO1103 ^property[0].valueCode = #RO11 
* #SO "SO Sonographie"
* #SO ^property[0].code = #child 
* #SO ^property[0].valueCode = #SO01 
* #SO ^property[1].code = #child 
* #SO ^property[1].valueCode = #SO02 
* #SO ^property[2].code = #child 
* #SO ^property[2].valueCode = #SO03 
* #SO ^property[3].code = #child 
* #SO ^property[3].valueCode = #SO04 
* #SO ^property[4].code = #child 
* #SO ^property[4].valueCode = #SO05 
* #SO ^property[5].code = #child 
* #SO ^property[5].valueCode = #SO06 
* #SO ^property[6].code = #child 
* #SO ^property[6].valueCode = #SO07 
* #SO01 "US Schädel"
* #SO01 ^property[0].code = #parent 
* #SO01 ^property[0].valueCode = #SO 
* #SO02 "US Thorax"
* #SO02 ^property[0].code = #parent 
* #SO02 ^property[0].valueCode = #SO 
* #SO03 "US Abdomen"
* #SO03 ^property[0].code = #parent 
* #SO03 ^property[0].valueCode = #SO 
* #SO03 ^property[1].code = #child 
* #SO03 ^property[1].valueCode = #SO0301 
* #SO03 ^property[2].code = #child 
* #SO03 ^property[2].valueCode = #SO0302 
* #SO03 ^property[3].code = #child 
* #SO03 ^property[3].valueCode = #SO0303 
* #SO03 ^property[4].code = #child 
* #SO03 ^property[4].valueCode = #SO0304 
* #SO03 ^property[5].code = #child 
* #SO03 ^property[5].valueCode = #SO0305 
* #SO03 ^property[6].code = #child 
* #SO03 ^property[6].valueCode = #SO0306 
* #SO0301 "US Abdomen gesamt"
* #SO0301 ^property[0].code = #parent 
* #SO0301 ^property[0].valueCode = #SO03 
* #SO0302 "US Oberbauch"
* #SO0302 ^property[0].code = #parent 
* #SO0302 ^property[0].valueCode = #SO03 
* #SO0303 "US Nieren und Retroperitoneum"
* #SO0303 ^property[0].code = #parent 
* #SO0303 ^property[0].valueCode = #SO03 
* #SO0304 "US Unterbauch"
* #SO0304 ^property[0].code = #parent 
* #SO0304 ^property[0].valueCode = #SO03 
* #SO0305 "US Transplantat Niere"
* #SO0305 ^property[0].code = #parent 
* #SO0305 ^property[0].valueCode = #SO03 
* #SO0306 "US Appendix"
* #SO0306 ^property[0].code = #parent 
* #SO0306 ^property[0].valueCode = #SO03 
* #SO04 "US Gefäßduplex"
* #SO04 ^property[0].code = #parent 
* #SO04 ^property[0].valueCode = #SO 
* #SO04 ^property[1].code = #child 
* #SO04 ^property[1].valueCode = #SO0401 
* #SO04 ^property[2].code = #child 
* #SO04 ^property[2].valueCode = #SO0402 
* #SO04 ^property[3].code = #child 
* #SO04 ^property[3].valueCode = #SO0403 
* #SO04 ^property[4].code = #child 
* #SO04 ^property[4].valueCode = #SO0404 
* #SO04 ^property[5].code = #child 
* #SO04 ^property[5].valueCode = #SO0405 
* #SO04 ^property[6].code = #child 
* #SO04 ^property[6].valueCode = #SO0406 
* #SO04 ^property[7].code = #child 
* #SO04 ^property[7].valueCode = #SO0407 
* #SO04 ^property[8].code = #child 
* #SO04 ^property[8].valueCode = #SO0408 
* #SO04 ^property[9].code = #child 
* #SO04 ^property[9].valueCode = #SO0409 
* #SO04 ^property[10].code = #child 
* #SO04 ^property[10].valueCode = #SO0410 
* #SO0401 "US Gefäßduplex der intrakraniellen Gefäße"
* #SO0401 ^property[0].code = #parent 
* #SO0401 ^property[0].valueCode = #SO04 
* #SO0402 "US Gefäßduplex Carotis Vertebralis"
* #SO0402 ^property[0].code = #parent 
* #SO0402 ^property[0].valueCode = #SO04 
* #SO0403 "US Gefäßduplex Transthorakale Echokardiographie (TTE)"
* #SO0403 ^property[0].code = #parent 
* #SO0403 ^property[0].valueCode = #SO04 
* #SO0404 "US Gefäßduplex arteriell OE"
* #SO0404 ^property[0].code = #parent 
* #SO0404 ^property[0].valueCode = #SO04 
* #SO0405 "US Gefäßduplex venös OE"
* #SO0405 ^property[0].code = #parent 
* #SO0405 ^property[0].valueCode = #SO04 
* #SO0406 "US Gefäßduplex Abdomen"
* #SO0406 ^property[0].code = #parent 
* #SO0406 ^property[0].valueCode = #SO04 
* #SO0407 "US Gefäßduplex Aorta"
* #SO0407 ^property[0].code = #parent 
* #SO0407 ^property[0].valueCode = #SO04 
* #SO0408 "US Gefäßduplex arteriell UE"
* #SO0408 ^property[0].code = #parent 
* #SO0408 ^property[0].valueCode = #SO04 
* #SO0409 "US Gefäßduplex venös UE"
* #SO0409 ^property[0].code = #parent 
* #SO0409 ^property[0].valueCode = #SO04 
* #SO0410 "US Farbduplex diverses"
* #SO0410 ^property[0].code = #parent 
* #SO0410 ^property[0].valueCode = #SO04 
* #SO05 "US small parts"
* #SO05 ^property[0].code = #parent 
* #SO05 ^property[0].valueCode = #SO 
* #SO05 ^property[1].code = #child 
* #SO05 ^property[1].valueCode = #SO0501 
* #SO05 ^property[2].code = #child 
* #SO05 ^property[2].valueCode = #SO0502 
* #SO05 ^property[3].code = #child 
* #SO05 ^property[3].valueCode = #SO0503 
* #SO05 ^property[4].code = #child 
* #SO05 ^property[4].valueCode = #SO0504 
* #SO05 ^property[5].code = #child 
* #SO05 ^property[5].valueCode = #SO0505 
* #SO05 ^property[6].code = #child 
* #SO05 ^property[6].valueCode = #SO0506 
* #SO05 ^property[7].code = #child 
* #SO05 ^property[7].valueCode = #SO0507 
* #SO05 ^property[8].code = #child 
* #SO05 ^property[8].valueCode = #SO0508 
* #SO05 ^property[9].code = #child 
* #SO05 ^property[9].valueCode = #SO0509 
* #SO05 ^property[10].code = #child 
* #SO05 ^property[10].valueCode = #SO0510 
* #SO05 ^property[11].code = #child 
* #SO05 ^property[11].valueCode = #SO0511 
* #SO05 ^property[12].code = #child 
* #SO05 ^property[12].valueCode = #SO0512 
* #SO05 ^property[13].code = #child 
* #SO05 ^property[13].valueCode = #SO0513 
* #SO05 ^property[14].code = #child 
* #SO05 ^property[14].valueCode = #SO0514 
* #SO05 ^property[15].code = #child 
* #SO05 ^property[15].valueCode = #SO0515 
* #SO05 ^property[16].code = #child 
* #SO05 ^property[16].valueCode = #SO0516 
* #SO05 ^property[17].code = #child 
* #SO05 ^property[17].valueCode = #SO0517 
* #SO05 ^property[18].code = #child 
* #SO05 ^property[18].valueCode = #SO0518 
* #SO05 ^property[19].code = #child 
* #SO05 ^property[19].valueCode = #SO0519 
* #SO0501 "US am Auge"
* #SO0501 ^property[0].code = #parent 
* #SO0501 ^property[0].valueCode = #SO05 
* #SO0502 "US Lymphknotenstationen"
* #SO0502 ^property[0].code = #parent 
* #SO0502 ^property[0].valueCode = #SO05 
* #SO0502 ^property[1].code = #child 
* #SO0502 ^property[1].valueCode = #SO050201 
* #SO0502 ^property[2].code = #child 
* #SO0502 ^property[2].valueCode = #SO050202 
* #SO0502 ^property[3].code = #child 
* #SO0502 ^property[3].valueCode = #SO050203 
* #SO050201 "US Lymphknotenstationen cervical"
* #SO050201 ^property[0].code = #parent 
* #SO050201 ^property[0].valueCode = #SO0502 
* #SO050202 "US Lymphknotenstationen axillär"
* #SO050202 ^property[0].code = #parent 
* #SO050202 ^property[0].valueCode = #SO0502 
* #SO050203 "US Lymphknotenstationen ingunial"
* #SO050203 ^property[0].code = #parent 
* #SO050203 ^property[0].valueCode = #SO0502 
* #SO0503 "US Schilddrüse"
* #SO0503 ^property[0].code = #parent 
* #SO0503 ^property[0].valueCode = #SO05 
* #SO0504 "US Hals, Speicheldrüsen"
* #SO0504 ^property[0].code = #parent 
* #SO0504 ^property[0].valueCode = #SO05 
* #SO0505 "US Mamma li."
* #SO0505 ^property[0].code = #parent 
* #SO0505 ^property[0].valueCode = #SO05 
* #SO0506 "US Mamma re."
* #SO0506 ^property[0].code = #parent 
* #SO0506 ^property[0].valueCode = #SO05 
* #SO0507 "US Mamma bds."
* #SO0507 ^property[0].code = #parent 
* #SO0507 ^property[0].valueCode = #SO05 
* #SO0508 "US Scrotum"
* #SO0508 ^property[0].code = #parent 
* #SO0508 ^property[0].valueCode = #SO05 
* #SO0509 "US Achillessehne li."
* #SO0509 ^property[0].code = #parent 
* #SO0509 ^property[0].valueCode = #SO05 
* #SO0510 "US Achillessehne re."
* #SO0510 ^property[0].code = #parent 
* #SO0510 ^property[0].valueCode = #SO05 
* #SO0511 "US Achillessehne bds."
* #SO0511 ^property[0].code = #parent 
* #SO0511 ^property[0].valueCode = #SO05 
* #SO0512 "US Endovaginale Sonographie"
* #SO0512 ^property[0].code = #parent 
* #SO0512 ^property[0].valueCode = #SO05 
* #SO0513 "US Transrectale Sonographie"
* #SO0513 ^property[0].code = #parent 
* #SO0513 ^property[0].valueCode = #SO05 
* #SO0514 "US kindliche Hüfte li."
* #SO0514 ^property[0].code = #parent 
* #SO0514 ^property[0].valueCode = #SO05 
* #SO0515 "US kindliche Hüfte re."
* #SO0515 ^property[0].code = #parent 
* #SO0515 ^property[0].valueCode = #SO05 
* #SO0516 "US kindliche Hüfte bds."
* #SO0516 ^property[0].code = #parent 
* #SO0516 ^property[0].valueCode = #SO05 
* #SO0517 "US oberflächliche Strukturen"
* #SO0517 ^property[0].code = #parent 
* #SO0517 ^property[0].valueCode = #SO05 
* #SO0518 "US Bewegungsapparat"
* #SO0518 ^property[0].code = #parent 
* #SO0518 ^property[0].valueCode = #SO05 
* #SO0519 "US Endosonographie"
* #SO0519 ^property[0].code = #parent 
* #SO0519 ^property[0].valueCode = #SO05 
* #SO06 "US Intervention"
* #SO06 ^property[0].code = #parent 
* #SO06 ^property[0].valueCode = #SO 
* #SO06 ^property[1].code = #child 
* #SO06 ^property[1].valueCode = #SO0601 
* #SO06 ^property[2].code = #child 
* #SO06 ^property[2].valueCode = #SO0602 
* #SO06 ^property[3].code = #child 
* #SO06 ^property[3].valueCode = #SO0603 
* #SO06 ^property[4].code = #child 
* #SO06 ^property[4].valueCode = #SO0604 
* #SO06 ^property[5].code = #child 
* #SO06 ^property[5].valueCode = #SO0605 
* #SO06 ^property[6].code = #child 
* #SO06 ^property[6].valueCode = #SO0606 
* #SO0601 "US Biopsie"
* #SO0601 ^property[0].code = #parent 
* #SO0601 ^property[0].valueCode = #SO06 
* #SO0602 "US Drainage"
* #SO0602 ^property[0].code = #parent 
* #SO0602 ^property[0].valueCode = #SO06 
* #SO0603 "US Markierung"
* #SO0603 ^property[0].code = #parent 
* #SO0603 ^property[0].valueCode = #SO06 
* #SO0604 "US Intervention Diverses "
* #SO0604 ^property[0].code = #parent 
* #SO0604 ^property[0].valueCode = #SO06 
* #SO0605 "US Punktion"
* #SO0605 ^property[0].code = #parent 
* #SO0605 ^property[0].valueCode = #SO06 
* #SO0606 "US Ablation"
* #SO0606 ^property[0].code = #parent 
* #SO0606 ^property[0].valueCode = #SO06 
* #SO07 "US Echokardiographie"
* #SO07 ^property[0].code = #parent 
* #SO07 ^property[0].valueCode = #SO 
* #SO07 ^property[1].code = #child 
* #SO07 ^property[1].valueCode = #SO0701 
* #SO07 ^property[2].code = #child 
* #SO07 ^property[2].valueCode = #SO0702 
* #SO07 ^property[3].code = #child 
* #SO07 ^property[3].valueCode = #SO0703 
* #SO07 ^property[4].code = #child 
* #SO07 ^property[4].valueCode = #SO0704 
* #SO07 ^property[5].code = #child 
* #SO07 ^property[5].valueCode = #SO0705 
* #SO0701 "US Stressechokardiographie"
* #SO0701 ^property[0].code = #parent 
* #SO0701 ^property[0].valueCode = #SO07 
* #SO0702 "US Intrakoronare Druckmessung (\"pressure wire\")"
* #SO0702 ^property[0].code = #parent 
* #SO0702 ^property[0].valueCode = #SO07 
* #SO0703 "US Intrakoronarer Ultraschall"
* #SO0703 ^property[0].code = #parent 
* #SO0703 ^property[0].valueCode = #SO07 
* #SO0703 ^property[1].code = #child 
* #SO0703 ^property[1].valueCode = #SO070301 
* #SO0703 ^property[2].code = #child 
* #SO0703 ^property[2].valueCode = #SO070302 
* #SO070301 "US Intrakoronarer Ultraschall - IVUS"
* #SO070301 ^property[0].code = #parent 
* #SO070301 ^property[0].valueCode = #SO0703 
* #SO070302 "US Intrakoronarer Ultraschall - OCT"
* #SO070302 ^property[0].code = #parent 
* #SO070302 ^property[0].valueCode = #SO0703 
* #SO0704 "US TEE – Transösophageale Echokardiographie"
* #SO0704 ^property[0].code = #parent 
* #SO0704 ^property[0].valueCode = #SO07 
* #SO0705 "US TTE – Transthorakale Echokardiographie"
* #SO0705 ^property[0].code = #parent 
* #SO0705 ^property[0].valueCode = #SO07 
